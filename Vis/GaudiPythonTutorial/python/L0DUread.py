# get the basic configuration from here
from LHCbConfig import *
#from LHCbConfig import HltConf
lhcbApp.DataType = "DC06"

#importOptions('$L0DUOPTS/L0DUFromRaw.opts')
#importOptions('~miriam/python/bw/L0.opts')
l0seq = GaudiSequencer("seqL0")
appConf.TopAlg += [ l0seq ]
L0Conf().setProp( "L0Sequencer", l0seq )
L0Conf().setProp( "ReplaceL0BanksWithEmulated", True )
L0Conf().TCK = '0xFFF8'
#HltConf().ThresholdSettings = 'Miriam_20090430'
# define a helper class:
class irange(object) :
   def __init__(self, b, e ):
     self.begin, self.end  = b, e
   def __iter__(self):
     it = self.begin
     while it != self.end :
       yield it.__deref__()
       it.__postinc__(1)

rawwriter  = OutputStream('RawWriter', Preload = False,
  ItemList = ["/Event#1","/Event/DAQ#1","/Event/DAQ/RawEvent#1","/Event/DAQ/ODIN#1"],
  Output   = "DATAFILE='PFN:/castor/cern.ch/user/d/dijkstra/Stripped-mbias-DC08/test.raw' TYP='POOL_ROOTTREE' OPT='REC' ")
     
#  Output   = "DATAFILE='mdf-test.raw' TYP='POOL_ROOTTREE' OPT='REC' ")
appConf = ApplicationMgr( OutputLevel = INFO, AppName = 'readtest')
appConf.OutStream = [rawwriter]

appConf.HistogramPersistency = "HBOOK"
#appConf.HistogramPersistency = "ROOT"
HistogramPersistencySvc().OutputFile  = "test.hbook"

EventSelector().PrintFreq  = 1000

import GaudiPython
gbl=GaudiPython.gbl
from gaudigadgets import *

#EventSelector(
# Input = ["DATA='castor:/castor/cern.ch/grid/lhcb/test/MDF/00003083/0000/00003083_00000040_1.mdf' SVC='LHCb::MDFSelector'",
#          "DATA='castor:/castor/cern.ch/grid/lhcb/test/MDF/00003083/0000/00003083_00000041_1.mdf' SVC='LHCb::MDFSelector'"
#          ])
              
appMgr = GaudiPython.AppMgr() 
sel    = appMgr.evtsel()
sel.open(['PFN:/castor/cern.ch/user/d/dijkstra/BW-division/mbias-lumi2.raw'])
#sel.open(['PFN:/castor/cern.ch/user/d/dijkstra/BW-division/Bd2KstarMuMu-lum2.raw'])
#sel.open(['PFN:/castor/cern.ch/user/m/mitesh/125/0/outputdata/KStarMuMu_forTrigger.dst',
#         'PFN:/castor/cern.ch/user/m/mitesh/125/1/outputdata/KStarMuMu_forTrigger.dst',
#          'PFN:/castor/cern.ch/user/m/mitesh/125/2/outputdata/KStarMuMu_forTrigger.dst',
#          'PFN:/castor/cern.ch/user/m/mitesh/125/3/outputdata/KStarMuMu_forTrigger.dst',
#          'PFN:/castor/cern.ch/user/m/mitesh/125/4/outputdata/KStarMuMu_forTrigger.dst',
#          'PFN:/castor/cern.ch/user/m/mitesh/125/5/outputdata/KStarMuMu_forTrigger.dst',
#          'PFN:/castor/cern.ch/user/m/mitesh/125/6/outputdata/KStarMuMu_forTrigger.dst',
#          'PFN:/castor/cern.ch/user/m/mitesh/125/7/outputdata/KStarMuMu_forTrigger.dst',
#          'PFN:/castor/cern.ch/user/m/mitesh/125/8/outputdata/KStarMuMu_forTrigger.dst',
#          'PFN:/castor/cern.ch/user/m/mitesh/125/9/outputdata/KStarMuMu_forTrigger.dst',
#          'PFN:/castor/cern.ch/user/m/mitesh/125/10/outputdata/KStarMuMu_forTrigger.dst',
#          'PFN:/castor/cern.ch/user/m/mitesh/125/11/outputdata/KStarMuMu_forTrigger.dst',
#          'PFN:/castor/cern.ch/user/m/mitesh/125/12/outputdata/KStarMuMu_forTrigger.dst',
#          'PFN:/castor/cern.ch/user/m/mitesh/125/13/outputdata/KStarMuMu_forTrigger.dst'
#          ])
evt = appMgr.evtsvc()

hist  = appMgr.histsvc()
#Declare retrieve function for hist ids.
fh=hist.retrieve
h=hist.book('1','L0Du decision',2,-0.5,1.5)
h=hist.book('2','L0DU-YES decision',10,-0.5,9.5)
h=hist.book('11','hadron Et',256,0.,5.12)
h=hist.book('12','e Et',256,0.,5.12)
h=hist.book('13','photon Et',256,0.,5.12)
h=hist.book('14','muon1 pt',128,0.,5.12)
h=hist.book('15','muon1 pt vs muon2 pt',128,0.,5.12,128,0.,5.12)
h=hist.book('21','hadron Et',256,0.,5.12)
h=hist.book('22','e Et',256,0.,5.12)
h=hist.book('23','photon Et',256,0.,5.12)
h=hist.book('24','muon1 pt',128,0.,5.12)

appMgr.algorithm('RawWriter').Enable = False  # stop automatic execution of RawWriter

n = 10000
nread=0
nL0YES=0
while n>0 :
 n-=1 
 appMgr.run(1)
 # check L0
 L0DUReport = evt['Trig/L0/L0DUReport']
 if not L0DUReport : break  # probably end of input
 nread+=1
 if nread<2:
    evt.dumpAll()
    print L0DUReport
    for i in range(0,9):
      print i,L0DUReport.channelName(i)
 #extract some Et values...
 L0DUConfig = L0DUReport.configuration()
 datas      = L0DUConfig.data()
 for x in irange(datas.begin(),datas.end()) :
    idata = x.second
    if idata.name()=='Muon1(Pt)': ptm1=idata.value()/1000.
    if idata.name()=='Muon2(Pt)': ptm2=idata.value()/1000.
 if ptm2>0.: fh('15').fill(ptm1,ptm2)
 if L0DUReport.decision()<>0:
      nL0YES+=1
      fh('1').fill(1)
      for x in irange(datas.begin(),datas.end()) :
        idata = x.second
        if idata.name()=='Hadron(Et)': fh('11').fill(idata.value()/1000.)
        if idata.name()=='Electron(Et)': fh('12').fill(idata.value()/1000.)
        if idata.name()=='Photon(Et)': fh('13').fill(idata.value()/1000.)
        if idata.name()=='Muon1(Pt)': fh('14').fill(idata.value()/1000.)
      for i in range(0,9):
          if L0DUReport.channelDecisionByName(L0DUReport.channelName(i))<>0: fh("2").fill(i)
      #rc = appMgr.algorithm('RawWriter').execute() # output event
 else:
      fh('1').fill(0)
      for x in irange(datas.begin(),datas.end()) :
        idata = x.second
        if idata.name()=='Hadron(Et)': fh('21').fill(idata.value()/1000.)
        if idata.name()=='Electron(Et)': fh('22').fill(idata.value()/1000.)
        if idata.name()=='Photon(Et)': fh('23').fill(idata.value()/1000.)
        if idata.name()=='Muon1(Pt)': fh('24').fill(idata.value()/1000.)

print 'read ',nread,' events'
print 'L0DU accepted ',nL0YES,' events'
 
