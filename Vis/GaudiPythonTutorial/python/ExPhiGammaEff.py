# setenv PYTHONPATH /afs/cern.ch/sw/lcg/external/processing/0.51/${CMTCONFIG}/lib/python2.5/site-packages:$PYTHONPATH
from ROOT import TFile,TH1F,TH2F,TCanvas,TBrowser,gStyle,TText,TMath,TF1,gROOT,MakeNullPointer,gSystem
f_2008   = True
debug    = False

# get the basic configuration from here
from LHCbConfig import *
if not f_2008:
    lhcbApp.DataType = "DC06"
else:
    lhcbApp.DataType = "2008"
    lhcbApp.Simulation = True     

appConf = ApplicationMgr( OutputLevel = INFO, AppName = 'ExPhiGammaEff' )
EventSelector().PrintFreq  = 100
importOptions ('$CALOASSOCIATORSROOT/options/CaloAssociators.opts')


# suddenly required, no idea why otherwise, error:
FileCatalog().Catalogs = []

import GaudiPython
import GaudiKernel.SystemOfUnits as units

gbl = GaudiPython.gbl
ParticleID     = gbl.LHCb.ParticleID
MCTrackInfo    = gbl.MCTrackInfo
MCParticle     = gbl.LHCb.MCParticle
Track          = gbl.LHCb.Track
CaloCluster    = gbl.LHCb.CaloCluster
part           = MakeNullPointer(MCParticle)

import gaudigadgets
enums = gaudigadgets.getEnumNames('LHCb::Track')

def lifetime(p):
 tau = 0
 ov  = p.originVertex()
 ev  = p.endVertices()[0].target()
 tau = ( ev.time() - ov.time() ) / p.gamma()  
 return tau

def InEcalAcceptance(g):  
 # define "in Ecal acceptance" for MC-photons:
 mom = g.momentum() 
 mcpz = mom.z()
 mcpy = mom.y() 
 mcpx = mom.x() 
 inEcal = ( 0 < mom.r() )  & (   abs(mcpx/mcpz)<(4000./12500.) )    # check the numbers here! 
 inEcal = inEcal           & (   abs(mcpy/mcpz)<(3000./12500.) )    # check the numbers here!
 inEcal = inEcal           & ( ( abs(mcpx/mcpz)>(300. /12500.) ) |  ( abs(mcpy/mcpz)>(300. /12500.) ) )  # check the numbers here!
 return inEcal

def findRecoGamma(g,evt):
 table = evt['Relations/Rec/Calo/Clusters']
 ##invert table: MCParticle -> CaloCluster         
 itable = gbl.LHCb.RelationWeighted1D(MCParticle,CaloCluster,'float') ( table , 1 )
 links = itable.relations( g )
 return links
             
def chi2Match(type,p,evt):  
 pmom = p.momentum()
 sx = pmom.x()/pmom.z()
 sy = pmom.y()/pmom.z()
 p  = pmom.r()
 chi2min = 999999.
 tmatch  = None
 for t in evt['Rec/Track/Best']: 
   if type == "Long" and t.type() != t.Long : continue
   if t.type() != t.Long and t.type() != t.Velo and t.type() != t.Downstream : continue  
   fstate = t.firstState()
# ignore correlations 
   chi2x = (fstate.tx()-sx)*(fstate.tx()-sx)/fstate.errTx2()    
   chi2y = (fstate.ty()-sy)*(fstate.ty()-sy)/fstate.errTy2()    
   chi2 = chi2x+chi2y 
   if type == "Long" :
     delp = abs((t.p()-p)/p)
     chi2+=(delp*p)/(TMath.Sqrt(fstate.errQOverP2())*t.p()*t.p())
   if chi2 < chi2min : 
       chi2min = chi2
       if type == "Long" : tmatch = t              
 matched = False 
 if chi2min < 50 : matched = True    
 return matched,tmatch,chi2min
   
   
h_tau_mc = TH1F('h_tau_mc','tau Bs truth ',100,0.,10.)
h_tau_rg = TH1F('h_tau_rg','tau Bs for reconstructible in Ecal ',100,0.,10.)
h_tau_ec = TH1F('h_tau_ec','tau Bs for reconstructed Shower ',100,0.,10.)
h_tau_rv = TH1F('h_tau_rv','tau Bs for reconstructible in Velo ',100,0.,10.)
h_tau_rl = TH1F('h_tau_rl','tau Bs for reconstructible long ',100,0.,10.)
h_tau_rw = TH1F('h_tau_rw','tau Bs for reconstructed in Velo ',100,0.,10.)
h_tau_re = TH1F('h_tau_re','tau Bs for reconstructed long ',100,0.,10.)
h_tau_rwm = TH1F('h_tau_rwm','tau Bs for reconstructed in Velo chi2match ',100,0.,10.)
h_tau_rem = TH1F('h_tau_rem','tau Bs for reconstructed long chi2match    ',100,0.,10.)

h_single_rv = TH1F('h_single_rv','tau Bs for reconstructible in Velo ',100,0.,10.)
h_single_rl = TH1F('h_single_rl','tau Bs for reconstructible long ',100,0.,10.)
h_single_rw = TH1F('h_single_rw','tau Bs for reconstructed in Velo ',100,0.,10.)
h_single_re = TH1F('h_single_re','tau Bs for reconstructed long ',100,0.,10.)
h_single_rwm = TH1F('h_single_rwm','tau Bs for reconstructed in Velo chi2match ',100,0.,10.)
h_single_rem = TH1F('h_single_rem','tau Bs for reconstructed long chi2match    ',100,0.,10.)



myHistos = {}
for h in gROOT.GetList() : 
    myHistos[h.GetName()] = h

def processFile(file) :
 print '+++++ Start with file ',file
 htemp = {}
 for h in myHistos.keys() : 
    htemp[h] = myHistos[h].Clone()
     
 appMgr = GaudiPython.AppMgr()
 sel  = appMgr.evtsel()
 sel.open(file)
 evt  = appMgr.evtsvc()
 msg  = appMgr.service('MessageSvc', 'IMessageSvc')
 dps  = appMgr.service('EventDataSvc', 'IDataProviderSvc')
 from LinkerInstances.eventassoc import linkedFrom, linkedTo  

 MCDecayFinder  = appMgr.toolsvc().create('MCDecayFinder', interface='IMCDecayFinder')
 MCDecayFinder.setDecay( "[ B_s0  -> ( phi(1020) -> K+  K-  ) gamma]cc" )
 ppSvc = appMgr.ppSvc()
 bs_tau = ppSvc.find(ParticleID(531)).lifetime()

 while 0 < 1:
  appMgr.run(1)  
# check if there are still valid events 
  if not evt['Rec/Header'] : break
  if debug : 
    print ' next event ',evt['Rec/Header'].runNumber(), evt['Rec/Header'].evtNumber()   
  mc        = evt['MC/Particles']
  if not MCDecayFinder.hasDecay(mc) : continue
  decaylist = []
  trackinfo = MCTrackInfo(dps, msg)
  mc2track  = linkedFrom(Track,MCParticle,'Rec/Track/Best')   
  cont = evt['Rec/Track/Best']

  while MCDecayFinder.findDecay(mc,part)>0 :
   pclone = part.clone()
   GaudiPython.setOwnership(pclone,True)
   decaylist.append(pclone)
  for decay in decaylist :
   pid = decay.particleID().pid()
   tau = lifetime(decay)/bs_tau 
   daughters = GaudiPython.gbl.std.vector('const LHCb::MCParticle*')()
   MCDecayFinder.descendants(decay,daughters)
# find the photon   
   gamma = None  
   for dpart in daughters :
    if dpart.particleID().abspid() != 22 : continue 
    mother = dpart.mother()
    if mother.particleID().abspid() != 531  : continue
    gamma = dpart
    break
   if gamma  :  
     if InEcalAcceptance(gamma): htemp['h_tau_rg'].Fill(tau)
   else : 
    print 'should not happen'
    for d in  daughters : print d  
# find the reconstructed shower   
   if findRecoGamma(gamma,evt).size() > 0 : htemp['h_tau_ec'].Fill(tau)
# find the two kaons 
   kaons = [] 
   for dpart in daughters :
    if dpart.particleID().abspid() != 321 : continue 
    mother = dpart.mother()
    if mother.particleID().pid() != 333  : continue
    kaons.append(dpart)
   if len(kaons) != 2 : 
    continue
# 
   if debug :
    print ' kaon 0, ',kaons[0].p(), kaons[0].key(),trackinfo.hasVelo(kaons[0]),trackinfo.hasVeloAndT(kaons[0])
    print ' kaon 1, ',kaons[1].p(), kaons[1].key(),trackinfo.hasVelo(kaons[1]),trackinfo.hasVeloAndT(kaons[1])
   oVx = kaons[0].originVertex().position()
   rho =  oVx.rho()   
   z   =  oVx.z()
   htemp['h_tau_mc'].Fill(tau)
   if trackinfo.hasVelo(kaons[0]) : htemp['h_single_rv'].Fill(tau)
   if trackinfo.hasVelo(kaons[1]) : htemp['h_single_rv'].Fill(tau)
   inVelo = True
   if not trackinfo.hasVelo(kaons[0]) or not trackinfo.hasVelo(kaons[1]) : inVelo = False
   if inVelo : 
    htemp['h_tau_rv'].Fill(tau)
   inT = True
   if not trackinfo.hasVeloAndT(kaons[0]) or not trackinfo.hasVeloAndT(kaons[1]) : inT = False
   if inT :    htemp['h_tau_rl'].Fill(tau) 
   if trackinfo.hasVeloAndT(kaons[0]) : htemp['h_single_rl'].Fill(tau)
   if trackinfo.hasVeloAndT(kaons[1]) : htemp['h_single_rl'].Fill(tau)
# check associations:
   if debug: 
    if mc2track.range(kaons[0]).size()>1 or  mc2track.range(kaons[1]).size()>1 : 
     print 'more than one track reco :',mc2track.range(kaons[0]).size() , mc2track.range(kaons[1]).size()
     for t in mc2track.range(kaons[0]) : 
       print t
     for t in mc2track.range(kaons[1]) : 
       print t

   reco1 = False
   for t in mc2track.range(kaons[0]) : 
      if t.type() != t.Long and t.type() != t.Velo and t.type() != t.Downstream : continue
      reco1 = t.key()
      if trackinfo.hasVeloAndT(kaons[0]) : htemp['h_single_rw'].Fill(tau)
      break
   reco2 = False
   for t in mc2track.range(kaons[1]) : 
      if t.type() != t.Long and t.type() != t.Velo and t.type() != t.Downstream : continue
      reco2 = t.key()
      if trackinfo.hasVeloAndT(kaons[1]) : htemp['h_single_rw'].Fill(tau)
      break
   if inT and reco1 and reco2 : htemp['h_tau_rw'].Fill(tau)
# try with chi2Match
   veloMatch = False
   if chi2Match('Velo',kaons[0],evt)[0] and  trackinfo.hasVeloAndT(kaons[0]) :  htemp['h_single_rwm'].Fill(tau)
   if chi2Match('Velo',kaons[1],evt)[0] and  trackinfo.hasVeloAndT(kaons[1]) :  htemp['h_single_rwm'].Fill(tau)
   if inT :
     if chi2Match('Velo',kaons[0],evt)[0] and chi2Match('Velo',kaons[1],evt)[0] :
      veloMatch = True
      htemp['h_tau_rwm'].Fill(tau)

   reco11 = False
   for t in mc2track.range(kaons[0]) : 
      if t.type() != t.Long : continue
      reco11 = t.key()
      if trackinfo.hasVeloAndT(kaons[0]) and trackinfo.hasVeloAndT(kaons[0]) : htemp['h_single_re'].Fill(tau)
      break
   reco22 = False
   for t in mc2track.range(kaons[1]) : 
      if t.type() != t.Long : continue
      reco22 = t.key()
      if trackinfo.hasVeloAndT(kaons[1]) and trackinfo.hasVeloAndT(kaons[1]) : htemp['h_single_re'].Fill(tau)
      break
   if inT and reco11 and reco22 and reco1 and reco2  :  htemp['h_tau_re'].Fill(tau)
# try with chi2Match
   if chi2Match('Long',kaons[0],evt)[0] and  trackinfo.hasVeloAndT(kaons[0]) : htemp['h_single_rem'].Fill(tau)
   if chi2Match('Long',kaons[1],evt)[0] and  trackinfo.hasVeloAndT(kaons[1]) : htemp['h_single_rem'].Fill(tau)
   if veloMatch and inT :
     if chi2Match('Long',kaons[0],evt)[0] and chi2Match('Long',kaons[1],evt)[0] : htemp['h_tau_rem'].Fill(tau)

   if debug :
    if reco1 : 
      t = cont[reco1]
      print 'reconstructed as velo type',t.key(),enums['Types'][t.type()],enums['History'][t.history()],t.p()   
    if reco11 : 
      t = cont[reco11]
      print 'reconstructed as long',t.key(),enums['Types'][t.type()],enums['History'][t.history()],t.p()   
    matched, t, chi2min = chi2Match('Long',kaons[0],evt)
    if matched : 
      print 'matched with    ',t.key(),enums['Types'][t.type()],enums['History'][t.history()],t.p(), chi2min           
    if reco2 : 
      t = cont[reco2]
      print 'reconstructed as velo type',t.key(),enums['Types'][t.type()],enums['History'][t.history()],t.p()   
    if reco22 : 
      t = cont[reco22]
      print 'reconstructed as long',t.key(),enums['Types'][t.type()],enums['History'][t.history()],t.p()  
    matched, t, chi2min = chi2Match('Long',kaons[1],evt)
    if matched : 
      print 'matched with    ',t.key(),enums['Types'][t.type()],enums['History'][t.history()],t.p(), chi2min           
      
      
 print 'finished ',file,'entries ',htemp['h_tau_mc'].GetEntries(),htemp['h_tau_mc'].GetEntries()
 appMgr.stop()
 return htemp

if __name__ == '__main__' :
 files = []    
 if not debug : 
  if f_2008: file  = '/castor/cern.ch/grid/lhcb/MC/2008/DST/00003989/0000/00003989_00000XXX_5.dst'
  else : file  = '/castor/cern.ch/grid/lhcb/production/DC06/phys-v2-lumi2/00002019/DST/0000/00002019_00000XXX_5.dst'
  nfiles = 100
  if debug : nfiles = 3
  for n in range(1,nfiles) : 
   ff = file.replace('XXX','%(X)03d'%{'X':n})
   x  = os.system('nsls '+ff)
   if x == 0 :  files.append(ff)
 else :
  files = ['/castor/cern.ch/grid/lhcb/MC/2008/DST/00003989/0000/00003989_00000165_5.dst']  
   
  #------Use up to 8 processes
 from processing import Pool
 n = 8
 pool = Pool(n)
 result = pool.map_async(processFile, files)
 for hlist in result.get(timeout=10000) : 
    for h in hlist :
     if myHistos[h] : myHistos[h].Add(hlist[h])
     else                     : myHistos[h] = hlist[h]
         
f=TFile('Bsgammaeff.root','recreate')
for h in myHistos :
  sc = myHistos[h].Write()
f.Close()

def eff_calc(h1,h2) : 
 h3 = h1.Clone()
 h3.Sumw2()
 h3.Divide(h1,h2,1.,1.,'B')
 return h3
 
def eff_calc2(h1,h2) : 
 eff = h1.GetEntries() / h2.GetEntries()
 return eff
  
myHistos['h_eff_tau_rv'] = eff_calc(myHistos['h_tau_rv'],myHistos['h_tau_mc'])
myHistos['h_eff_tau_rl'] = eff_calc(myHistos['h_tau_rl'],myHistos['h_tau_rv'])
myHistos['h_eff_tau_rw'] = eff_calc(myHistos['h_tau_rw'],myHistos['h_tau_rl'])
myHistos['h_eff_tau_re'] = eff_calc(myHistos['h_tau_re'],myHistos['h_tau_rw'])
myHistos['h_eff_tau_rg'] = eff_calc(myHistos['h_tau_rg'],myHistos['h_tau_mc'])
myHistos['h_eff_tau_ec'] = eff_calc(myHistos['h_tau_ec'],myHistos['h_tau_rg'])
#myHistos['h_eff_tau_id'] = eff_calc(myHistos['h_tau_id'],myHistos['h_tau_re'])

myHistos['h_eff_tau_rwm'] = eff_calc(myHistos['h_tau_rwm'],myHistos['h_tau_rl'])
myHistos['h_eff_tau_rem'] = eff_calc(myHistos['h_tau_rem'],myHistos['h_tau_rwm'])

print 'efficiencies '
print 'reconstructible in Velo:'    ,eff_calc2(myHistos['h_tau_rv'],myHistos['h_tau_mc'])
print 'reconstructible as Long:'    ,eff_calc2(myHistos['h_tau_rl'],myHistos['h_tau_rv'])
print 'reconstructed   in Velo:'    ,eff_calc2(myHistos['h_tau_rw'],myHistos['h_tau_rl'])
print 'reconstructed   as Long:'    ,eff_calc2(myHistos['h_tau_re'],myHistos['h_tau_rw'])
print 'track reco efficiency total:',eff_calc2(myHistos['h_tau_re'],myHistos['h_tau_rl'])
print 'reconstructible in Ecal:'    ,eff_calc2(myHistos['h_tau_rg'],myHistos['h_tau_mc'])
print 'reconstructed shower:'       ,eff_calc2(myHistos['h_tau_ec'],myHistos['h_tau_rg'])
print 'using Chi2 matching'
print 'reconstructed   in Velo:'    ,eff_calc2(myHistos['h_tau_rwm'],myHistos['h_tau_rl'])
print 'reconstructed   as Long:'    ,eff_calc2(myHistos['h_tau_rem'],myHistos['h_tau_rwm'])
print 'reconstructed   as Long track:'    ,eff_calc2(myHistos['h_tau_rem'],myHistos['h_tau_rl'])

print 'efficiencies for single tracks'
print 'reconstructible in Velo:'    ,0.5*eff_calc2(myHistos['h_single_rv'],myHistos['h_tau_mc'])
print 'reconstructible as Long:'    ,eff_calc2(myHistos['h_single_rl'],myHistos['h_single_rv'])
print 'reconstructed   in Velo:'    ,eff_calc2(myHistos['h_single_rw'],myHistos['h_single_rl'])
print 'reconstructed   as Long:'    ,eff_calc2(myHistos['h_single_re'],myHistos['h_single_rw'])
print 'track reco efficiency total:',eff_calc2(myHistos['h_single_re'],myHistos['h_single_rl'])
print 'using Chi2 matching'
print 'reconstructed   in Velo:'    ,eff_calc2(myHistos['h_single_rwm'],myHistos['h_single_rl'])
print 'reconstructed   as Long:'    ,eff_calc2(myHistos['h_single_rem'],myHistos['h_single_rwm'])
print 'reconstructed   as Long track:'    ,eff_calc2(myHistos['h_single_rem'],myHistos['h_single_rl'])
 
