# get the basic configuration from here
from LHCbConfig import *
lhcbApp.DataType = "2011"
    
appConf = ApplicationMgr(OutputLevel = INFO) 

import GaudiPython
# load some additional gadgets
import gaudigadgets

appMgr = GaudiPython.AppMgr()
sel    = appMgr.evtsel()
sel.open(['$PANORAMIXDATA/Sel_Bsmumu_2highest.dst']) 
evt = appMgr.evtsvc()
# read one event
appMgr.run(1)
evt.dump()
print '==================================='
# unpack all packed containers
gaudigadgets.unpackAll()
evt.dump()




