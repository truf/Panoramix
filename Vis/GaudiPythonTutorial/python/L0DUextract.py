# get the basic configuration from here
from LHCbConfig import *
lhcbApp.DataType = "2008"
privateData = False

# Default options to rerun L0 
if lhcbApp.DataType == 'DC06' :  
 l0seq = GaudiSequencer("seqL0")
 appConf.TopAlg += [ l0seq ]
 L0Conf().setProp( "L0Sequencer", l0seq )
 L0Conf().setProp( "ReplaceL0BanksWithEmulated", True ) 
 L0Conf().TCK = '0xFFF8'
 from Configurables import GaudiSequencer, HltConf
 ApplicationMgr().ExtSvc += [ "LoKiSvc" ]
 #HltConf().HltType = 'PA+LU+VE+MU+HA+PH+EL'
 #HltConf().HltType = 'Hlt1+Hlt2'
 HltConf().HltType = 'Hlt1'
 HltConf().ThresholdSettings = 'Miriam_20090430' 
 appConf.TopAlg += [ GaudiSequencer('Hlt') ]
 
from Configurables import EventNodeKiller
rawwriter  = OutputStream('RawWriter', Preload = False,
  ItemList = ["/Event#1","/Event/DAQ#1","/Event/DAQ/RawEvent#1","/Event/DAQ/ODIN#1"],
  OptItemList = ["/Event/MyVector#1","/Event/MyObject#1"],
  Output   = "DATAFILE='PFN:L0yes.raw' TYP='POOL_ROOTTREE' OPT='REC' ")
appConf    = ApplicationMgr( OutputLevel = INFO, AppName = 'L0DUextract') 
appConf.OutStream = [rawwriter]
DataOnDemandSvc().Algorithms += ["DATA='/Event/Trig/L0/L0DUReport' TYPE='GaudiSequencer/L0'"]

EventNodeKiller().Nodes    = ["Rec", "MC", "Raw", "Gen", "Link", "pSim" , "pRec", "Prev", "PrevPrev", "Next", "Trig"]
EventSelector().PrintFreq  = 50


import GaudiPython
from GaudiPython import gbl

from gaudigadgets import *

# dictionary for private data is genereated by createPrivateDict.py
gbl.gSystem.Load('enclose') # Need to load the just created dictionary without autoloading
MyClass = gbl.Enclose(gbl.MyClass)
Vector  = gbl.Enclose('std::vector<double>')

def add_priv_data():
#---Add MyClass object------
  o = MyClass()
  o.i, o.d, o.s = evt['Gen/Header'].evtNumber(),  evt['Gen/Header'].luminosity(), evt['Gen/Header'].applicationName()
  GaudiPython.setOwnership(o, False)  # Neeed to give up ownership
  evt.registerObject('/Event/MyObject', o)
#---Add vector-----
  v = Vector()
  for x in evt['Gen/Header'].collisions() :  
   e = x.target().x1Bjorken()
   v.push_back(e)
  GaudiPython.setOwnership(v, False)
  evt.registerObject('/Event/MyVector', v)
  return True

def test():
#---Add vector-----
  v = Vector()
  for x in range(5) :  
   v.push_back(x)
  return True

files = []
file  = '/castor/cern.ch/grid/lhcb/MC/2008/DST/00003991/0000/00003991_00000XXX_5.dst'
import os
for n in range(1,5) : 
  ff = file.replace('XXX','%(X)03d'%{'X':n})
  x  = os.system('nsls '+ff)
  if x == 0 :  files.append("DATA='"+ff+"'  TYP='POOL_ROOTTREE' OPT='READ'")
EventSelector(Input = files)

appMgr = GaudiPython.AppMgr()
evt  = appMgr.evtsvc()

appMgr.algorithm('RawWriter').Enable = False  # stop automatic execution of RawWriter

while 1>0 : 
 appMgr.run(1)
 if not evt['Rec/Header'] : break  # probably end of input
 # need to add private data here before the L0 sequence kills the source
 if privateData : rc = add_priv_data() 
 # check L0
 L0dir = evt['Trig/L0/L0DUReport']
 if L0dir.decision() > 0 : 
  rc = appMgr.algorithm('RawWriter').execute() # output event             
appMgr.exit()                           
