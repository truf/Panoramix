from ROOT import TH1F, TBrowser, TCanvas
# get the basic configuration from here
from LHCbConfig import *
lhcbApp.DataType   = "2011"
lhcbApp.Simulation = False

appConf = ApplicationMgr(OutputLevel = INFO, AppName = 'Ex3c')

import GaudiPython
# load some additional gadget
from gaudigadgets import doxygen

appMgr = GaudiPython.AppMgr()
det    = appMgr.detsvc()
evt    = appMgr.evtsvc()
sel    = appMgr.evtsel()
sel.open(['$PANORAMIXDATA/Sel_Bsmumu_2highest.dst']) 
# this is a bit stupid, but needed otherwise event time wrong and update service fails
lhcb = det['/dd/Structure/LHCb']

appMgr.run(1)  
veloClusters = evt['Raw/Velo/Clusters']
print 'size of VeloCluster container:', veloClusters.size()
ItClusters = evt['Raw/IT/Clusters']
print 'size of ItCluster container:', ItClusters.size()
Muoncoords = evt['Raw/Muon/Coords']
print 'size of Muon container:', Muoncoords.size()

# The key for VeloClusters is a VeloChannelID object, 
# therefore direct access by index, evt['Raw/Velo/Clusters'][0], does not work.
aVeloCluster = veloClusters.containedObjects()[0]
doxygen(aVeloCluster)

