from ROOT import TH1F,TH2F,gSystem
# get the basic configuration from here
from LHCbConfig import *
lhcbApp.DataType = "DC06"
max_Events = 1000

newDST = True

appConf = ApplicationMgr(OutputLevel = INFO, AppName = 'Ex17b')

appConf.ExtSvc += ['TagCollectionSvc/EvtTupleSvc']
 
import GaudiPython
import GaudiKernel.SystemOfUnits as units

if newDST : 
 file = 'PFN:castor:/castor/cern.ch/user/l/lshchuts/stripping/SETC_newDST_v31/1.root'
else : 
 file = 'PFN:castor:/castor/cern.ch/user/l/lshchuts/stripping/SETC_v31/1.root'

EventSelector(
    Input = ["COLLECTION=\'TagCreator/1\' DATAFILE='%s' TYP='POOL_ROOT' SEL='(PreselHeavyDimuon>0)'"%file] )
if newDST :
 FileCatalog().Catalogs = ["xmlcatalog_file:newDST_v31.xml"]
else : 
 FileCatalog().Catalogs = ["xmlcatalog_file:dst_v31.xml"]

appMgr = GaudiPython.AppMgr()

evt = appMgr.evtsvc()
h_bmass_vs_pt=TH2F('h_bmass_vs_pt','B mass vs pt',100,0.,10.,100,2.0,11.0)

def my_loop():
 nevents = 0
 while nevents < max_Events :
  appMgr.run(1)
  if not evt['Rec/Header'] : break
  nevents+=1
  mybees = evt['Phys/PreselHeavyDimuon/Particles']
  if mybees :
   for b in mybees :
    rc=h_bmass_vs_pt.Fill(b.pt()/units.GeV,b.measuredMass()/units.GeV)

print "Start loop"
my_loop()
h_bmass_vs_pt.Draw()
