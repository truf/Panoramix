import os

# loop over all files in current directory, replace string A with B

# A='DecodeRawEvent.opts'
# B='DecodeRawEvent.py'

#A = """DataOnDemandSvc().Algorithms += ["DATA='/Event/MC/Particles' TYPE='UnpackMCParticle'"]"""
#B = """DataOnDemandSvc().AlgMap['/Event/MC/Particles'] =  'UnpackMCParticle' """

A = """DataOnDemandSvc().Algorithms += ["DATA='/Event/MC/Vertices' TYPE='UnpackMCVertex'"]"""
B = """DataOnDemandSvc().AlgMap['/Event/MC/Vertices'] =  'UnpackMCVertex' """
 
for fn in os.listdir('.'):
 if fn.find('.py') < 0 or fn=='replace.py': continue
 f = open(fn)
 xx = f.readlines()
 yy = []
 for l in xx : 
   yy.append(l.replace(A,B))
 f.close()
 f = open(fn,'w')  
 f.writelines(yy)
