from ROOT import TH1F, TFile
# get the basic configuration from here
from LHCbConfig import *
lhcbApp.DataType = "2008"

appConf = ApplicationMgr(OutputLevel = INFO,AppName = 'Ex10b')

from Configurables import MagneticFieldSvc
MagneticFieldSvc().UseConstantField = True
MagneticFieldSvc().UseConditions    = False
appConf.ExtSvc += ['MagneticFieldSvc']

from Configurables import OTTimeCreator,OTRawBankDecoder
OTTimeCreator().addTool(OTRawBankDecoder(),name='OTRawBankDecoder')
slot = 'DAQ/RawEvent'
OTTimeCreator().OTRawBankDecoder.RawEventLocation = slot
ToolSvc().addTool(OTRawBankDecoder(),name='OTRawBankDecoder')
ToolSvc().OTRawBankDecoder.RawEventLocation = slot

EventSelector().PrintFreq  = 5000

from TrackSys.Configuration import *
TrackSys().setSpecialDataOption("fieldOff",True)

#PatSeeding for cosmics

from Configurables import (PatSeeding, PatSeedingTool)
if TrackSys().getProp("fieldOff"):
   PatSeeding("PatSeeding").addTool(PatSeedingTool, name="PatSeedingTool")
   PatSeeding("PatSeeding").PatSeedingTool.xMagTol = 4e2;
   PatSeeding("PatSeeding").PatSeedingTool.zMagnet = 0.;
   PatSeeding("PatSeeding").PatSeedingTool.FieldOff = True;
   PatSeeding("PatSeeding").PatSeedingTool.MinMomentum = 5e4;
importOptions('$PATALGORITHMSROOT/options/PatSeedingTool-Cosmics.opts')
appConf.TopAlg += ['PatSeeding']

EventSelector(
    Input = ["DATA='castor:/castor/cern.ch/grid/lhcb/data/2008/RAW/LHCb/PHYSICS_COSMICS/26027/026027_0000061307.raw' SVC='LHCb::MDFSelector'"] )

import GaudiPython
appMgr = GaudiPython.AppMgr()

h_ot    = TH1F('h_ot','OT number of hits',100,-0.5,99.5)
h_track = TH1F('h_track','number of tracks',10,-0.5,9.5)
 
evt = appMgr.evtsvc()
r = open('Ex10b_results.txt','append')
r.write(slot +'\n')
for i in range(10000): 
  appMgr.run(1)
  if not evt['Raw/OT/Times'] : break
  otsize = evt['Raw/OT/Times'].size()
  h_ot.Fill(otsize)
  tsize = evt['Rec/Track/Seed'].size()
  h_track.Fill(tsize)
  for t in evt['Rec/Track/Seed'] : 
    r.write(' x,y,tx,ty: %5.3f %5.3f %5.3f %5.3f \n'%(t.states()[0].x(),t.states()[0].y(),t.states()[0].tx(),t.states()[0].ty()) )

r.write(' %20s %5.3f \n'%(h_ot.GetTitle(),h_ot.GetMean()) )
r.write(' %20s %5.3f \n'%(h_track.GetTitle(),h_track.GetEntries()-h_track.GetBinContent(1)) )
r.close()

f = TFile(slot.replace('/','')+'.root','recreate')
h_ot.Write()
h_track.Write()
f.Close()
