import sys,os,subprocess,time
# find out if in batch mode or not
batch   = False
analyze = False
if len(sys.argv) > 1 :
  if sys.argv[1]=='batch' : batch = True
  # in case, only analyze log files
  if sys.argv[1]=='-a' : analyze = True

examples = ['Ex0.py','Ex1.py','Ex1b.py','Ex2.py'
           ,'Ex3.py','Ex3b.py','Ex3c.py','Ex3d.py','Ex3e.py','Ex4.py','Ex5.py'
           ,'Ex6.py','Ex6b.py','Ex7.py','Ex7b.py','Ex7c.py','Ex8.py','Ex9.py'
           ,'Ex11.py','Ex12.py','Ex13.py','Ex13b.py','Ex16.py','RootExa2.py','Minuit_Example.py','decodePUS.py'
           ,'TGhost.py','rad_map09.py','VeloDet10.py','VeloCluster.py'
           ,'KsCheck.py','L0DUextract.py','L0DUreadtest.py','Reco.py','PrimVX.py','Ex_TisTos.py'
           ,'checkMeasurements_pg.py','checkGeant4_MS.py','readSIM_MCHits.py','IPandPresol_plot.py'
           ,'container_sizes.py','microDSTEx0.py','microDSTEx1.py','selv19r9.py','PrimVX.py'
           ,'createPrivateDict.py' # does not work on Ubuntu, something with boost
           ,'FullSequence.py'
#'Ex15.py' # Panoramix example
#'database_localcopy.py' # create slice of database
]

path = ''
if os.environ.has_key('GAUDIPYTHONTUTORIALROOT'): path = os.environ['GAUDIPYTHONTUTORIALROOT']+'/python/'

# prepare directory with all log files
found = False
for d in os.listdir(os.curdir):
 if d == 'log' :
  found =  True
  break
if not found : os.mkdir('log')

#execute all examples and store log file and return codes
# lines to be excluded when running in batch mode
excl_lines = []
excl_lines.append('webbrowser.open')
excl_lines.append('doxygen')

rc={}
if not analyze:
 for e in examples :
  print 'executing ',e
  fn   = e[:e.find('.py')]+'.log'
  logf = 'log/'+fn
  f=open(logf, 'w')
# make some massage to python file, redefine raw_input
  ex  = open(path+e)
  import tempfile
  tempdir = tempfile.mkdtemp()
  temppython = os.path.join(tempdir, 'tmp.py')
  print 'Creating temp file: ', temppython
  tmp = open(temppython,'w')
  tmp.write("def raw_input(s) : return 'ok' \n")
  if batch : 
   tmp.write("import ROOT \n")
   tmp.write("ROOT.gROOT.SetBatch(True) \n")
  lines = ex.readlines()
  for x in lines : 
   if batch :
  # exclude some lines
    for e in excl_lines: 
      if x.find(e) > -1 : 
       x = '#'
       break
      if x.find('nevent') == -1  : continue 
      nev    = x.find('nevent')
      eqsign = x[nev:].find('=') + nev
      if eqsign == -1 : continue
      nr = x[eqsign+1:]
      try: 
       if int(nr) > 100 : 
         x = x[:eqsign] + ' 100\n'       
      except: 
       continue  
   tmp.write(x)
  tmp.close()
# 
  start = time.time()
  xx = subprocess.Popen(['python', temppython],stdin=subprocess.PIPE,stdout=subprocess.PIPE)
  f.writelines(xx.stdout.readlines())
  rc[e] = 0
  os.remove(temppython)
  end = time.time()
  print 'wall clock time',end-start
  f.close()
 #os.system('mv tmp.py temp/'+e)
 
# analyze return codes:
 for e in examples :
  if rc[e] !=0 : print 'Problem in ',e,' return code = ',rc[e]

# analyze log files for errors and warnings

# known errors
known_errors = []
known_errors.append('INFO')
known_errors.append('no ROOT output file name')
known_errors.append('VALUE')
known_errors.append('STRATEGY=')
known_errors.append('DetDesc::IGeometryErrorSvc')
known_errors.append('Recovery of geometry errors')
known_errors.append('Fix your cmt')
known_errors.append('create/destroy') 
known_errors.append('initialize/finalize')
known_errors.append('skip already') 
known_errors.append('histograms saving not required')
known_errors.append('muon tracks unavailable')
known_errors.append('success')
known_errors.append('already included, ignored.')
known_errors.append('MessageSvc.setError')
known_errors.append('Referring to existing')
known_errors.append('You may not be able to navigate')
known_errors.append('It is an error to supply a base')
known_errors.append('You may not be able to nagivate back')
known_errors.append('using constant magnetic field')
known_errors.append('entirely steered by options')
known_errors.append('Requested condDB but using manually set polarity')
known_errors.append('Vertex location HARD-CODED')
known_errors.append('CONVERGENCE AND ERROR MATRIX')
known_errors.append('EXTERNAL ERROR MATRIX')
known_errors.append('MINOS ERRORS')
known_errors.append('Hessian or error matrix')
known_errors.append('parameter error analysis')
known_errors.append('/stat/')
known_errors.append('error banks')
known_errors.append('Default tag requested')
known_errors.append('MSG::ERROR')
known_errors.append('WARNING RawBankToSTLiteClusterAlg')
known_errors.append('"#errors"') 
known_errors.append('-ErrorsPrint') 
known_errors.append('-ErrorMax') 
known_errors.append('-ErrorCount') 
known_errors.append('-AcceptOnError') 
known_errors.append('TrajPoca:: Minimization did not converge')
known_errors.append('Fit data is empty')
known_errors.append('WARNING MuonIDAlg::')
known_errors.append('WARNING MCDecayFinder:: Could not find decay')
known_errors.append('Using default tag')

failed = False
for e in examples :
 print 'examine log file for ',e
 logf = 'log/'+e[:e.find('.py')]+'.log'
 f = open(logf)
 x=f.readlines()
 f.close()
 for line in x:
  test = line.lower()
  if test.find('warning') > -1 or test.find('error') > -1 or test.find('fatal') > -1 :
   known = False
   for x in known_errors :
    if test.find(x.lower())>-1 : known = True
   if known : 
    continue
   else : 
    if test.find('error') > -1 or test.find('fatal') > -1 : failed = True 
    print 'to be checked',line

if failed: 
 sys.exit(1)
