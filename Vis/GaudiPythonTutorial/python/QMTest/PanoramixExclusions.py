from GaudiTest import LineSkipper, RegexpReplacer
from GaudiConf.QMTest.LHCbExclusions import preprocessor as LHCbPreprocessor

preprocessor = LHCbPreprocessor + \
  LineSkipper(["TimingAuditor.T...   INFO * UnpackTrack"]) + \
  LineSkipper(["OnXSvc               INFO OnX GUI file"]) + \
  LineSkipper(["VisualizationSvc     INFO Loading visualization attributes file"]) + \
  LineSkipper(["TimingAuditor.T...   INFO EVENT LOOP"]) 
