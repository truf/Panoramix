from ROOT import TH1F,TH2F
# get the basic configuration from here
from LHCbConfig import *
lhcbApp.DataType = "2008"
lhcbApp.Simulation = True     

import GaudiKernel.SystemOfUnits as units
appConf = ApplicationMgr(OutputLevel = INFO, AppName = 'VeloPosToolCheck')

import GaudiPython
appMgr = GaudiPython.AppMgr()
det    = appMgr.detsvc()

h_VeloRerror   = TH1F('h_VeloRerror',   ' Velo R error as function of strip nr',2048,-0.5,2047.5)           
h_VelofRerror   = TH1F('h_VelofRerror',   ' Velo R fractional error as function of strip nr',2048,-0.5,2047.5)           
h_VeloRpitch   = TH1F('h_VeloRpitch',   ' Velo R pitch as function of strip nr',2048,-0.5,2047.5)           

h_VeloeRerror   = TH1F('h_VeloeRerror',   ' Velo R fractional emulated error as function of strip nr',2048,-0.5,2047.5)           
h_VeloRpitch2   = TH1F('h_VeloRpitch2',   ' Velo R pitch as function of strip nr',2048,-0.5,2047.5)           
  
gbl            = GaudiPython.gbl
VeloCluster    = gbl.LHCb.VeloCluster
VeloLiteCluster    = gbl.LHCb.VeloLiteCluster
VeloChannelID  = gbl.LHCb.VeloChannelID
pair           = GaudiPython.makeClass('pair<int,unsigned int>')
vector         = GaudiPython.makeClass('vector<pair<int,unsigned int> >')

velo       = det['/dd/Structure/LHCb/BeforeMagnetRegion/Velo']
velopotool = appMgr.toolsvc().create('VeloClusterPosition',interface='IVeloClusterPosition')

def meanResolution(pitch) : 
  p_0 = -6.301
  p_1 = 0.27
  return (p_0+(p_1*pitch))

# take a r sensor
sensorID = 1
RType    = 1
sensor = velo.rSensor(sensorID)
for strip in range(2048):
  clid  = VeloChannelID(sensorID,strip)
  fracStrip = 0
  size = 0 
  secondThres = True
  vlcl  = VeloLiteCluster(clid,fracStrip,size, secondThres)
  npair = pair(strip,99) 
  adcValues = vector()  
  adcValues.push_back(npair)  
  cl = VeloCluster(vlcl,adcValues)
  clusInfo    = velopotool.position(cl) 
  inters      = clusInfo.fractionalPosition       
  errMeasure  = clusInfo.fractionalError * velo.rSensor(clid).rPitch( cl.channelID().strip() ) 
  ferrMeasure  = clusInfo.fractionalError
  h_VeloRerror.Fill(strip,errMeasure)
  h_VelofRerror.Fill(strip,ferrMeasure)
  pitch = velo.rSensor(clid).rPitch( cl.channelID().strip() ) 
  h_VeloRpitch.Fill(strip,pitch)
# emulate VeloClusterPosition
  centreChannel=cl.channelID()
  sens=velo.sensor(centreChannel.sensor())
  fractionalPos = velopotool.fracPosLA(cl) 
  pitch=sensor.rPitch(centreChannel.strip(), fractionalPos)
  h_VeloRpitch2.Fill(strip,pitch)
  errorPos=meanResolution(pitch/units.micrometer)
  errorPos/=(pitch/units.micrometer)
  h_VeloeRerror.Fill(strip,errorPos)
  
  
    
