# run in Panoramix
#/castor/cern.ch/grid/lhcb/production/DC06/L0-v1-lumi2/00001959/DST/0000/00001959_00000001_1.dst' 
#/castor/cern.ch/grid/lhcb/production/DC06/L0-v1-lumi2/00001959/DST/0000/00001959_00000002_1.dst' 

print ' modified script'
pano = True

from panoramixmodule import *
# prepare page:
if not ui().findWidget('Viewer_2d') :     
 sys_import('truf_Velo_RZView_setup')

from ROOT import TMath,TH1F,TH2F
from LinkerInstances.eventassoc import linkedFrom, linkedTo  
MCParticle   = GaudiPython.gbl.LHCb.MCParticle 
ParticleID   = GaudiPython.gbl.LHCb.ParticleID 
VeloCluster  = GaudiPython.gbl.LHCb.VeloCluster
XYZPoint     = LHCbMath.XYZPoint
import gaudigadgets
enums = gaudigadgets.getEnumNames('LHCb::Track')
ppSvc = appMgr.ppSvc()

#get TISTOS tool
tsvc = appMgr.toolsvc()
tistostool= tsvc.create('TriggerTisTos',interface='ITriggerTisTos')

## trackcontainer = 'Rec/Track/Velo'
trackcontainer = 'Hlt1/Track/Forward'

h_rz     = TH1F('h_rz','max fraction of rz clusters',100,-0.1,1.1)
h_other     = TH1F('h_other','max fraction of rz clusters in other track',100,-0.1,1.1)
h_other1    = TH1F('h_other1','max fraction of rz clusters in other track',100,-0.1,1.1)
h_rz1    = TH1F('h_rz1','max fraction of rz clusters, 1 ghost',100,-0.1,1.1)
h_rznog  = TH1F('h_rznog','max fraction of rz clusters, no electrons at all',100,-0.1,1.1)
h_rznoe  = TH1F('h_rznoe','max fraction of rz clusters, no electrons',100,-0.1,1.1)
h_rznoe1 = TH1F('h_rznoe1','max fraction of rz clusters, no electrons, 1 ghost',100,-0.1,1.1)
h_3d     = TH1F('h_3d','max fraction of 3d clusters',100,-0.1,1.1)
h_ntr     = TH1F('h_ntr','number of tracks per event',100, 0.5,100.5)
h_ngh     = TH1F('h_ngh','number of ghosts per event',100,-0.5,99.5)
h_idparent     = TH1F('h_idparent','ID of parent MCParticles',10000,-0.5,9999.5)
h_idparentw    = TH1F('h_idparentw','ID of parent MCParticles weighted',10000,-0.5,9999.5)

h_3dxy   = TH2F('h_3dxy','tx / ty of velo ghosts',100,-0.4,0.4,100,-0.4,0.4)
h_xy     = TH2F('h_xy','tx / ty all tracks',100,-0.4,0.4,100,-0.4,0.4)


def findGhost():
 found = False
 mc = evt['MC/Particles']
 vc = evt['Raw/Velo/Clusters']
 tr = evt[trackcontainer]
 if not tr : return False 
 clu2mc  = linkedTo(MCParticle,VeloCluster,'Raw/Velo/Clusters')   
 session().setParameter('modeling.userTrackRange','true') 
 session().setParameter('modeling.trackStartz','-500.')
 session().setParameter('modeling.trackEndz','1000.')
 session().setParameter('modeling.projection','-ZR')
 session().setParameter('modeling.lineWidth','3')  
 nghost = 0
 sel = 'Hlt1SingleHadronDecision'
 objs = tistostool.hltSelectionObjectSummaries(sel)
 tracklist = map(lambda x: x.summarizedObject(),objs)
# check for recvertex:
 sel = 'Hlt1DiHadronDecision'
 objs = tistostool.hltSelectionObjectSummaries(sel)
 can = map(lambda x: x.summarizedObject(),objs)
 for x in can:
    for t in x.tracks(): 
      tracklist.append(t.target())      
 sc = h_ntr.Fill(tr.size())
 for t in tracklist :  
  ss = 0
  ssrz = 0
  match = {}
  matchrz = {}
  for l in t.lhcbIDs():
   if not l.isVelo() : continue
   ss +=1
   lv = l.veloID()
   cl = vc.containedObject(lv.channelID())
   for m in clu2mc.range(cl) :
     if match.has_key(m.key()) :   match[m.key()]+=1
     else                      :   match[m.key()] =1  
     if cl.isRType()   :  
      ssrz+=1
      if matchrz.has_key(m.key()) :   matchrz[m.key()]+=1
      else                      :   matchrz[m.key()] =1     
  mlist = match.values()
  mlist.sort(reverse=True)
  maxl = 0
  if len(mlist)>0 : maxl = mlist[0]
  mlistrz = matchrz.values()
  mlistrz.sort(reverse=True)
  maxrz = 0
  if len(mlistrz)>0 : maxrz = mlistrz[0]
  fst = t.firstState()
  h_3d.Fill(float(maxl)/float(ss))
  test1 = 0
  if maxl > 3 : h_xy.Fill(fst.tx(),fst.ty())   
  r3d = float(maxl)/float(ss)
  rrz = float(maxrz)/float(ssrz)     
  if r3d < 0.7 : 
   h_rz.Fill(rrz)
   gconf = False
   for m in match.keys():
      if mc[m].particleID().abspid() == 11 : 
       gconf = True
       break
   if not gconf :  sc = h_rznog.Fill(rrz)   
   for m in matchrz.keys() : 
    if mc[m].particleID().abspid() == 11 : test1 = 11  
   if test1 != 11 : 
     h_rznoe.Fill(rrz)
   h_3dxy.Fill(fst.tx(),fst.ty())   
   nghost+=1
   print '========================================================================'
   print 'ghost found', t.key(),evt['Rec/Header'].evtNumber()
   print 'matching', match,   r3d
   print 'rz      ', matchrz, rrz
   maxratio = 0
# check for other track
   for m in matchrz :
    if matchrz[m] == mlistrz[0] :
# look only for the mcparticle with most of the links
       others = othertrack(m,t.key())
       maxratio = 0
       for pair in others:
        print 'other tracks found:',pair[0],pair[1]
        olist = pair[1].values()
        olist.sort(reverse=True)
        oaxl = 0
        oss  = 0
        for x in olist : oss+=x
        if oss == 0: 
         oss = 100
        else : 
         oaxl = olist[0]
        ratio = float(oaxl)/float(oss)
        if ratio > maxratio : maxratio = ratio
        print 'debug:',olist,oaxl,oss,ratio,maxratio
   h_other.Fill(maxratio)		
   if test1 != 11  : 
     print 'No electron involved ',test1 
   Style().setColor('green')     
   for m in match.keys() :                            
     mother = mc[m].mother()
     mkey = None
     mpid = 0
     if mother : 
      mkey = mother.key()
      motherid = ppSvc.find(mother.particleID()).name()  
      mpid = mother.particleID().abspid() 
     else : motherid = 'None'           
     print m,ppSvc.find(mc[m].particleID()).name(),mkey,motherid
     h_idparent.Fill(mpid)
     h_idparent.Fill(mpid,match[m])
     if pano: sc = Object_visualize(mc[m])   
   Style().setColor('cyan')
   if pano: sc = Object_visualize(t)
   Style().setColor('orange')
   for l in t.lhcbIDs():
    if not l.isVelo() : continue
    lv = l.veloID()
    cl = vc.containedObject(lv.channelID())
    if pano: sc = Object_visualize(cl)
   # try chi2 matching
   matched,tmatch,chi2min = chi2Match(t)   
   if matched : 
     print 'chi2 matching:',matched,tmatch,ppSvc.find(mc[tmatch].particleID()).name(),chi2min
     Style().setColor('magenta')
     if pano: sc = Object_visualize(mc[tmatch])
   found = True  
 if nghost==1 and test1 != 11 : h_rznoe1.Fill(rrz)   
 if nghost==1  : 
   h_rz1.Fill(rrz)   
   h_other1.Fill(maxratio)		
 h_ngh.Fill(nghost)
 return found 

def dumpMC(t):
  vc = evt['Raw/Velo/Clusters']  
  clu2mc  = linkedTo(MCParticle,VeloCluster,'Raw/Velo/Clusters')   
  match = {}
  for l in t.lhcbIDs():
   lv = l.veloID()
   cl = vc.containedObject(lv.channelID())
   for m in clu2mc.range(cl) :
     if match.has_key(m.key()) :   match[m.key()]+=1
     else                      :   match[m.key()] =1     
  mlist = match.values()
  Style().setColor('green')
  for m in match.keys() :                            
     if pano: sc = Object_visualize(mc[m])   

def run(): 
 for n in range(500):
  EraseEventAllRegions()
  appMgr.run(1)
  if not evt['Rec/Header']: break
  if findGhost() :
   break 

def plotClusters():
   Style().setColor('violet')
   data_collect(da(),'VeloCluster','isR==false')
   data_visualize(da())  
   Style().setColor('white')
   data_collect(da(),'VeloCluster','isR==true')
   data_visualize(da())

def othertrack(mkey,tkey):
 others = []
 mc = evt['MC/Particles']
 vc = evt['Raw/Velo/Clusters']
 clu2mc  = linkedTo(MCParticle,VeloCluster,'Raw/Velo/Clusters')   
 for t in evt[trackcontainer]:
   if t.key() == tkey : continue
   match = {}
   count = 0
   for l in t.lhcbIDs():
    if not l.isVelo() : continue
    lv = l.veloID()
    cl = vc.containedObject(lv.channelID())
    for m in clu2mc.range(cl) :
      if cl.isRType()   :  
       if match.has_key(m.key()) :   match[m.key()]+=1
       else                      :   match[m.key()] =1    
       if m.key() == mkey  : count+=1
   if count > 2 : others.append([t.key(),match]) 
 return others   
 
def chi2Match(t,flag=False):  
 fstate = t.firstState()
# ignore correlations 
 tx   = fstate.tx()
 ctx  = fstate.errTx2()    
 ty   = fstate.ty()
 cty  = fstate.errTy2()    
 chi2min = 999999.
 tmatch  = None
 for mcp in evt['MC/Particles']:
  pmom = mcp.momentum()
  sx = pmom.x()/(pmom.z()+0.00001)
  sy = pmom.y()/(pmom.z()+0.00001)    
  p  = pmom.r()
# ignore correlations 
  chi2x = (tx-sx)*(tx-sx)/ctx 
  chi2y = (ty-sy)*(ty-sy)/cty 
  chi2 = chi2x+chi2y 
  if t.type() == t.Long and flag :
     print 'should not be called',flag
     delp = abs((t.p()-p)/(p+0.00001))
     chi2+=(delp*p)/(TMath.Sqrt(fstate.errQOverP2())*t.p()*t.p())
  if chi2 < chi2min : 
       chi2min = chi2
       tmatch = mcp.key()              
 matched = False 
 if chi2min < 50 : matched = True    
 return matched,tmatch,chi2min

from ROOT import TFile
def saveHistos(): 
 f=TFile('VeloGhosts.root','recreate')
 sc = h_rz.Write()
 sc = h_rznoe.Write()
 sc = h_rznoe1.Write()
 sc = h_3d.Write()
 f.Close()
 
def wprint(format='JPEG',quality='100'): 
  fname = 'Ghost_'+str(evt['Rec/Header'].evtNumber())+'.jpg'   
  ui().synchronize()
  if format == 'wrl' :
    Region().write_wrl()
    os.rename('out.wrl',fname+'.wrl')
  elif format == 'hiv' :
    Region().write_hiv()
    os.rename('out.hiv',fname+'.hiv')
  else : 
   widget = ui().currentWidget()
   if widget :
#   format = "GL2PS"
#   format = "JPEG90"
   # in principle is available too :
#   format = "PS"     # pixmap PS.
#   format = "GIF"    # pixmap GIF
#   format = "JPEG<quality>" # pixmap. <quality> is an int in [0,100]. 
#Exa : JPEG80
#   format = "GL2PDF"
#   format = "GL2SVG"
#   format = "PSVec"
    print widget.write(fname,format,quality),    fname, ' with format ',format,' and quality',quality

   
   
def fmax(adict) : 
  mm    = -1
  mkey  = -1
  for m in adict.keys():
    if adict[m] > mm : 
      mm    = adict[m]
      mkey  = m   
  return mkey

def mysort(adict):
  bdict = {}
  while len(adict) > 0 :
   n =  fmax(adict)
   nn = int(h_idparent.GetBinCenter(n))
   print n,adict[n],ppSvc.find(ParticleID(nn)).name(),h_idparentw.GetBinContent(n)
   bdict[n] = adict[n]
   del adict[n] 
  return bdict 
   
def ids():
 pidlist = {}
 for n in range(2,h_idparent.GetNbinsX()):
   nn = int(h_idparent.GetBinCenter(n))
   pid = h_idparent.GetBinContent(n)
   if pid != 0 : 
     pidlist[n] = pid     
 spidlist = mysort(pidlist)    
 for n in spidlist.keys():
     nn = int(h_idparent.GetBinCenter(n))
     pid = h_idparent.GetBinContent(n)
     print ppSvc.find(ParticleID(nn)).name(),pid,h_idparentw.GetBinContent(n)

def particle_stat(): 
 h_idparent.Reset()
 for n in range(500):
  appMgr.run(1)
  for m in evt['MC/Particles']:
   oVx = m.originVertex().position()
   if oVx.rho() < 5. and abs(oVx.z())<200. : 
    mpid = m.particleID().abspid() 
    h_idparent.Fill(mpid)
       
def fclu2mc(t): 
 mc = evt['MC/Particles']
 vc = evt['Raw/Velo/Clusters']
 clu2mc  = linkedTo(MCParticle,VeloCluster,'Raw/Velo/Clusters')   
 for l in t.lhcbIDs():
    if not l.isVelo() : continue
    lv = l.veloID()
    cl = vc.containedObject(lv.channelID())
    text =  str(cl.channelID().channelID())
    for m in clu2mc.range(cl) :      
      text+= str(cl.isRType())+'  '+ str(m.key())
    print text   
  
