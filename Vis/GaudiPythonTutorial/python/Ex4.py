from ROOT import TH1F, TBrowser, TCanvas
    
# get the basic configuration from here
from LHCbConfig import *
lhcbApp.DataType = "2011"

appConf = ApplicationMgr(AppName = 'Ex4',OutputLevel = INFO)

import GaudiPython
# load some additional gadgets
from gaudigadgets import doxygen

appMgr = GaudiPython.AppMgr()
det    = appMgr.detSvc() 
lhcb   = det['/dd/Structure/LHCb']

# read one event to get right event time
sel    = appMgr.evtsel()
sel.open(['$PANORAMIXDATA/Sel_Bsmumu_2highest.dst']) 
evt = appMgr.evtsvc()
# read one event
appMgr.run(1)

velo   = det['/dd/Structure/LHCb/BeforeMagnetRegion/Velo']

doxygen(velo)

rSensor_10 = velo.rSensor(10)
print 'max radius of zone 0: ',rSensor_10.rMax(0)
print 'min radius of zone 0: ',rSensor_10.rMin(0)
print 'radius of strip 145: ',  rSensor_10.rOfStrip(145)

ac = velo.geometry().alignmentCondition()
doxygen(ac)
print ac.printParams()

module = det['/dd/Structure/LHCb/BeforeMagnetRegion/Velo/VeloLeft/Module00']
sensor = det['/dd/Structure/LHCb/BeforeMagnetRegion/Velo/VeloLeft/Module00/RPhiPair00/Detector-00']

