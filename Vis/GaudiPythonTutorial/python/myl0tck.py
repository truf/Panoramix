from Gaudi.Configuration import *
from Configurables import L0DUMultiConfigProvider,L0DUConfigProvider
l0DUConfig = L0DUMultiConfigProvider('L0DUConfig')
#------------------------- TCK = 0x0012 / Recipe name : MINBIAS_CaloOrMuonOrSpd_v2_0x0012
l0DUConfig.registerTCK += ["0x0012"]
l0DUConfig.addTool(L0DUConfigProvider,'TCK_0x0012')
l0DUConfig.TCK_0x0012.Name = "MINBIAS_CaloOrMuonOrSpd_v2_0x0012"
l0DUConfig.TCK_0x0012.Description ="(hadEt>10 and spdMult>1) or (mu1Pt>0) or (spdMult>10)"

l0DUConfig.TCK_0x0012.Conditions = [
[ "name=[Hadron,Et]"     , "data=[Hadron(Et)]" , "comparator=[>]" , "threshold=[10]" ],
[ "name=[SpdMult]"       , "data=[Spd(Mult)]"  , "comparator=[>]" , "threshold=[1]"  ],
[ "name=[SpdMult,GT,10]" , "data=[Spd(Mult)]"  , "comparator=[>]" , "threshold=[10]" ],
[ "name=[Muon1,Pt]"      , "data=[Muon1(Pt)]"  , "comparator=[>]" , "threshold=[0]"  ]
]

l0DUConfig.TCK_0x0012.Channels =[
  ["name=[GEC]"            , "rate==[100]", "conditions= [SpdMult]  && [PuMult] && [SumEt] && [PuPeak2] ", "DISABLE=[TRUE]" ],
  ["name=[Electron]"       , "rate==[100]", "conditions= [Electron,HighEt]    && [GEC] " ],
  ["name=[Photon]"         , "rate==[100]", "conditions= [Photon,HighEt]      && [GEC] " ],
  ["name=[Hadron]"         , "rate==[100]", "conditions= [Hadron,HighEt]      && [GEC] " ],
  ["name=[LocalPi0]"       , "rate==[100]", "conditions= [LocalPi0,HighEt]    && [GEC] " ],
  ["name=[GlobalPi0]"      , "rate==[100]", "conditions= [GlobalPi0,HighEt]   && [GEC] " ],
  ["name=[Muon]"           , "rate==[100]", "conditions= [Muon,HighPt]        && [GEC] " ],
  ["name=[DiMuon]"         , "rate==[100]", "conditions= [DiMuon,HighPt]      && [SumEt]"]
]




