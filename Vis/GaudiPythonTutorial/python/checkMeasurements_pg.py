debug    = False
veloOpen = False
max_Events = 500

from ROOT import TFile,TCanvas,TH1F,TH2F,TH3F,TBrowser,gROOT,TF1,gStyle,TText,TMinuit,gSystem,Double
import math

# get the basic configuration from here
from LHCbConfig import *
lhcbApp.DataType   = "2010"
lhcbApp.Simulation = True     
lhcbApp.DDDBtag   =  "head-20101206"
lhcbApp.CondDBtag =  "sim-20101210-vc-md100" 

if veloOpen:
    CondDB().LocalTags["SIMCOND"] = ["velo-open"]

importOptions('$ASSOCIATORSROOT/options/MCParticleToMCHit.py')
appConf = ApplicationMgr( OutputLevel = INFO, AppName = 'checkMeasurements' )

EventSelector().PrintFreq = 10

import GaudiPython
from gaudigadgets import *
from LinkerInstances.eventassoc import *

appMgr = GaudiPython.AppMgr()

sel  = appMgr.evtsel()
evt  = appMgr.evtsvc()
his  = appMgr.histsvc()
det  = appMgr.detsvc()
part = appMgr.ppSvc()

sel.open(['$PANORAMIXDATA/Sel_00006198_00000001_1.xdst'])

h_VeloHitMap         = TH2F('h_VeloHitMap',   ' Velo hit map x/y, MC midpoints',     100,-100.,100.,100,-100.,100.)           
h_midPointLocal_z    = TH2F('h_midPointLocal_z', ' midpoint local z for all sensors',120,-0.5,119.5, 100,-0.01,0.01)      
h_midPointGlobal_z   = TH2F('h_midPointGlobal_z', ' midpoint globalz - ztrans for all sensors',120,-0.5,119.5, 500,-0.5,0.5)      

h_VeloR_norm          = TH1F('h_VeloR_norm',   ' Velo R cluster norm',     100,0.,0.15)
h_VeloPhi_norm        = TH1F('h_VeloPhi_norm',   ' Velo Phi cluster norm',     100,0.,0.15)

h_VeloR_diff        = TH1F('h_VeloR_diff',   ' Velo R cluster - MC',     100,-0.2,0.2)
h_VeloPhi_diff      = TH1F('h_VeloPhi_diff', ' Velo Phi cluster - MC',   100,-0.2,0.2)
h_VeloR_pull        = TH1F('h_VeloR_pull',   ' Velo R cluster pull',     100,-5.,5.)
h_VeloPhi_pull      = TH1F('h_VeloPhi_pull', ' Velo Phi cluster pull',   100,-5.,5.)

h_VeloR_residual_r        = TH2F('h_VeloR_residual_r',     ' Velo R cluster - MC   vs. r ',    25,0.,50., 100,-0.1,0.1)
h_VeloR_residual_r_low    = TH2F('h_VeloR_residual_r_low', ' Velo R cluster - MC   vs. r low angles',    25,0.,50., 100,-0.1,0.1)
h_VeloR_residual_r_large  = TH2F('h_VeloR_residual_r_large',' Velo R cluster - MC   vs. r large angles', 25,0.,50., 100,-0.1,0.1)
h_VeloR_residual_r_large_oneStr  = TH2F('h_VeloR_residual_r_large_oneStr',' Velo R cluster - MC   vs. r large angles, one strip', 25,0.,50., 100,-0.1,0.1)
h_VeloPhi_residual_r      = TH2F('h_VeloPhi_residual_r',   ' Velo Phi cluster - MC vs. r ',    25,0.,50., 100,-0.1,0.1)
h_VeloR_residual_sl       = TH2F('h_VeloR_residual_sl',    ' Velo R cluster - MC   vs. tan(theta) ', 40,0.,0.4, 100,-0.1,0.1)
h_VeloR_residual_sl_back  = TH2F('h_VeloR_residual_sl_back',    ' Velo R cluster - MC   vs. tan(theta), backward tracks ', 40,0.,0.4, 100,-0.1,0.1)
h_VeloPhi_residual_sl     = TH2F('h_VeloPhi_residual_sl',  ' Velo Phi cluster - MC vs. tan(theta) ', 40,0.,0.4, 100,-0.1,0.1)
h_VeloR_residual_phi      = TH2F('h_VeloR_residual_phi',   ' Velo R cluster - MC   vs. phi ',  25,-3.2,3.2, 100,-0.1,0.1)
h_VeloPhi_residual_phi    = TH2F('h_VeloPhi_residual_phi', ' Velo Phi cluster - MC vs. phi ',  25,-3.2,3.2, 100,-0.1,0.1)

h_VeloR_residual_r_sp = TH2F('h_VeloR_residual_r_sp', ' Velo R cluster - MC small pitch vs. slope  ',  25,0.,0.3, 100,-0.1,0.1)

h_VeloR_residual_norm_sl     = TH3F('h_VeloR_residual_norm_sl',   ' Velo R cluster - MC   vs. tan(theta) ',  40,0.,0.4,12,40.,100., 100,-1.,1.)
h_VeloPhi_residual_norm_sl   = TH3F('h_VeloPhi_residual_norm_sl', ' Velo Phi cluster - MC vs. tan(theta) ',  40,0.,0.4,12,40.,100., 100,-1.,1.)

h_VeloR_residual_eta   = TH2F('h_VeloR_residual_eta',   ' Velo R cluster - MC   vs. eta ',  40,0.,1., 100,-1.,1.)
h_VeloPhi_residual_eta = TH2F('h_VeloPhi_residual_eta',' Velo Phi cluster - MC vs. eta ',  40,0.,1., 100,-1.,1.)
h_VeloR_residual_eta_odd   = TH2F('h_VeloR_residual_eta_odd',   ' Velo R cluster - MC   vs. eta odd ',  40,0.,1., 100,-1.,1.)
h_VeloPhi_residual_eta_odd  = TH2F('h_VeloPhi_residual_eta_odd', ' Velo Phi cluster - MC vs. eta odd',  40,0.,1., 100,-1.,1.)
h_VeloPhi_residual_eta_odd_inn  = TH2F('h_VeloPhi_residual_eta_odd_inn', ' Velo Phi cluster - MC vs. eta odd inn',  40,0.,1., 100,-1.,1.)
h_VeloPhi_residual_eta_odd_out  = TH2F('h_VeloPhi_residual_eta_odd_out', ' Velo Phi cluster - MC vs. eta odd out',  40,0.,1., 100,-1.,1.)
h_VeloPhi_residual_eta_even  = TH2F('h_VeloPhi_residual_eta_even', ' Velo Phi cluster - MC vs. eta even',  40,0.,1., 100,-1.,1.)
h_VeloR_residual_eta_diff   = TH1F('h_VeloR_residual_eta_diff',   ' (Velo R cluster - MC) - eta',  100,-1.,1.)
h_VeloR2_residual_eta_diff   = TH1F('h_VeloR2_residual_eta_diff',   ' (Velo R cluster 2 strip - MC) - eta',  100,-1.,1.)
h_VeloPhi_residual_eta_diff = TH1F('h_VeloPhi_residual_eta_diff', ' (Velo Phi cluster - MC) - eta',  100,-1.,1.)

h_VeloR_rdiff_r        = TH2F('h_VeloR_rdiff_r',     ' Velo R cluster rad - MC rad  vs. r ',  25,0.,50., 100,-0.1,0.1)
h_VeloR_rdiff_phi      = TH2F('h_VeloR_rdiff_phi',   ' Velo R cluster rad - MC rad  vs. phi ',  25,0.,50., 100,-0.1,0.1)

h_VeloR_residual_r_even         = TH2F('h_VeloR_residual_r_even',     ' Velo R cluster - MC   vs. r even sensors',  25,0.,50., 100,-0.1,0.1)
h_VeloPhi_residual_r_even       = TH2F('h_VeloPhi_residual_r_even',   ' Velo Phi cluster - MC vs. r even sensors',    25,0.,50., 100,-0.1,0.1)
h_VeloR_residual_sl_even        = TH2F('h_VeloR_residual_sl_even',     ' Velo R cluster - MC   vs. vs. tan(theta)  even sensors',40,0.,0.4,100,-0.1,0.1)
h_VeloPhi_residual_sl_even      = TH2F('h_VeloPhi_residual_sl_even',   ' Velo Phi cluster - MC vs. vs. tan(theta)  even sensors',40,0.,0.4,100,-0.1,0.1)
h_VeloR_residual_phi_even       = TH2F('h_VeloR_residual_phi_even',   ' Velo R cluster - MC   vs. phi ',25,-3.2,3.2, 100,-0.1,0.1)
h_VeloPhi_residual_phi_even     = TH2F('h_VeloPhi_residual_phi_even', ' Velo Phi cluster - MC vs. phi ',  25,-3.2,3.2, 100,-0.1,0.1)
h_VeloR_residual_r_odd          = TH2F('h_VeloR_residual_r_odd',     ' Velo R cluster - MC   vs. r odd sensors',  25,0.,50., 100,-0.1,0.1)
h_VeloPhi_residual_r_odd        = TH2F('h_VeloPhi_residual_r_odd',   ' Velo Phi cluster - MC vs. r odd sensors',    25,0.,50., 100,-0.1,0.1)
h_VeloR_residual_sl_odd        = TH2F('h_VeloR_residual_sl_odd',     ' Velo R cluster - MC   vs. vs. tan(theta)  odd sensors',40,0.,0.4,100,-0.1,0.1)
h_VeloPhi_residual_sl_odd      = TH2F('h_VeloPhi_residual_sl_odd',   ' Velo Phi cluster - MC vs. vs. tan(theta)  odd sensors',40,0.,0.4,100,-0.1,0.1)
h_VeloR_residual_r_odd_sect     = TH2F('h_VeloR_residual_r_odd_sect',     ' Velo R cluster - MC   vs. r odd sensors',  25,0.,50., 100,-0.1,0.1)
h_VeloR_residual_phi_odd_sect   = TH2F('h_VeloR_residual_phi_odd_sect',   ' Velo R cluster - MC vs. phi odd sensors',  25,-3.2,3.2, 100,-0.1,0.1)
h_VeloR_residual_phi_odd        = TH2F('h_VeloR_residual_phi_odd',   ' Velo R cluster - MC   vs. phi ',25,-3.2,3.2, 100,-0.1,0.1)
h_VeloPhi_residual_phi_odd      = TH2F('h_VeloPhi_residual_phi_odd', ' Velo Phi cluster - MC vs. phi ',  25,-3.2,3.2, 100,-0.1,0.1)
h_VeloPhi_residual_phi_odd_inn  = TH2F('h_VeloPhi_residual_phi_odd_inn', ' Velo Phi cluster - MC vs. phi ',  25,-3.2,3.2, 100,-0.1,0.1)
h_VeloPhi_residual_phi_odd_out  = TH2F('h_VeloPhi_residual_phi_odd_out', ' Velo Phi cluster - MC vs. phi ',  25,-3.2,3.2, 100,-0.1,0.1)

h_VeloR_pull_r        = TH2F('h_VeloR_pull_r',     ' Velo R cluster pull  vs. r ',  25,0.,50., 100,-5.,5.)
h_VeloPhi_pull_r      = TH2F('h_VeloPhi_pull_r', ' Velo Phi cluster pull vs. r ',    25,0.,50., 100,-5.,5.)
h_VeloR_pull_phi      = TH2F('h_VeloR_pull_phi',     ' Velo R cluster pull   vs. phi ',25,-3.2,3.2, 100,-5.,5.)
h_VeloPhi_pull_phi    = TH2F('h_VeloPhi_pull_phi', ' Velo Phi cluster pull vs. phi ',  25,-3.2,3.2, 100,-5.,5.)

h_dist_z_r    =   TH1F('h_dist_z_r', ' z distance of MCHit and Trajectory, r sensors', 100,-0.005,0.005)      
h_dist_z_r_odd   = TH1F('h_dist_z_r_odd', ' z distance of MCHit and Trajectory, phi sensors', 100,-0.005,0.005)      
h_dist_z_r_even  = TH1F('h_dist_z_r_even', ' z distance of MCHit and Trajectory, phi sensors', 100,-0.005,0.005)      
h_dist_z_phi  =   TH1F('h_dist_z_phi', ' z distance of MCHit and Trajectory, phi sensors', 100,-0.005,0.005)      
h_dist_z_phi_odd   = TH1F('h_dist_z_phi_odd', ' z distance of MCHit and Trajectory, phi sensors', 100,-0.005,0.005)      
h_dist_z_phi_even  = TH1F('h_dist_z_phi_even', ' z distance of MCHit and Trajectory, phi sensors', 100,-0.005,0.005)      
h_dist_z  =   TH1F('h_dist_z', ' z distance of MCHit and Trajectory', 100,-0.01,0.01)      
h_test    =   TH2F('h_test', ' my sign against distsign', 5,-4.5,4.5,5,-4.5,4.5)      

th2f_type = type(h_test)

hlist_sensr   = []
hlist_sensphi = []
for d in range(42) :
    name = 'h_VeloR_diff_'+str(d)
    hh = TH1F(name,   ' Velo R cluster - MC',     100,-0.2,0.2)
    hlist_sensr.append(hh)    
    name = 'h_VeloPhi_diff_'+str(d+64)
    hh = TH1F(name,   ' Velo Phi cluster - MC',   100,-0.2,0.2)
    hlist_sensphi.append(hh)    
  
gbl =   GaudiPython.gbl
XYZPoint    = LHCbMath.XYZPoint
XYZVector   = LHCbMath.XYZVector
Rotation3D  = gbl.Math.Rotation3D
EulerAngles = gbl.Math.EulerAngles
AxisAngle   = gbl.Math.AxisAngle
ParticleID  = gbl.LHCb.ParticleID

MCParticle   = gbl.LHCb.MCParticle
MCHit        = gbl.LHCb.MCHit
VeloCluster  = gbl.LHCb.VeloCluster
lhcbid       = gbl.LHCb.LHCbID
VeloChannelID       = gbl.LHCb.VeloChannelID
LineTraj     = gbl.LHCb.LineTraj        

velo  = det['/dd/Structure/LHCb/BeforeMagnetRegion/Velo']
poca  = appMgr.toolsvc().create('TrajPoca', interface='ITrajPoca')
velopotool = appMgr.toolsvc().create('VeloClusterPosition',interface='IVeloClusterPosition')

def inspect():                                                                                                      
  from ROOT import Double                                                                                           
  dx = Double()                                                                                                     
  dy = Double()                                                                                                     
  dz = Double()                                                                                                     
  det01_00 = det['/dd/Structure/LHCb/BeforeMagnetRegion/Velo/VeloRight/Module01/RPhiPair01/Detector-00']            
  own = det01_00.geometry().ownToNominalMatrix()                                                                    
  own.Translation().GetComponents(dx,dy,dz)                                                                         
  print dx,dy,dz                                                                                                    
  rotxyz = []                                                                                                       
  for k in range(9):                                                                                                
    rotxyz.append(Double())                                                                                         
  own.Rotation().GetComponents(rotxyz[0], rotxyz[1], rotxyz[2], rotxyz[3], rotxyz[4], rotxyz[5],                    
                      rotxyz[6], rotxyz[7], rotxyz[8])                                                              
  for k in range(9):                                                                                                
    print k, rotxyz[k]                                                                                              
  lv = det['/dd/Geometry/BeforeMagnetRegion/Velo/Sensors/lvVeloRDetector01']                                        
  pvs = lv.pvolumes()                                                                                               
  det01_01 = det['/dd/Structure/LHCb/BeforeMagnetRegion/Velo/VeloRight/Module01/RPhiPair01/Detector-01']            
  
def Velo_Alignment_info() :
 vecz  = {}
 vectS = {}
 for k in range(42) :
   vectS[k] = velo.sensor(k)
 for k in range(64,106) :
   vectS[k] = velo.sensor(k)
 for k in range(128,132) :
   vectS[k] = velo.sensor(k)    
 local =  XYZPoint()
 globl =  XYZPoint()
 print 'alignment const.  phi,psi,theta, x,  y,  z'
 rot = Rotation3D()
 tra = XYZVector()
 for k in vectS : 
  vectS[k].geometry().toGlobalMatrix().GetDecomposition(rot,tra)
  e=EulerAngles()*rot
  phi   = e.Phi()/math.pi*180
  psi   = e.Psi()/math.pi*180
  theta = e.Theta()/math.pi*180
  globl = vectS[k].geometry().toGlobal(local)
  print '%3d' %(vectS[k].sensorNumber()), phi,psi,theta,tra.x(),tra.y(),tra.z()
  ##print '%3d' %(vectS[k].sensorNumber()), phi,psi,theta,tra.x(),tra.y(),tra.z(),globl.z()
  vecz[k] = tra.z()
 return vecz
 
def fillHitmap():
 for h in evt['MC/Velo/Hits']:
   p = h.midPoint()
   sc = h_VeloHitMap.Fill(p.x(),p.y())

def midPointLocal():
  contmc = evt['MC/Velo/Hits']
  for h in contmc:
   en = h.entry()
   ex = h.exit()  
   delz = ex.z() - en.z()    
   if delz > 0.299 : 
       p = h.midPoint()
       sensdet  = velo.sensor(h.sensDetID())
       nr = h.sensDetID()
       p_loc    = sensdet.geometry().toLocal(p)
       #print 'det id:', nr,' local z of midpoint', p_loc.z()
       h_midPointLocal_z.Fill(nr,p_loc.z())
       h_midPointGlobal_z.Fill(nr,p.z() - vecz[nr]) 
       
def my_interstrip(cl) :
  interst = 0.
  total   = 0.
  max     = 0.
  for n in range(cl.size()) :
    adc = cl.adcValue(n)
    if adc > max : max = adc
  max = 0.99*max  
  for n in range(cl.size()) :
    adc = cl.adcValue(n)
    if adc > max : 
      interst += adc * n
      total+=adc
  interst = float(interst) / float(total)
  return interst

def my_eta(cl) :
  strip = 0
  max   = 0.
  for n in range(cl.size()) :
    adc = cl.adcValue(n)
    if adc > max : 
      max = adc
      strip = n
  eta = 0
  if cl.size() > 1 : 
    if n>0 : 
     eta = float(cl.adcValue(n-1))/float(cl.adcValue(n-1)+cl.adcValue(n))   
    else :
     eta = float(cl.adcValue(0))/float(cl.adcValue(0)+cl.adcValue(1))   
  return eta


def sz_interstrip(cl) :
  interst = 0
  sens = velo.sensor(cl.channelID().sensor())
  interstrip = -100
  centre = 0
  for n in range(cl.size()) :
    sens.channelDistance(VeloChannelID(cl.strip(0)),VeloChannelID(cl.strip(n)),interstrip)
    centre+=(interstrip)*cl.adcValue(n)
    if interstrip != n : print 'this should not happen',interstrip,n,cl.size()
  centre/=cl.totalCharge()
  intDistance=int(centre+0.5)
  fractionalPos=centre-intDistance
  return [fractionalPos,intDistance]
      

nevents = 0
vecz = Velo_Alignment_info()
while nevents < max_Events : 
  appMgr.run(1)
  nevents += 1
  if debug and nevents > 1 : break
  cont = evt['Raw/Velo/Clusters']
  if not cont : break      
  fillHitmap()
  midPointLocal()
####MC relation                                                       
  lvelohit  = linkedFrom(MCHit,MCParticle,'MC/Particles2MCVeloHits') 
  lvelopart = linkedTo(MCParticle,VeloCluster,'Raw/Velo/Clusters')   
  if lvelohit.notFound()>0  : 
   print ' no MCHit links found, program can stop'
   evt.dump()
   break
  if lvelopart.notFound()>0 : 
   print ' no MCParticle links found, program can stop'
   break 
  for cl in cont :
###take only one strip clusters  
###if cl.size()>1: continue
   vecpart = lvelopart.range(cl)
   clid    = cl.channelID().sensor()
   sensor  = velo.sensor(clid)
   if vecpart.size() == 1 and clid<128 :
####use only the simplest case, one mcparticle / cluster  
####there seems to be no MCHit for pile up sensors 
####take only MCparticles in the acceptance:
    pmc  = vecpart[0].momentum()
    oVx  = vecpart[0].originVertex()
    tx = pmc.px()/pmc.pz()
    ty = pmc.py()/pmc.pz()
    slope = math.sqrt(tx*tx+ty*ty)
    if pmc.r() < 1000. : continue
#    if oVx.position().rho() > 1.  : continue
    if abs(tx) < 0.4 and abs(ty) < 0.4 :   
     vechits = lvelohit.range(vecpart[0]) 
     if vechits.size() > 0 :
      velomchit = vechits[0]      
      success = -1
      for m in vechits : 
######## maybe check for sensitive det id and cluster channelID
        if m.sensDetID() == clid : 
         velomchit = m
         success = 1
         break  
###### get trajectory  
      if success == -1 :
         print 'no mchit found, should never happen',clid
      else : 
       clusInfo = velopotool.position(cl) 
       inters = clusInfo.fractionalPosition       
       strip  = VeloChannelID(cl.channelID().sensor(),cl.strip(0+int(inters)))       
       traj   = velo.trajectory(lhcbid(strip),inters-int(inters))
###### find minimum distance betweeen point and traj 
       p = XYZPoint(m.midPoint())
       dis = XYZVector()
       a  = Double(0.001)
       s  = Double(0.1+0.0)
       s2 = Double(0.1+0.0)
# calulate closest distance to MC midpoint
       success = poca.minimize(traj.get(),s,p,dis,a)    
# calulate closest distance to line between entry and exit point
#       mc_traj = LineTraj(m.exit(),m.entry())    
#       success = poca.minimize(traj.get(),s,mc_traj,s2,dis,a)    
       h_dist_z.Fill(dis.z())  
###       r = dis.r()
### just for testing
       r = dis.rho()
       signDist = -1
       if dis.Cross(traj.get().direction(s)).z() > 0.0 :
          signDist = 1
# testtesttets
       rmc = p.rho()
       rmr = traj.get().beginPoint().rho() 
       mysign = 1
       if rmc > rmr :
        mysign = -1          
       r = r*signDist
       rho_glob = p.rho()
       rdiff    = traj.get().position(0.5).rho() - rho_glob
# go to local coordinates for 2d histograms
       sensdet  = velo.sensor(cl.channelID().sensor())
       p_loc   = sensdet.geometry().toLocal(p)
       phi     = p_loc.phi()
       ## just for a test
       ## phi     = p.phi()
       rho     = p_loc.rho()  
       eta      = my_eta(cl)
       if cl.isRType() : 
       #  strip pitch
         norm = sensor.rPitch(cl.strip(0))  
         success = h_VeloR_norm.Fill(norm)    
         success = h_test.Fill(mysign,signDist)
         success = h_VeloR_diff.Fill(r)
         success = hlist_sensr[cl.channelID().sensor()].Fill(r)
         success = h_VeloR_residual_r.Fill(rho,r)  
         success = h_VeloR_residual_phi.Fill(phi,r)  
         success = h_VeloR_rdiff_r.Fill(rho,rdiff)  
         success = h_VeloR_rdiff_phi.Fill(phi,rdiff)  
         success = h_dist_z_r.Fill(dis.z())  
         success = h_VeloR_residual_sl.Fill(slope,r)
         success = h_VeloR_residual_norm_sl.Fill(slope,norm*1000.,r/norm)
         success = h_VeloR_residual_eta.Fill(eta,r/norm)
         ctre = r/norm - (-0.4*eta + 0.2)
         if eta == 0 : ctre = -1.1
         success = h_VeloR_residual_eta_diff.Fill(ctre)
         if cl.size() == 2 : success = h_VeloR2_residual_eta_diff.Fill(ctre)
         if pmc.z() < 0 : 
            success = h_VeloR_residual_sl_back.Fill(slope,r)
         if rho < 9. : 
            success = h_VeloR_residual_r_sp.Fill(slope,r)  
         if slope < 0.03 and slope > 0.01 : 
            success = h_VeloR_residual_r_low.Fill(rho,r)  
         if slope < 0.4 and slope > 0.3 : 
            success = h_VeloR_residual_r_large.Fill(rho,r)  
            if cl.size() == 1 :
              success = h_VeloR_residual_r_large_oneStr.Fill(rho,r)  

#### now look at the errors
         errMeasure  = clusInfo.fractionalError * velo.rSensor(clid).rPitch( cl.channelID().strip() ) 
         pull = r/errMeasure
         success = h_VeloR_pull.Fill(pull) 
         success = h_VeloR_pull_r.Fill(rho,pull)  
         success = h_VeloR_pull_phi.Fill(phi,pull)  
         if sensor.isRight() :
           if (sensor.isLeft() and math.fmod(clid,4) > 0) or (sensor.isRight() and math.fmod(clid-1,4) > 0) : 
             success = h_VeloR_residual_r_odd.Fill(rho,r)  
             success = h_VeloR_residual_sl_odd.Fill(slope,r)
             success = h_VeloR_residual_phi_odd.Fill(phi,r)  
             success = h_VeloR_residual_eta_odd.Fill(eta,r/norm)
             if sensor.zoneOfStrip(cl.strip(0)) == 0 :
                success = h_VeloR_residual_phi_odd_sect.Fill(phi,r) 
                success = h_VeloR_residual_r_odd_sect.Fill(rho,r) 
             success = h_dist_z_r_odd.Fill(dis.z())  
           else :
             success = h_VeloR_residual_r_even.Fill(rho,r)  
             success = h_VeloR_residual_sl_even.Fill(slope,r)
             success = h_VeloR_residual_phi_even.Fill(phi,r) 
             success = h_dist_z_r_even.Fill(dis.z())  
       elif cl.isPhiType() :    
       #  strip pitch
         norm = sensor.phiPitch(rho)
       # recalculate slope by multiplying with stereo angle
# the following will not work for Open Velo !!
         sdir  = traj.direction(0.5)
         ideal = XYZVector(p_loc.x(),p_loc.y(),0.)
         angle = ideal.Dot(sdir)/ideal.rho()
         if angle < 1. : 
           stereo = math.acos(ideal.Dot(sdir)/ideal.rho())
           slope = stereo * slope
         else :
           slope = 0.  
         success = h_VeloPhi_norm.Fill(norm)          
         success = h_VeloPhi_diff.Fill(r) 
         success = hlist_sensphi[cl.channelID().sensor()-64].Fill(r)
         success = h_VeloPhi_residual_r.Fill(rho,r)  
         success = h_VeloPhi_residual_phi.Fill(phi,r)  
         success = h_dist_z_phi.Fill(dis.z())  
         success = h_VeloPhi_residual_sl.Fill(slope,r)
         success = h_VeloPhi_residual_norm_sl.Fill(slope,norm*1000.,r/norm) 
         success = h_VeloPhi_residual_eta.Fill(eta,r/norm)
         ctre   = r/norm + (-0.78*eta + 0.39)
         if (sensor.isLeft() and math.fmod(clid,4) > 0) or (sensor.isRight() and math.fmod(clid-1,4) > 0) :
           ctre = r/norm - (-0.78*eta + 0.39)
         if eta == 0 : ctre = -1.1  
         success = h_VeloPhi_residual_eta_diff.Fill(ctre)
         if sensor.isRight() :
           if (sensor.isLeft() and math.fmod(clid,4) > 0) or (sensor.isRight() and math.fmod(clid-1,4) > 0) : 
             success = h_VeloPhi_residual_r_odd.Fill(rho,r)  
             success = h_VeloPhi_residual_phi_odd.Fill(phi,r)  
             success = h_VeloPhi_residual_sl_odd.Fill(slope,r)             
             success = h_VeloPhi_residual_eta_odd.Fill(eta,r/norm)
             success = h_dist_z_phi_odd.Fill(dis.z())  
             if rho > 18. : 
               success = h_VeloPhi_residual_phi_odd_out.Fill(phi,r)  
               success = h_VeloPhi_residual_eta_odd_out.Fill(eta,r/norm)
             else : 
               success = h_VeloPhi_residual_phi_odd_inn.Fill(phi,r)  
               success = h_VeloPhi_residual_eta_odd_inn.Fill(eta,r/norm)
           else :
             success = h_VeloPhi_residual_r_even.Fill(rho,r)  
             success = h_VeloPhi_residual_phi_even.Fill(phi,r) 
             success = h_dist_z_phi_even.Fill(dis.z())  
             success = h_VeloPhi_residual_sl_even.Fill(slope,r)  
             success = h_VeloPhi_residual_eta_even.Fill(eta,r/norm)
#### now look at the errors
         errMeasure  = clusInfo.fractionalError * velo.phiSensor(clid).phiPitch( rho )
         pull = r/errMeasure
         success = h_VeloPhi_pull.Fill(pull) 
         success = h_VeloPhi_pull_r.Fill(rho,pull)  
         success = h_VeloPhi_pull_phi.Fill(phi,pull)  


if debug : print 1/0

online = True         
f = TFile('checkMeasurements_pg.root','recreate')
for h in gROOT.GetList(): 
 print 'write ',h.GetName()
 h.Write()
f.Close()

g=TF1('g','gaus')
tc = TCanvas('tc','Velo Measurement checks',750,500)
tc.Divide(2,2)
tc.cd(1)
h_VeloR_residual_r.Draw()
tc.cd(2)
h_VeloR_residual_phi.Draw()
tc.cd(3)
h_VeloPhi_residual_r.Draw()
tc.cd(4)
h_VeloPhi_residual_phi.Draw()
tc.Update()
tc.Print('residual_2d.jpg')

hlist_res = []
try :
 if not online : 
  f.ReadAll()
  th2f_type = type(h_test)
  object_list = f.GetList() 
 else : 
  object_list = gROOT.GetList()  
except :
  object_list = gROOT.GetList() 
for h in object_list : 
 if (h.GetName().find('residual') > 0 or h.GetName().find('rdiff') > 0) and type(h)==th2f_type :  
  hlist_res.append(h)

for h in hlist_res :
 print h.GetTitle()
 g.SetParameter(0,100.)
 g.SetParameter(1,0.0)
 g.SetParameter(2,0.01) 
 h.FitSlicesY(g,0,-1,0,'Q')
 

hlist_res_1   = []
for h in hlist_res :
 key = h.GetName()+'_1'
 hnew = gROOT.FindObjectAny(key)
# overwrite histogram with Mean 
 hnew.SetTitle('Mean of residual')
 for n in range(1,h.GetNbinsX()) :
  test = h.ProjectionY('test',n,n)
  hnew.SetBinContent(n,test.GetMean())
  hnew.SetBinError(n,test.GetMeanError())  
 hlist_res_1.append(hnew)
hlist_res_2     = []
hlist_res_rms   = []
for h in hlist_res :
 key = h.GetName()+'_2'
 hnew = gROOT.FindObjectAny(key)
 hrms = TH1F(key.replace('_2','_rms'),h.GetTitle(),h.GetNbinsX(),h.GetBinLowEdge(1),h.GetBinLowEdge(h.GetNbinsX()+1))
# overwrite histogram with sigma 
 hnew.SetTitle('sigma of residual')
 hrms.SetTitle('rms of residual')
 for n in range(1,h.GetNbinsX()) :
  test = h.ProjectionY(key+'_proj',n,n)
  success = test.Fit('g','Q')
  hnew.SetBinContent(n,g.GetParameter(2))
  hnew.SetBinError(n,g.GetParError(2))  
  hrms.SetBinContent(n,test.GetRMS())
  hrms.SetBinError(n,test.GetRMSError())  
 hlist_res_2.append(hnew)
 hlist_res_rms.append(hrms)

for h in hlist_res_1 :
 h.SetMaximum(0.006)
 h.SetMinimum(-0.006)

h_VeloR_residual_r_1              = gROOT.FindObjectAny('h_VeloR_residual_r_1')
h_VeloR_residual_phi_1            = gROOT.FindObjectAny('h_VeloR_residual_phi_1')
h_VeloPhi_residual_phi_1          = gROOT.FindObjectAny('h_VeloPhi_residual_phi_1')
h_VeloPhi_residual_r_1            = gROOT.FindObjectAny('h_VeloPhi_residual_r_1')
h_VeloR_residual_phi_odd_1        = gROOT.FindObjectAny('h_VeloR_residual_phi_odd_1')
h_VeloR_residual_r_odd_1          = gROOT.FindObjectAny('h_VeloR_residual_r_odd_1')
h_VeloPhi_residual_phi_odd_1      = gROOT.FindObjectAny('h_VeloPhi_residual_phi_odd_1')
h_VeloPhi_residual_r_odd_1        = gROOT.FindObjectAny('h_VeloPhi_residual_r_odd_1')
h_VeloR_residual_r_odd_sect_1     = gROOT.FindObjectAny('h_VeloR_residual_r_odd_sect_1')
h_VeloR_residual_phi_odd_sect_1   = gROOT.FindObjectAny('h_VeloR_residual_phi_odd_sect_1')
h_VeloR_residual_phi_even_1       = gROOT.FindObjectAny('h_VeloR_residual_phi_even_1')
h_VeloR_residual_r_even_1         = gROOT.FindObjectAny('h_VeloR_residual_r_even_1')
h_VeloPhi_residual_phi_even_1     = gROOT.FindObjectAny('h_VeloPhi_residual_phi_even_1')
h_VeloPhi_residual_r_even_1       = gROOT.FindObjectAny('h_VeloPhi_residual_r_even_1')
h_VeloR_rdiff_r_1                 = gROOT.FindObjectAny('h_VeloR_rdiff_r_1')
h_VeloR_rdiff_phi_1               = gROOT.FindObjectAny('h_VeloR_rdiff_phi_1')
h_VeloPhi_residual_phi_odd_inn_1  = gROOT.FindObjectAny('h_VeloPhi_residual_phi_odd_inn_1')  
h_VeloPhi_residual_phi_odd_out_1  = gROOT.FindObjectAny('h_VeloPhi_residual_phi_odd_out_1')   

tc0 = TCanvas('tc0','Velo Measurement check bias',750,500)
tc0.Divide(2,2)
tc0.cd(1)
h_VeloR_residual_r_1.Draw()
tc0.cd(2)
h_VeloR_residual_phi_1.Draw()
tc0.cd(3)
h_VeloPhi_residual_r_1.Draw()
tc0.cd(4)
h_VeloPhi_residual_phi_1.Draw()
tc0.Update()
tc0.Print('residual_bias.jpg')

tce = TCanvas('tce','Velo Measurement check bias left and even',750,500)
tce.Divide(2,2)
tce.cd(1)
h_VeloR_residual_r_even_1.Draw()
tce.cd(2)
h_VeloR_residual_phi_even_1.Draw()
tce.cd(3)
h_VeloPhi_residual_r_even_1.Draw()
tce.cd(4)
h_VeloPhi_residual_phi_even_1.Draw()
tce.Update()

tco = TCanvas('tco','Velo Measurement check bias left and odd',750,500)
tco.Divide(2,2)
tco.cd(1)
h_VeloR_residual_r_odd_1.Draw()
tco.cd(2)
h_VeloR_residual_phi_odd_1.Draw()
tco.cd(3)
h_VeloPhi_residual_r_odd_1.Draw()
tco.cd(4)
h_VeloPhi_residual_phi_odd_1.Draw()
h_VeloPhi_residual_phi_odd_inn_1.SetMarkerColor(3)
h_VeloPhi_residual_phi_odd_inn_1.Draw('same') 
h_VeloPhi_residual_phi_odd_out_1.SetMarkerColor(4)
h_VeloPhi_residual_phi_odd_out_1.Draw('same')

tco.Update()

tcr = TCanvas('tcr','residual fits',800,1000)
tcr.Divide(4,5)
gStyle.SetOptFit(111)
npad = 1
for n in range(1,24) :
 test = h_VeloPhi_residual_r_odd.ProjectionY('test',n,n+1)
 test.SetTitle(h_VeloPhi_residual_r_odd.GetTitle())
 if test.GetEntries() > 50 : 
  tcr.cd(npad)
  npad +=1
  test.Fit(g)
  test.DrawCopy()
  ypos = test.GetMaximum()*0.1
  text = 'Mean='+'%4.2f'%(g.GetParameter(1)*1000.)+'um'
  tx = TText(-0.04,ypos,text)
  tx.DrawText(-0.04,ypos,text)

h_VeloR_residual_phi_2   = gROOT.FindObjectAny('h_VeloR_residual_phi_2')
h_VeloR_residual_r_2     = gROOT.FindObjectAny('h_VeloR_residual_r_2')
h_VeloPhi_residual_phi_2 = gROOT.FindObjectAny('h_VeloPhi_residual_phi_2')
h_VeloPhi_residual_r_2   = gROOT.FindObjectAny('h_VeloPhi_residual_r_2')

tc1 = TCanvas('tc1','Velo Measurement checks  sigma',750,500)
tc1.Divide(2,2)
tc1.cd(1)
h_VeloR_residual_r_2.Draw()
tc1.cd(2)
h_VeloR_residual_phi_2.Draw()
tc1.cd(3)
h_VeloPhi_residual_r_2.Draw()
tc1.cd(4)
h_VeloPhi_residual_phi_2.Draw()
tc1.Update()

tc2 = TCanvas('tc2','Velo Measurement checks',750,500)
tc2.Divide(2,1)
tc2.cd(1)
tc2.cd(1).SetLogy(1)
h_VeloPhi_diff.Fit(g)
tc2.cd(2)
tc2.cd(2).SetLogy(1)
h_VeloR_diff.Fit(g)

tp = TCanvas('tp','Velo Pull checks',750,500)
tp.Divide(2,2)
tp.cd(1)
h_VeloR_pull_r.Draw()
tp.cd(2)
h_VeloR_pull_phi.Draw()
tp.cd(3)
h_VeloPhi_pull_r.Draw()
tp.cd(4)
h_VeloPhi_pull_phi.Draw()
tp.Update()

tp2 = TCanvas('tp2','Velo Pull checks',750,500)
tp2.Divide(2,1)
tp2.cd(1)
tp2.cd(1).SetLogy(1)
h_VeloPhi_pull.Fit(g)
tp2.cd(2)
tp2.cd(2).SetLogy(1)
h_VeloR_pull.Fit(g)

h_VeloR_pull_phi.FitSlicesY(g,0,-1,0,'Q')
h_VeloR_pull_r.FitSlicesY(g,0,-1,0,'Q')
h_VeloPhi_pull_phi.FitSlicesY(g,0,-1,0,'Q')
h_VeloPhi_pull_r.FitSlicesY(g,0,-1,0,'Q')

h_VeloR_pull_phi_2 = gROOT.FindObjectAny('h_VeloR_pull_phi_2')
h_VeloR_pull_r_2 = gROOT.FindObjectAny('h_VeloR_pull_r_2')
h_VeloPhi_pull_phi_2 = gROOT.FindObjectAny('h_VeloPhi_pull_phi_2')
h_VeloPhi_pull_r_2 = gROOT.FindObjectAny('h_VeloPhi_pull_r_2')

tp1 = TCanvas('tp1','Velo Measurement checks  sigma 2',750,500)
tp1.Divide(2,2)
tp1.cd(1)
h_VeloR_pull_r_2.Draw()
tp1.cd(2)
h_VeloR_pull_phi_2.Draw()
tp1.cd(3)
h_VeloPhi_pull_r_2.Draw()
tp1.cd(4)
h_VeloPhi_pull_phi_2.Draw()
tp1.Update()

tce.Print('residual_bias_left_even.jpg')
tco.Print('residual_bias_left_odd.jpg')
tc1.Print('residual_randphil.jpg')
tc2.Print('residual_gauss.jpg')
tp.Print('pull_2d.jpg')
tp1.Print('pull_randphi.jpg')
tp2.Print('pull_gauss.jpg')
       
# new stuff, residuals as function of slope
tsl1 = TCanvas('tsk1','Velo Measurement checks  slope',900,500)
tsl1.Divide(4,2)    
h_VeloR_residual_sl_1 = gROOT.FindObjectAny('h_VeloR_residual_sl_1')
h_VeloR_residual_sl_odd_1 = gROOT.FindObjectAny('h_VeloR_residual_sl_odd_1')
h_VeloR_residual_sl_even_1 = gROOT.FindObjectAny('h_VeloR_residual_sl_even_1')
h_VeloR_residual_sl_back_1 = gROOT.FindObjectAny('h_VeloR_residual_sl_back_1')
h_VeloPhi_residual_sl_1 = gROOT.FindObjectAny('h_VeloPhi_residual_sl_1')
h_VeloPhi_residual_sl_odd_1 = gROOT.FindObjectAny('h_VeloPhi_residual_sl_odd_1')
h_VeloPhi_residual_sl_even_1 = gROOT.FindObjectAny('h_VeloPhi_residual_sl_even_1')
h_VeloR_residual_r_low_1 = gROOT.FindObjectAny('h_VeloR_residual_r_low_1')
h_VeloR_residual_r_large_1 = gROOT.FindObjectAny('h_VeloR_residual_r_large_1')
h_VeloR_residual_r_large_oneStr_1 = gROOT.FindObjectAny('h_VeloR_residual_r_large_oneStr_1')
tsl1.cd(1)
h_VeloR_residual_sl_1.Draw()
tsl1.cd(2)
h_VeloR_residual_sl_back_1.Draw()
tsl1.cd(3)
h_VeloR_residual_sl_odd_1.Draw()
tsl1.cd(4)
h_VeloPhi_residual_sl_1.Draw()
tsl1.cd(5)
h_VeloPhi_residual_sl_odd_1.Draw()
tsl1.cd(6)
h_VeloR_residual_r_low_1.Draw()
tsl1.cd(7)
h_VeloR_residual_r_large_1.Draw()
tsl1.cd(8)
h_VeloR_residual_r_large_oneStr_1.Draw()

tcz = TCanvas('tcz','mid point',900,500)
tcz.cd(1)
h_midPointLocal_z.Draw() 
tcz.Print('localz.jpg')

h_VeloR_residual_r_sp.Draw() 

#h_midPointGlobal_z.Draw()
