from ROOT import TH1F, TBrowser, TCanvas,Double
# get the basic configuration from here
from LHCbConfig import *
lhcbApp.DataType = "2011"

import GaudiKernel.SystemOfUnits as units

appConf = ApplicationMgr(AppName = 'Ex7c',OutputLevel = INFO)
appConf.ExtSvc += ['MagneticFieldSvc']
EventClockSvc().EventTimeDecoder = 'FakeEventTime'

import GaudiPython

appMgr = GaudiPython.AppMgr()
sel    = appMgr.evtsel()
det    = appMgr.detSvc() 
lhcb   = det['/dd/Structure/LHCb']
sel.open(['$PANORAMIXDATA/Sel_Bsmumu_2highest.dst']) 
appMgr.run(1)

magSvc = appMgr.service('MagneticFieldSvc','ILHCbMagnetSvc')
magSvc.fieldGrid().setScaleFactor(1.)

XYZPoint    = LHCbMath.XYZPoint
XYZVector   = LHCbMath.XYZVector

a = XYZPoint(0.,0.,5000.)
v = XYZVector()
magSvc.fieldVector(a,v)
print 'magnetic field Bx,By,Bz %3.2F,%3.2F,%3.2F at point x,y,z %3.4F,%3.4F,%3.4F'%(
   v.x()/units.gauss,v.y()/units.gauss,v.z()/units.gauss,a.x(),a.y(),a.z())
a = XYZPoint(0.,0.,0.)
v = XYZVector()
magSvc.fieldVector(a,v)
print 'magnetic field Bx,By,Bz %3.2F,%3.2F,%3.2F at point x,y,z %3.4F,%3.4F,%3.4F'%(
   v.x()/units.gauss,v.y()/units.gauss,v.z()/units.gauss,a.x(),a.y(),a.z())
a = XYZPoint(0.,0.,900.)
v = XYZVector()
magSvc.fieldVector(a,v)
print 'magnetic field Bx,By,Bz %3.2F,%3.2F,%3.2F at point x,y,z %3.4F,%3.4F,%3.4F'%(
   v.x()/units.gauss,v.y()/units.gauss,v.z()/units.gauss,a.x(),a.y(),a.z())

_ = raw_input('press enter to continue...')

bintegrator = appMgr.toolsvc().create('BIntegrator',interface='IBIntegrator')
Bdl = XYZVector()
zCenter = Double(0.)
b = XYZPoint(0.,0.,2500.)
result = bintegrator.calculateBdlAndCenter(a,b,0.,0.,zCenter,Bdl)
print 'Bdlx,Bdly,Bdlz = %3.4F,%3.4F,%3.4F'%(Bdl.x()/units.gauss,Bdl.y()/units.gauss,Bdl.z()/units.gauss)

def stepIntegration(z0,z1):
  Bdl = XYZVector()
  BDl = XYZVector()
  zCenter = Double(0.)
  dz  = 5. 
  z   = z0
  bdl = 0
  while z1 > z+dz:
   a = XYZPoint(0.,0.,z)
   b = XYZPoint(0.,0.,z+dz)
   result = bintegrator.calculateBdlAndCenter(a,b,0.,0.,zCenter,Bdl)
   z = z+dz  
   BDl = BDl+Bdl     
  a = XYZPoint(0.,0.,z)
  b = XYZPoint(0.,0.,z1)
  result = bintegrator.calculateBdlAndCenter(a,b,0.,0.,zCenter,Bdl)  
  BDl = BDl+Bdl     
  return BDl


# appMgr.stop() 
