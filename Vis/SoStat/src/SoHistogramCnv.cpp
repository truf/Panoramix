
// this :
#include "SoHistogramCnv.h"

// AIDA :
#include <AIDA/IHistogram1D.h>
#include <AIDA/IHistogram2D.h>

// Gaudi :
#include <GaudiKernel/ISvcLocator.h>
#include <GaudiKernel/IService.h>
#include <GaudiKernel/MsgStream.h>
#include <GaudiKernel/CnvFactory.h>
#include <GaudiKernel/DataObject.h>

// OnXSvc :
#include <OnXSvc/IUserInterfaceSvc.h>
#include <OnXSvc/ClassID.h>

// RootSvc :
#include <RootSvc/IRootSvc.h>

// HEPVis :
#include <HEPVis/nodekits/SoPlotterRegion.h>
#include <HEPVis/nodekits/SoPlotter.h>

// Lib :
#include "Lib/smanip.h"
#include "Lib/Interfaces/ISession.h"

// OnX :
#include <OnX/Interfaces/IWidget.h>
#include <OnX/Interfaces/IWidgetClass.h>

#include "GaudiKernel/HistogramBase.h"

DECLARE_CONVERTER_FACTORY(SoHistogramCnv)

//////////////////////////////////////////////////////////////////////////////
  SoHistogramCnv::SoHistogramCnv(
                                 ISvcLocator* aSvcLoc
                                 )
    :Converter(So_TechnologyType,CLID_Histogram,aSvcLoc)
    ,fUISvc(0)
    ,fRootSvc(0)
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
}
//////////////////////////////////////////////////////////////////////////////
StatusCode SoHistogramCnv::initialize(
)
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  StatusCode status = Converter::initialize();
  if( status.isFailure() ) return status;

  {
    if(fUISvc) {
      fUISvc->release();
      fUISvc = 0;
    }
    status = service("OnXSvc",fUISvc,true);
    if(status.isFailure() || !fUISvc) {
      MsgStream log(msgSvc(), "SoHistogramCnv");
      log << MSG::ERROR << " OnXSvc not found " << endmsg;
      return StatusCode::FAILURE;
    }
    fUISvc->addRef();
  }

  return status;
}
//////////////////////////////////////////////////////////////////////////////
StatusCode SoHistogramCnv::finalize()
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  if(fUISvc) {
    fUISvc->release();
    fUISvc = 0;
  }
  if(fRootSvc) {
    fRootSvc->release();
    fRootSvc = 0;
  }
  return StatusCode::SUCCESS;
}
//////////////////////////////////////////////////////////////////////////////
long SoHistogramCnv::repSvcType() const
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  return i_repSvcType();
}
//////////////////////////////////////////////////////////////////////////////
const CLID& SoHistogramCnv::classID(
)
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  return CLID_Histogram;
}
//////////////////////////////////////////////////////////////////////////////
unsigned char SoHistogramCnv::storageType(
)
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  return So_TechnologyType;
}

StatusCode SoHistogramCnv::plotWithSoPlotter(MsgStream& log,DataObject* aObject) {
  if(!fUISvc) {
    log << MSG::ERROR << " OnXSvc not found " << endmsg;
    return StatusCode::FAILURE;
  }

  ISession* session = fUISvc->session();
  if(!session) {
    log << MSG::ERROR << " can't get ISession." << endmsg;
    return StatusCode::FAILURE;
  }

  IWidget* widget = fUISvc->currentWidget();
  if(!widget) {
    log << MSG::ERROR << " can't get current widget." << endmsg;
    return StatusCode::FAILURE;
  }

  if( (widget->widgetClass().name()!="PageViewer") &&
      (widget->widgetClass().name()!="PlanePageViewer") ){
    log << MSG::INFO
        << " widget not a PageViewer or a PlanePageViewer." << endmsg;
    return StatusCode::FAILURE;
  }

  SoRegion* soRegion = fUISvc->currentSoRegion();
  if(!soRegion) {
    log << MSG::INFO << " can't get SoRegion." << endmsg;
    return StatusCode::FAILURE;
  }

  if(!soRegion->isOfType(SoPlotterRegion::getClassTypeId())) {
    log << MSG::INFO << " region is not a SoPlotterRegion." << endmsg;
    return StatusCode::FAILURE;
  }

  std::string value;
  bool append = false;
  session->parameterValue("OnXLab_tree.append",value);
  Lib::smanip::tobool(value,append);

  if(!append) soRegion->clear("");

  if(AIDA::IHistogram1D* h1 =
     dynamic_cast<AIDA::IHistogram1D*>(aObject)) {

    SbPlottableObject* sbPlottable =
      new SoHistogramRep1D(aObject->name(),h1);
    soRegion->doIt(SbPlotterDoIt(sbPlottable));

  } else if(AIDA::IHistogram2D* h2 =
            dynamic_cast<AIDA::IHistogram2D*>(aObject)) {

    SbPlottableObject* sbPlottable =
      new SoHistogramRep2D(aObject->name(),h2);
    soRegion->doIt(SbPlotterDoIt(sbPlottable));

  } else {
    log << MSG::INFO
        << " today only plotting of AIDA::IHistogram1D,2D supported."
        << endmsg;
    return StatusCode::FAILURE;
  }

  return StatusCode::SUCCESS;
}

StatusCode SoHistogramCnv::plotWithTCanvas(MsgStream& log,DataObject* aObject) {
  // attempt to plot with a ROOT TCanvas, if there is one around.

  if(!fRootSvc) {
    StatusCode status = service("RootSvc",fRootSvc,true);
    if(status.isFailure()  || !fRootSvc) {
      log << MSG::INFO << " RootSvc not found " << endmsg;
      return StatusCode::FAILURE;
    }
    fRootSvc->addRef();
  }

  if(AIDA::IHistogram* histogram = dynamic_cast<AIDA::IHistogram*>(aObject)) {

    fRootSvc->visualize(*histogram);

  } else {
    log << MSG::INFO
        << " object not an AIDA::IHistogram."
        << endmsg;
    return StatusCode::FAILURE;
  }

  return StatusCode::SUCCESS;
}

//////////////////////////////////////////////////////////////////////////////
StatusCode SoHistogramCnv::createRep(
                                     DataObject* aObject
                                     ,IOpaqueAddress*&
                                     )
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  MsgStream log(msgSvc(), "SoHistogramCnv");
  log << MSG::INFO << "Histogram createReps" << endmsg;

  if(!aObject) {
    log << MSG::INFO << " NULL object." << endmsg;
    return StatusCode::FAILURE;
  }

  if(!fUISvc) {
    log << MSG::INFO << " OnXSvc not found " << endmsg;
    return StatusCode::FAILURE;
  }

  ISession* session = fUISvc->session();
  if(!session) {
    log << MSG::INFO << " can't get ISession." << endmsg;
    return StatusCode::FAILURE;
  }

  std::string value;
  session->parameterValue("plotting.plotter",value);

  return ( value=="TCanvas" ?  
           plotWithTCanvas(log,aObject) :
           plotWithSoPlotter(log,aObject) );
}
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
// AIDA :
#include <AIDA/IHistogram1D.h>
#include <AIDA/IHistogram2D.h>
#include <AIDA/IAxis.h>
#include <AIDA/IAnnotation.h>

// HEPVis :
#include <HEPVis/SbPlottableThings.h>

// Lib:
#include <Lib/smanip.h>

//////////////////////////////////////////////////////////////////////////////
SoHistogramRep1D::SoHistogramRep1D(
                                   const std::string& aName
                                   ,const AIDA::IHistogram1D* aHistogram
                                   )
  :fName(aName)
  ,fHistogram(aHistogram)
  ,fDataClass("AIDA::IHistogram1D")
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
}
//////////////////////////////////////////////////////////////////////////////
SoHistogramRep1D::~SoHistogramRep1D(
)
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
}
//////////////////////////////////////////////////////////////////////////////
bool SoHistogramRep1D::isValid(
) const
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  return true;
}
//////////////////////////////////////////////////////////////////////////////
const char* SoHistogramRep1D::getName(
)
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  return fName.c_str();
}
//////////////////////////////////////////////////////////////////////////////
const char* SoHistogramRep1D::getLabel(
)
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  fTitle = fHistogram->title();
  return fTitle.c_str();
}
//////////////////////////////////////////////////////////////////////////////
int SoHistogramRep1D::getDimension(
) const
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  return fHistogram->dimension();
}
//////////////////////////////////////////////////////////////////////////////
void* SoHistogramRep1D::cast(
                             const char* aClass
                             ) const
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  if(std::string(aClass)=="SbPlottableObject") {
    return (SbPlottableObject*)this;
  } else if(std::string(aClass)=="SbPlottableBins1D") {
    return (SbPlottableBins1D*)this;
  } else {
    return 0;
  }
}
//////////////////////////////////////////////////////////////////////////////
void* SoHistogramRep1D::nativeObject(
) const
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  return (void*)fHistogram;
}
//////////////////////////////////////////////////////////////////////////////
const char* SoHistogramRep1D::nativeObjectClass(
) const
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  return fDataClass.c_str();
}
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
int SoHistogramRep1D::getAxisNumberOfBins(
) const
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  return fHistogram->axis().bins();
}
//////////////////////////////////////////////////////////////////////////////
float SoHistogramRep1D::getAxisMinimum(
) const
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  return (float)fHistogram->axis().lowerEdge();
}
//////////////////////////////////////////////////////////////////////////////
float SoHistogramRep1D::getAxisMaximum(
) const
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  return (float)fHistogram->axis().upperEdge();
}
//////////////////////////////////////////////////////////////////////////////
void SoHistogramRep1D::getBinsSumOfWeightsRange(
                                                float& aMin
                                                ,float& aMax
                                                ) const
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  aMin = (float)fHistogram->minBinHeight();
  aMax = (float)fHistogram->maxBinHeight();
}
//////////////////////////////////////////////////////////////////////////////
int SoHistogramRep1D::getBinNumberOfEntries(
                                            int aI
                                            ) const
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  return fHistogram->binEntries(aI);
}
//////////////////////////////////////////////////////////////////////////////
float SoHistogramRep1D::getBinLowerEdge(
                                        int aI
                                        ) const
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  return (float)fHistogram->axis().binLowerEdge(aI);
}
//////////////////////////////////////////////////////////////////////////////
float SoHistogramRep1D::getBinUpperEdge(
                                        int aI
                                        ) const
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  return (float)fHistogram->axis().binUpperEdge(aI);
}
//////////////////////////////////////////////////////////////////////////////
float SoHistogramRep1D::getBinSumOfWeights(
                                           int aI
                                           ) const
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  return (float)fHistogram->binHeight(aI);
}
//////////////////////////////////////////////////////////////////////////////
float SoHistogramRep1D::getBinBar(
                                  int aI
                                  ) const
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  return (float)fHistogram->binError(aI);
}
//////////////////////////////////////////////////////////////////////////////
const char* SoHistogramRep1D::getInfos(
                                       const char* /* aOptions */
                                       )
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  fInfos.clear();
  //FIXME : have to handle aOptions in order to fill correctly
  //        the info / stat box. Need to have something similar to :
  //          OnXLab::PlottableHistogram1D::getInfos
  return fInfos.c_str();
}
//////////////////////////////////////////////////////////////////////////////
const char* SoHistogramRep1D::getLegend(
)
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  fLegend = fHistogram->annotation().value("Legend");
  return fLegend.c_str();
}
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
SoHistogramRep2D::SoHistogramRep2D(
                                   const std::string& aName
                                   ,const AIDA::IHistogram2D* aHistogram
                                   )
  :fName(aName)
  ,fHistogram(aHistogram)
  ,fDataClass("AIDA::IHistogram2D")
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
}
//////////////////////////////////////////////////////////////////////////////
SoHistogramRep2D::~SoHistogramRep2D(
)
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
}
//////////////////////////////////////////////////////////////////////////////
bool SoHistogramRep2D::isValid(
) const
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  return true;
}
//////////////////////////////////////////////////////////////////////////////
const char* SoHistogramRep2D::getName(
)
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  return fName.c_str();
}
//////////////////////////////////////////////////////////////////////////////
const char* SoHistogramRep2D::getLabel(
)
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  fTitle = fHistogram->title();
  return fTitle.c_str();
}
//////////////////////////////////////////////////////////////////////////////
int SoHistogramRep2D::getDimension(
) const
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  return fHistogram->dimension();
}
//////////////////////////////////////////////////////////////////////////////
void* SoHistogramRep2D::nativeObject(
) const
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  return (void*)fHistogram;
}
//////////////////////////////////////////////////////////////////////////////
const char* SoHistogramRep2D::nativeObjectClass(
) const
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  return fDataClass.c_str();
}
//////////////////////////////////////////////////////////////////////////////
void* SoHistogramRep2D::cast(
                             const char* aClass
                             ) const
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  if(std::string(aClass)=="SbPlottableObject") {
    return (SbPlottableObject*)this;
  } else if(std::string(aClass)=="SbPlottableBins2D") {
    return (SbPlottableBins2D*)this;
  } else {
    return 0;
  }
}
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
int SoHistogramRep2D::getAxisNumberOfBinsX(
) const
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  return fHistogram->xAxis().bins();
}
//////////////////////////////////////////////////////////////////////////////
float SoHistogramRep2D::getAxisMinimumX(
) const
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  return (float)fHistogram->xAxis().lowerEdge();
}
//////////////////////////////////////////////////////////////////////////////
float SoHistogramRep2D::getAxisMaximumX(
) const
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  return (float)fHistogram->xAxis().upperEdge();
}
//////////////////////////////////////////////////////////////////////////////
int SoHistogramRep2D::getAxisNumberOfBinsY(
) const
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  return fHistogram->yAxis().bins();
}
//////////////////////////////////////////////////////////////////////////////
float SoHistogramRep2D::getAxisMinimumY(
) const
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  return (float)fHistogram->yAxis().lowerEdge();
}
//////////////////////////////////////////////////////////////////////////////
float SoHistogramRep2D::getAxisMaximumY(
) const
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  return (float)fHistogram->yAxis().upperEdge();
}
//////////////////////////////////////////////////////////////////////////////
void SoHistogramRep2D::getBinsSumOfWeightsRange(
                                                float& aMin
                                                ,float& aMax
                                                ) const
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  aMin = (float)fHistogram->minBinHeight();
  aMax = (float)fHistogram->maxBinHeight();
}
//////////////////////////////////////////////////////////////////////////////
int SoHistogramRep2D::getBinNumberOfEntries(
                                            int aI
                                            ,int aJ
                                            ) const
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  return fHistogram->binEntries(aI,aJ);
}
//////////////////////////////////////////////////////////////////////////////
float SoHistogramRep2D::getBinLowerEdgeX(
                                         int aI
                                         ) const
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  return (float)fHistogram->xAxis().binLowerEdge(aI);
}
//////////////////////////////////////////////////////////////////////////////
float SoHistogramRep2D::getBinUpperEdgeX(
                                         int aI
                                         ) const
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  return (float)fHistogram->xAxis().binUpperEdge(aI);
}
//////////////////////////////////////////////////////////////////////////////
float SoHistogramRep2D::getBinLowerEdgeY(
                                         int aJ
                                         ) const
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  return (float)fHistogram->yAxis().binLowerEdge(aJ);
}
//////////////////////////////////////////////////////////////////////////////
float SoHistogramRep2D::getBinUpperEdgeY(
                                         int aJ
                                         ) const
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  return (float)fHistogram->yAxis().binUpperEdge(aJ);
}
//////////////////////////////////////////////////////////////////////////////
float SoHistogramRep2D::getBinSumOfWeights(
                                           int aI
                                           ,int aJ
                                           ) const
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  return (float)fHistogram->binHeight(aI,aJ);
}
//////////////////////////////////////////////////////////////////////////////
float SoHistogramRep2D::getBinBar(
                                  int aI
                                  ,int aJ
                                  ) const
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  return (float)fHistogram->binError(aI,aJ);
}
//////////////////////////////////////////////////////////////////////////////
const char* SoHistogramRep2D::getInfos(
                                       const char* /* aOptions */
                                       )
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  fInfos.clear();
  //FIXME : have to handle aOptions in order to fill correctly
  //        the info / stat box. Need to have something similar to :
  //          OnXLab::PlottableHistogram2D::getInfos
  return fInfos.c_str();
}
//////////////////////////////////////////////////////////////////////////////
const char* SoHistogramRep2D::getLegend(
)
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  fLegend = fHistogram->annotation().value("Legend");
  return fLegend.c_str();
}
