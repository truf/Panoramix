// ============================================================================
// CVS tag $Name: not supported by cvs2svn $
// ============================================================================
// $Log: not supported by cvs2svn $
// Revision 1.8  2008/02/05 13:56:59  gybarran
// G.Barrand : rm OSC_VERSION_16_0 logic
//
// Revision 1.7  2007/02/02 16:52:21  truf
// *** empty log message ***
//
// Revision 1.6  2006/12/07 09:00:35  gybarran
// G.Barrand : optimize iterator and getting of the currentRegion
//
// Revision 1.5  2006/10/24 12:51:09  gybarran
// G.Barrand : prepare OSC-16
//
// Revision 1.4  2003/06/24 14:05:40  barrand
// *** empty log message ***
//
// Revision 1.3  2002/10/15 14:57:30  barrand
// G.Barrand : migrate to Lib/v5 and HEPVis/v6r1
//
// Revision 1.2  2002/09/11 07:00:58  barrand
// G.Barrand : update according new event model and OnX/v11
//
// Revision 1.1.1.1  2001/10/17 18:26:06  ibelyaev
// New Package: Visualization of Calorimeter objects 
//
// ============================================================================
#ifndef SOCALO_CALODIGITTYPE_H 
#define SOCALO_CALODIGITTYPE_H 1
// STD & STL 
#include <string>
// Lib & OnX 
#include <Lib/Interfaces/IPrinter.h>
#include <Lib/Interfaces/IIterator.h>
#include <Lib/Compiler.h>
#include <OnX/Core/BaseType.h>
// SoCalo
#include "CaloBaseType.h"
#include "Event/CaloDigit.h"

/** @class CaloDigitType CaloDigitType.h
 *  
 *  "So"-representation of CaloDigit object 
 *
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
 *  @date   10/10/2001
 */
class CaloDigitType: public     OnX::BaseType   ,
                     protected  CaloBaseType 
{
public:

  /** standard constructor 
   *  @param Type    "So"-type name 
   *  @param SvcLoc  pointer to Service Locator 
   *  @param Printer reference to a printer.
   */
  CaloDigitType( const std::string& Type   , 
                 ISvcLocator*       SvcLoc ,
		 IPrinter& Printer);
  virtual ~CaloDigitType( ); ///< Destructor
  
public: //Lib::IType
  virtual std::string name() const ;
  virtual Lib::IIterator* iterator();
  virtual Lib::Variable value( Lib::Identifier,const std::string&,void* );
public: //OnX::IType
  virtual void beginVisualize();
  virtual void visualize      ( Lib::Identifier ,void*);
  virtual void endVisualize();
private:
  void visualizeMCParticle(LHCb::CaloDigit&);
};

// ============================================================================
#endif ///< SOCALO_CALODIGITTYPE_H
// ============================================================================
