// ============================================================================
// CVS tag $Name: not supported by cvs2svn $ 
// ============================================================================
// $Log: not supported by cvs2svn $
// Revision 1.8  2008/07/28 08:13:33  truf
// just to please cmt/cvs, no changes
//
// Revision 1.7  2007/02/02 15:48:29  ranjard
// v8r0 - fix to use new PluginManager
//
// Revision 1.6  2006/12/07 10:01:07  gybarran
// G.Barrand : handle modeling solid, wire_frame
//
// Revision 1.5  2006/12/07 09:01:32  gybarran
// G.Barrand : handle coloring and other vis attributes. Have correct data-accessor name for picking
//
// Revision 1.4  2004/02/05 09:04:43  gybarran
// modifs for GAUDI_v14r1, LHCB_v15r1
//
// Revision 1.3  2003/03/10 10:46:47  barrand
// G.Barrand : fixes for g++ 3.2
//
// Revision 1.2  2002/09/11 07:00:58  barrand
// G.Barrand : update according new event model and OnX/v11
//
// Revision 1.1.1.1  2001/10/17 18:26:07  ibelyaev
// New Package: Visualization of Calorimeter objects 
// 
// ============================================================================
#ifndef     SOCALO_SOCALODIGITCNV_H 
#define     SOCALO_SOCALODIGITCNV_H 1 
// STD and STL 
#include <vector>
#include <string> 
// Calo 
#include "SoCaloBaseCnv.h"

// forward decalarations 
template <class CNV>
class CnvFactory;
class DeCalorimeter; 

/**  @class SoCaloDigitCnv  SoCaloDigitCnv.h 
 *     
 *   Converter for visualization of  
 *   container of CaloDigit objects
 *   
 *   @author  Vanya Belyaev 
 *   @date    18/04/2001 
 */

class SoCaloDigitCnv : public SoCaloBaseCnv 
{
  ///
  //friend class CnvFactory<SoCaloDigitCnv>; 
  ///
public:
  ///
  /// standard initialization method 
  virtual StatusCode initialize ();
  /// standard finalization  method 
  virtual StatusCode finalize   ();

  virtual long repSvcType() const; 

  /// the only one essential method 
  virtual StatusCode createRep( DataObject* /* Object */ , 
                                IOpaqueAddress*&  /* Address */ );
  /// Class ID for created object == class ID for this specific converter
  static const CLID&         classID     ();
  /// storage Type 
  static unsigned char storageType () ; 
  ///
  //protected:
  ///
  /// standard constructor 
  SoCaloDigitCnv( ISvcLocator* svcLoc ); 
  /// virtual destructor   
  virtual ~SoCaloDigitCnv();
  /// helpful methos to save typing 
  StatusCode  locateCalo( const unsigned int calo ); 
  /// accessor to calorimeter 
  const DeCalorimeter* calorimeter( const unsigned int calo );
  ///
private: 
  ///
  /// default constructor is disabled 
  SoCaloDigitCnv           (                       ) ;
  /// copy constructor is disabled 
  SoCaloDigitCnv           ( const SoCaloDigitCnv& ) ;
  /// assignment is disabled 
  SoCaloDigitCnv& operator=( const SoCaloDigitCnv& ) ; 
  ///
  std::string attribute ( const std::string& att ) const;
  bool modelingSolid ( ) const;
private:
  ///
  /// located calorimeter objects 
  std::vector<const DeCalorimeter*>   m_calos; 
  ///
};



// ============================================================================
#endif  //  SOCALO_SOCALODIGITCNV_H 
// ============================================================================













