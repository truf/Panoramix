################################################################################
# Package: RootSvc
################################################################################
gaudi_subdir(RootSvc v2r12)

gaudi_depends_on_subdirs(GaudiKernel)

find_package(AIDA)
find_package(ROOT COMPONENTS Hist)

gaudi_add_module(RootSvc
                 source/Component/RootSvc.cpp
                 INCLUDE_DIRS AIDA ROOT
                 LINK_LIBRARIES ROOT GaudiKernel)

gaudi_add_dictionary(RootSvc
                     dict/dictionary.h
                     dict/dictionary.xml
                     INCLUDE_DIRS AIDA ROOT
                     LINK_LIBRARIES ROOT GaudiKernel)

gaudi_install_headers(RootSvc)

