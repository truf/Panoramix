#ifndef IRootSvc_h
#define IRootSvc_h

// Inheritance :
#include <GaudiKernel/IService.h>

static const InterfaceID IID_IRootSvc(346, 1, 0); 

namespace AIDA {class IHistogram;}

class IRootSvc : virtual public IService {
public:
  virtual ~IRootSvc(){}

  static const InterfaceID& interfaceID() { return IID_IRootSvc; }

  virtual StatusCode visualize(const AIDA::IHistogram&) = 0;
};			

#endif
