#ifndef RootSvc_WinTCanvas_h
#define RootSvc_WinTCanvas_h

// Inheritance :
#include <OnX/Win/WinTk.h>

class TCanvas;

namespace WinTk {

class RootCanvas : public Component {
public:
  static void startTimer();
  static void stopTimer();
public:
  RootCanvas(Component&,const std::string&,const std::string& = "");
  virtual ~RootCanvas();
  //virtual void show();
  TCanvas* canvas() const;
  void clear();
  void update();
private:
  static LRESULT CALLBACK proc(HWND,UINT,WPARAM,LPARAM);
  static void CALLBACK timerProc(HWND,UINT,UINT,DWORD);
private:
  int fWID;
  TCanvas* fCanvas;
};

}

#endif
