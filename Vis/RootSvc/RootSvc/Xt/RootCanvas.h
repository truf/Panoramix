//
//  The author (G.Barrand) does not authorize anybody
// to deposit this code in the ROOT repository.
// The ROOT team is not authorize to integrate and 
// distribute this code. G.Barrand. 21/05/2005.
//

#ifndef Rox_RootCanvas_h
#define Rox_RootCanvas_h

#include <X11/Intrinsic.h>

typedef struct _RootCanvasClassRec* RootCanvasWidgetClass;
typedef struct _RootCanvasRec* RootCanvasWidget;

typedef struct {
 int reason;
 XEvent* event;
} RoxAnyCallbackStruct;

extern WidgetClass rootCanvasWidgetClass;

class TCanvas;

TCanvas* RootCanvasGetTCanvas(Widget);
void RootCanvasClear(Widget);
void RootCanvasUpdate(Widget);

void RootCanvasStartTimer(XtAppContext);
void RootCanvasStopTimer(XtAppContext);

void RootCanvasClearClass();

#endif
