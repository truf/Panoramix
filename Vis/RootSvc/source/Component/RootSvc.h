#ifndef RootSvc_RootSvc_h
#define RootSvc_RootSvc_h

// Inheritance :
#include <GaudiKernel/Service.h>
#include <RootSvc/IRootSvc.h>

template <typename T> class SvcFactory;

class RootSvc 
:public Service 
,virtual public IRootSvc
{ 
public:	//IInterface
  virtual StatusCode queryInterface(const InterfaceID&,void**);
public: //IService
  virtual StatusCode initialize();
  virtual StatusCode finalize();
public: //IRootSvc
  virtual StatusCode visualize(const AIDA::IHistogram&);
  //protected:
  RootSvc(const std::string&,ISvcLocator*);
  virtual ~RootSvc();
  //friend class SvcFactory<RootSvc>;
};

#endif
