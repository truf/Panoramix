#include <OnXSvc/IUserInterfaceSvc.h>
#include <OnXSvc/ISoConversionSvc.h>
#include <GaudiKernel/IRunable.h>

template <class T> struct OnXSvc_Interface {
  OnXSvc_Interface() {}
  static T* cast(IInterface* in) {
    void* out = 0;
    if ( in->queryInterface(T::interfaceID(), &out ).isSuccess() ){
       return (T*)out;
      }
    else return 0;
  }
};

//--- Templace instantiations
struct OnXSvc_dict_Instantiations 
{
  std::vector<Gaudi::XYZPoint> m_std_vector_Gaudi_XYZPoint;
  std::allocator<Gaudi::XYZPoint> m_std_allocator_Gaudi_XYZPoint;

  OnXSvc_Interface<IRunable> m_OnXSvc_Interface_IRunable;
  OnXSvc_Interface<IUserInterfaceSvc> m_OnXSvc_Interface_IUserInterfaceSvc;
  OnXSvc_Interface<ISoConversionSvc> m_OnXSvc_Interface_ISoConversionSvc;

};
