#ifndef OnXSvc_Filter_h
#define OnXSvc_Filter_h

#include <Slash/Data/IHandlersIterator.h>
#include <Slash/Data/IProcessor.h>
#include <Slash/Data/IIterator.h>
#include <Slash/Data/IHandlersIterator.h>
#include <Slash/Data/IHandler.h>

#include <OnXSvc/IUserInterfaceSvc.h>
#include <GaudiKernel/MsgStream.h>
#include <GaudiKernel/KeyedContainer.h>

template <class Object> 
class Filter {
public:
  typedef KeyedContainer<Object, Containers::HashMap> container;
  Filter(IUserInterfaceSvc& aUISvc,MsgStream& aLog)
  :fUISvc(aUISvc),fLog(aLog)
  ,fTypeManager(0),fType(0),fIterator(0)
  {
    fTypeManager = fUISvc.typeManager();
  }
  container* collect(container& aVector
                     ,const std::string& aWhat
		     ,const std::string& aCuts) {
    int number = aVector.size();
    if(number<=0) return 0;
    if(!setup(aWhat,aVector)) return 0;
    std::vector<std::string> args;
    args.push_back(aWhat);
    args.push_back(aCuts);
    fTypeManager->execute("collect",args);
    container* v = new container;
    if(v) {
      Slash::Data::IHandlersIterator* hit = fTypeManager->handlersIterator();
      for(;const Slash::Data::IHandler* h = hit->handler();hit->next()) {
	v->add((Object*)h->object());
      }
      delete hit;
    }
    fTypeManager->eraseHandlers();
    // fIterator deleted by the collect.
    fType->setIterator(0);
    return v;
  }
  void dump(container& aVector,const std::string& aWhat) {
    int number = aVector.size();
    if(number<=0) return;
    if(!setup(aWhat,aVector)) return;
    std::vector<std::string> args;
    args.push_back(aWhat);
    args.push_back("");
    fTypeManager->execute("collect",args);
    args.clear();
    args.push_back("table");
    //args.push_back("raw");
    fTypeManager->execute("dump",args);
    fTypeManager->eraseHandlers();
    // fIterator deleted by the collect.
    fType->setIterator(0);
  }
private:
  bool setup(const std::string& aWhat,container& aVector) {
    fType = 0;
    fIterator = 0;
    if(!fTypeManager) return false;
    Slash::Data::IAccessor* type = fTypeManager->findAccessor(aWhat);
    if(!type) { 
      // Not found in OnX dictionary. Try the Gaudi dico.
      type = fUISvc.metaType();
      if(type && type->setName(aWhat)) { 
	// Found in Gaudi dictionary.
      } else {
	fLog << MSG::INFO << " type \"" << aWhat << "\" not found." << endmsg;
	return false;
      }
    }

    class Iterator : public Slash::Data::IIterator {
    public: //Slash::Data::IIterator
      virtual void* object() {
	if(fIterator==fVector.end()) return 0;
	return *fIterator;
      }
      virtual void next() { ++fIterator;}
      virtual void* tag() {return 0;}
    public:
      Iterator(container& aVector):fIndex(0),fVector(aVector) {
	fIterator = fVector.begin();
      }
    private:
      unsigned int fIndex;
      container& fVector;
      typename container::iterator fIterator;
    };

    fType = type;
    fIterator = new Iterator(aVector);
    fType->setIterator(fIterator);
    return true;
  }
private:
  IUserInterfaceSvc& fUISvc;
  MsgStream& fLog;
  Slash::Data::IProcessor* fTypeManager;
  Slash::Data::IAccessor* fType;
  Slash::Data::IIterator* fIterator;
};

#endif


