// this :
#include "RichRecSegmentType.h"

//////////////////////////////////////////////////////////////////////////////
RichRecSegmentType::RichRecSegmentType( IUserInterfaceSvc* aUISvc,
                                        ISoConversionSvc* aSoCnvSvc,
                                        IDataProviderSvc* aDataProviderSvc,
                                        IToolSvc*         aToolSvc,
                                        IJobOptionsSvc*   aJoSvc,
                                        MsgStream & msgStream )
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
  : RichRecTypeBase<LHCb::RichRecSegment>( LHCb::RichRecSegments::classID(),
                                           "RichRecSegment",
                                           LHCb::RichRecSegmentLocation::Offline,
                                           aUISvc,aSoCnvSvc,aDataProviderSvc,
                                           aToolSvc,aJoSvc,msgStream )
{
  addProperty("Key",Lib::Property::INTEGER);
  addProperty("entryX",Lib::Property::DOUBLE);
  addProperty("entryY",Lib::Property::DOUBLE);
  addProperty("entryZ",Lib::Property::DOUBLE);
}
//////////////////////////////////////////////////////////////////////////////
Lib::Variable RichRecSegmentType::value( Lib::Identifier aIdentifier
                                         ,const std::string& aName
                                         ,void* )
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  LHCb::RichRecSegment* obj = (LHCb::RichRecSegment*)aIdentifier;
  if ( obj )
  {
    const LHCb::RichTrackSegment & seg = obj->trackSegment();
    const Gaudi::XYZPoint & ent = seg.entryPoint();

    if     ( aName == "Key" )
    {
      return Lib::Variable(printer(),obj->key());
    }
    else if(aName=="entryX")
    {
      return Lib::Variable(printer(),ent.x());
    }
    else if(aName=="entryY")
    {
      return Lib::Variable(printer(),ent.y());
    }
    else if(aName=="entryZ")
    {
      return Lib::Variable(printer(),ent.z());
    }
    else
    {
      return Lib::Variable(printer());
    }
  }
  return Lib::Variable(printer());
}

//////////////////////////////////////////////////////////////////////////////
void RichRecSegmentType::visualize( Lib::Identifier aIdentifier
                                    ,void* )
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  LHCb::RichRecSegment* segment = (LHCb::RichRecSegment*)aIdentifier;

  std::string value;
  fUISvc->session()->parameterValue("modeling.what",value);
  info() << "RichRecSegmentType : modeling.what = " << value << endmsg;
  if      ( value=="RichRecoPixels"  )
  {
    drawPixels( segment );
  }
  else if ( value=="RichRecoPhotons" )
  {
    drawPhotons( segment );
  }
  else if ( value=="RichRecoTracks" )
  {
    drawTrack( segment );
  }
  else if ( value=="RichRecoCKRings" )
  {
    drawRings( segment );
  }
  else if ( value=="RichMCSegments"  )
  {
    drawMCSegment ( segment );
  }
  else if ( value=="RichMCPixels"    )
  {
    drawMCPixels ( segment );
  }
  else if ( value=="RichMCPhotons"   )
  {
    drawMCPhotons ( segment );
  }
  else if ( value=="RichMCCKRings"   )
  {
    drawMCRings ( segment );
  }
  else
  {
    drawSegment ( segment );
  }
}
//////////////////////////////////////////////////////////////////////////////
