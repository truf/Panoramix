// ============================================================================
// CVS tag $Name: not supported by cvs2svn $
// ============================================================================
#ifndef     SORICHREC_SOMCRICHSEGMENTCNV_H
#define     SORICHREC_SOMCRICHSEGMENTCNV_H 1

// SoRich
#include "SoRichBaseCnv.h"

// forward decalarations
template <class CNV> class CnvFactory;

/**  @class SoRichRecSegmentCnv  SoRichRecSegmentCnv.h
 *
 *   Converter for visualization of
 *   container of RichRecSegment objects
 *
 *   @author  Chris Jones   Christopher.Rob.Jones@cern.ch
 *   @date    09/05/2004
 */

class SoRichRecSegmentCnv : public SoRichBaseCnv 
{

  friend class CnvFactory<SoRichRecSegmentCnv>;

public:

  /// standard initialization method
  virtual StatusCode initialize ();

  /// standard finalization  method
  virtual StatusCode finalize   ();

  virtual long repSvcType() const;

  /// the only one essential method
  virtual StatusCode createRep( DataObject* /* Object */ ,
                                IOpaqueAddress*&  /* Address */ );

  /// Class ID for created object == class ID for this specific converter
  static const CLID&  classID();

  /// storage Type
  static unsigned char storageType();

protected:

  /// standard constructor
  SoRichRecSegmentCnv( ISvcLocator* svcLoc );

  /// virtual destructor
  virtual ~SoRichRecSegmentCnv();

private:

  /// default constructor is disabled
  SoRichRecSegmentCnv           (                       ) ;

  /// copy constructor is disabled
  SoRichRecSegmentCnv           ( const SoRichRecSegmentCnv& ) ;

  /// assignment is disabled
  SoRichRecSegmentCnv& operator=( const SoRichRecSegmentCnv& ) ;

};


// ============================================================================
#endif  //  SORICHREC_SOMCRICHSEGMENTCNV_H
// ============================================================================













