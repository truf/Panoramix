#ifndef SoRichRec_RichRecPixelType_h
#define SoRichRec_RichRecPixelType_h

// Inheritance :
#include "RichRecTypeBase.h"

class IUserInterfaceSvc;
class ISoConversionSvc;
class IDataProviderSvc;
class IToolSvc;
class IJobOptionsSvc;

class RichRecPixelType : public RichRecTypeBase<LHCb::RichRecPixel> 
{

public: //Lib::IType

  virtual Lib::Variable value(Lib::Identifier,const std::string&,void*);
  virtual void visualize(Lib::Identifier aIdentifier,void*);

public:

  /// Constructor
  RichRecPixelType(IUserInterfaceSvc*,
                   ISoConversionSvc*,
                   IDataProviderSvc*,
                   IToolSvc*,
                   IJobOptionsSvc*,
                   MsgStream&);

};

#endif
