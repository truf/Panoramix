#ifndef SoRichRec_RichRecRingType_h
#define SoRichRec_RichRecRingType_h

// Inheritance :
#include "RichRecTypeBase.h"

class IUserInterfaceSvc;
class ISoConversionSvc;
class IDataProviderSvc;
class IToolSvc;
class IJobOptionsSvc;

class RichRecRingType : public RichRecTypeBase<LHCb::RichRecRing> 
{
public: //Lib::IType
  virtual Lib::Variable value(Lib::Identifier,const std::string&,void*);
  virtual void visualize(Lib::Identifier aIdentifier,void*);
public:
  RichRecRingType(IUserInterfaceSvc*,
                  ISoConversionSvc*,
                  IDataProviderSvc*,
                  IToolSvc*,
                  IJobOptionsSvc*,
                  MsgStream&);
};

#endif
