// ============================================================================
// CVS tag $Name: not supported by cvs2svn $
// ============================================================================

#include  "OnXSvc/Win32.h"

// This :
#include "SoRichRecPixelCnv.h"

// Inventor :
#include <Inventor/nodes/SoSeparator.h>

// GaudiKernel
#include <GaudiKernel/IRegistry.h>
#include <GaudiKernel/CnvFactory.h>
#include <GaudiKernel/IDataProviderSvc.h>
#include <GaudiKernel/SmartDataPtr.h>
#include <GaudiKernel/DataObject.h>
#include <GaudiKernel/IToolSvc.h>

// OnXSvc
#include <OnXSvc/IUserInterfaceSvc.h>
#include <OnXSvc/ClassID.h>
#include <OnXSvc/Helpers.h>

// Lib :
#include <Lib/smanip.h>
#include <Lib/Interfaces/ISession.h>

// Kernel
#include <Kernel/RichRadiatorType.h>

// Local
#include "RichRecPixelRep.h"

// HEPVis :
#include <HEPVis/nodekits/SoRegion.h>

// RichEvent
#include <Event/RichRecPixel.h>

// namespaces
using namespace LHCb;

/** @file SoRichRecPixelCnv.cpp
 *
 *  Implementation of SoRichRecPixelCnv class
 *
 *  @author  Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date    09/05/2004
 */

// ============================================================================
// mandatory factory business
// ============================================================================
DECLARE_CONVERTER_FACTORY(SoRichRecPixelCnv)

// ============================================================================
// standard constructor
// ============================================================================
SoRichRecPixelCnv::SoRichRecPixelCnv( ISvcLocator* svcLoc )
  : SoRichBaseCnv ( classID(), svcLoc )
{
  setName ( "SoRichRecPixelCnv" );
}

// ============================================================================
// standard desctructor
// ============================================================================
SoRichRecPixelCnv::~SoRichRecPixelCnv(){}

// ============================================================================
// initialize
// ============================================================================
StatusCode SoRichRecPixelCnv::initialize()
{
  StatusCode sc = SoRichBaseCnv::initialize();
  if ( sc.isFailure() )
    return Error("initialize: Could not initialize base class!");

  return StatusCode::SUCCESS;
}

// ============================================================================
// finalize
// ============================================================================
StatusCode SoRichRecPixelCnv::finalize()
{
  return SoRichBaseCnv::finalize();
}

long SoRichRecPixelCnv::repSvcType() const
{
  return i_repSvcType();
}

// ============================================================================
// Class ID for created object == class ID for this specific converter
// ============================================================================
const CLID& SoRichRecPixelCnv::classID()
{
  return RichRecPixels::classID();
}

// ============================================================================
// storage Type
// ============================================================================
unsigned char SoRichRecPixelCnv::storageType()
{
  return So_TechnologyType;
}

// ============================================================================
// the only one essential method
// ============================================================================
StatusCode SoRichRecPixelCnv::createRep( DataObject*         object     ,
                                         IOpaqueAddress*& /* Address */ )
{

  // Preliminary checks
  if ( 0 == object  ) return Error("createRep: DataObject* points to NULL");
  if ( 0 == uiSvc() ) return Error("createRep: IUserInterfaceSvc* points to NULL" );

  SoRegion* soRegion = uiSvc()->currentSoRegion();
  if(soRegion==0) 
    { return Error("createRep: SoRegion* points to NULL" );}

  ISession* session = uiSvc()->session();
  if(session==0) 
    { return Error("createRep: ISession* points to NULL" );}

  const RichRecPixels* pixels = dynamic_cast<const RichRecPixels*>( object );
  if ( 0 == pixels  )
    { return Error( "createRep: Wrong input data type '" +
                    System::typeinfoName(typeid(object)) + "'" ); }
  if ( pixels->empty() )
  { return Debug( "Container " + object->registry()->identifier() +
                  " is empty" ); }

  // Find first non-null digit
  typedef RichRecPixels::const_iterator RiMCSegIt;
  RiMCSegIt iseg;
  for ( iseg = pixels->begin(); iseg != pixels->end(); ++iseg ) { if(*iseg) break; }
  if ( pixels->end() == iseg )
    { return Warning("Container " + object->registry()->identifier() +
                     " contains NULL pointers !" , StatusCode::SUCCESS ) ;}

  // Colours
  double r, g, b, hr, hg, hb;
  std::string color1, color2;
  if ( !session->parameterValue( "modeling.color", color1 ) ) color1 = "red";
  Lib::smanip::torgb(color1,r,g,b);
  SbColor color(r,g,b);
  if ( !session->parameterValue( "modeling.highlightColor", color2 ) ) color2 = "red";
  Lib::smanip::torgb(color2,hr,hg,hb);
  SbColor hcolor(hr,hg,hb);

  // marker
  std::string shape;
  if ( !session->parameterValue("modeling.RichRecPixelMode",shape) ) shape = "marker";

  // create visualisation object
  Debug( "Visualizer for RichRecPixels : Style= " +shape +
         " : colors="+color1+" "+color2 );
  RichRecPixelRep vis( shape,
                       RichRecPixelRep::Qualities
		        ( color, hcolor, SoMarkerSet::CIRCLE_FILLED_5_5 ),
                       soRegion->styleCache());

  SoSeparator* separator = vis.begin();

  bool empty = true;
  for ( iseg = pixels->begin(); iseg != pixels->end(); ++iseg )
  { 
    if(!(*iseg)) continue; 
    vis.represent(*iseg,separator);
    empty = false;
  }

  if (empty)
  {
    separator->unref();
  } 
  else 
  {
    soRegion->resetUndo();
    region_addToDynamicScene(*soRegion,separator);
  }

  return StatusCode::SUCCESS;
}
