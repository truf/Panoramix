// ============================================================================
// CVS tag $Name: not supported by cvs2svn $
// ============================================================================
#ifndef     SORICHREC_SOMCRICHPHOTONCNV_H
#define     SORICHREC_SOMCRICHPHOTONCNV_H 1

// SoRich
#include "SoRichBaseCnv.h"

// forward decalarations
template <class CNV> class CnvFactory;

/**  @class SoRichRecPhotonCnv  SoRichRecPhotonCnv.h
 *
 *   Converter for visualization of
 *   container of RichRecPhoton objects
 *
 *   @author  Chris Jones   Christopher.Rob.Jones@cern.ch
 *   @date    09/05/2004
 */

class SoRichRecPhotonCnv : public SoRichBaseCnv {

  friend class CnvFactory<SoRichRecPhotonCnv>;

public:

  /// standard initialization method
  virtual StatusCode initialize ();

  /// standard finalization  method
  virtual StatusCode finalize   ();

  virtual long repSvcType() const;

  /// the only one essential method
  virtual StatusCode createRep( DataObject* /* Object */ ,
                                IOpaqueAddress*&  /* Address */ );

  /// Class ID for created object == class ID for this specific converter
  static const CLID&  classID();

  /// storage Type
  static unsigned char storageType () ;

protected:

  /// standard constructor
  SoRichRecPhotonCnv( ISvcLocator* svcLoc );

  /// virtual destructor
  virtual ~SoRichRecPhotonCnv();

private:

  /// default constructor is disabled
  SoRichRecPhotonCnv           (                       ) ;

  /// copy constructor is disabled
  SoRichRecPhotonCnv           ( const SoRichRecPhotonCnv& ) ;

  /// assignment is disabled
  SoRichRecPhotonCnv& operator=( const SoRichRecPhotonCnv& ) ;

};


// ============================================================================
#endif  //  SORICHREC_SOMCRICHPHOTONCNV_H
// ============================================================================













