// this :
#include "SoRichRecSvc.h"

// Gaudi :
#include <GaudiKernel/SvcFactory.h>
#include <GaudiKernel/IToolSvc.h>
#include <GaudiKernel/IJobOptionsSvc.h>
#include <GaudiKernel/IDataProviderSvc.h>

// OnXSvc :
#include <OnXSvc/IUserInterfaceSvc.h>
#include <OnXSvc/ISoConversionSvc.h>

// local
#include "RichRecPhotonType.h"
#include "RichRecRingType.h"
#include "RichRecSegmentType.h"
#include "RichRecPixelType.h"


DECLARE_SERVICE_FACTORY(SoRichRecSvc)

SoRichRecSvc::SoRichRecSvc( const std::string& name,
                            ISvcLocator* pSvcLocator )
  : Service(name,pSvcLocator)
  , m_uiSvc(0)
  , m_soConSvc(0)
  , m_evtDataSvc(0)
  , m_toolSvc(0)
  , m_joSvc(0)
  , m_msgStream(0) { }

SoRichRecSvc::~SoRichRecSvc() { delete m_msgStream; }

StatusCode SoRichRecSvc::initialize()
{
  StatusCode sc = Service::initialize();
  if ( sc.isFailure() ) return sc ;

  debug() << "Initialize " << endmsg;

  setProperties();

  sc = ( getService( "OnXSvc",          m_uiSvc      ) &&
         getService( "SoConversionSvc", m_soConSvc   ) &&
         getService( "EventDataSvc",    m_evtDataSvc ) &&
         getService( "ToolSvc",         m_toolSvc    ) &&
         getService( "JobOptionsSvc",   m_joSvc      ) );
  if ( sc.isFailure() ) return sc ;

  m_uiSvc->addType(new RichRecPhotonType (m_uiSvc,m_soConSvc,m_evtDataSvc,m_toolSvc,m_joSvc,msgStream()));
  m_uiSvc->addType(new RichRecRingType   (m_uiSvc,m_soConSvc,m_evtDataSvc,m_toolSvc,m_joSvc,msgStream()));
  m_uiSvc->addType(new RichRecSegmentType(m_uiSvc,m_soConSvc,m_evtDataSvc,m_toolSvc,m_joSvc,msgStream()));
  m_uiSvc->addType(new RichRecPixelType  (m_uiSvc,m_soConSvc,m_evtDataSvc,m_toolSvc,m_joSvc,msgStream()));

  return sc;
}

StatusCode SoRichRecSvc::finalize()
{
  releaseSvc ( m_uiSvc      );
  releaseSvc ( m_soConSvc   );
  releaseSvc ( m_evtDataSvc );
  releaseSvc ( m_toolSvc    );
  releaseSvc ( m_joSvc      );
  debug() << "Finalized successfully" << endmsg;
  return StatusCode::SUCCESS;
}
