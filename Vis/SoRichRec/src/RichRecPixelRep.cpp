// $Id: RichRecPixelRep.cpp,v 1.10 2009-05-26 07:46:13 gybarran Exp $ 
// ============================================================================
// CVS tag $Name: not supported by cvs2svn $
// ============================================================================

#include <OnXSvc/Win32.h>

// This :
#include "RichRecPixelRep.h"

#include <cmath>

// CLHEP
#include <GaudiKernel/Point3DTypes.h>

// Inventor
#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoLightModel.h>
#include <Inventor/nodes/SoDrawStyle.h>
#include <Inventor/nodes/SoCoordinate3.h>
#include <Inventor/nodes/SoLineSet.h>

#include <HEPVis/misc/SoStyleCache.h>
#include <HEPVis/nodes/SoSceneGraph.h>
#include <HEPVis/nodes/SoHighlightMaterial.h>

// Kernel
#include <Kernel/RichRadiatorType.h>

// RichEvent
#include <Event/RichRecPixel.h>

// namespaces
using namespace LHCb;

// ============================================================================
/** @file RichRecPixelRep.cpp
 *  Implementation of class RichRecPixelRep
 *
 *  @author  Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date    09/05/2004
 */
// ============================================================================

// ============================================================================
/** standard constructor
 */
// ============================================================================
RichRecPixelRep::RichRecPixelRep( const std::string& Type,
                                  const Qualities & qualities,
                                  SoStyleCache* styleCache)
  : m_type        ( Type      )
  , m_qualities   ( qualities )
  , m_styleCache  ( styleCache)
  , m_coordinate  ( 0 )
  , m_icoord      ( 0 )
{ }

// ============================================================================
/** destructor
 */
// ============================================================================
RichRecPixelRep::~RichRecPixelRep(){}

// ============================================================================
// ============================================================================
SoSeparator* RichRecPixelRep::begin()
{
  SoSeparator* separator = new SoSeparator();
  separator->addChild(m_styleCache->getLightModelBaseColor());
  separator->addChild(m_styleCache->getLineStyle());

  m_coordinate = new SoCoordinate3;
  separator->addChild(m_coordinate);
  m_icoord = 0;

  return separator;
}

void RichRecPixelRep::represent( const RichRecPixel * pixel
                                ,SoSeparator* aSeparator
                                )
{
  // Build picking string id :
  char sid[64];
  ::sprintf(sid,"RichRecPixel/0x%lx",(unsigned long)pixel);

  SoSceneGraph * separator = new SoSceneGraph();
  separator->setString(sid);

  aSeparator->addChild(separator);

  separator->addChild( m_styleCache->getHighlightMaterial(qualities().color(),
                                                          qualities().hcolor()));

  if ( markerStyle() == "marker" )
  {

    // Hit position
    const Gaudi::XYZPoint & gPos = pixel->globalPosition();

    SbVec3f points[1];
    points[0].setValue( gPos.x(), gPos.y(), gPos.z() );

    // Add one So per particle
    SoCoordinate3* coordinate3 = new SoCoordinate3;
    coordinate3->point.setValues(0,1,points);
    separator->addChild( coordinate3 );

    // So marker type
    SoMarkerSet* markerSet = new SoMarkerSet;
    markerSet->numPoints = 1;
    markerSet->markerIndex = qualities().markerType();
    separator->addChild( markerSet );

  }
}

// ============================================================================
