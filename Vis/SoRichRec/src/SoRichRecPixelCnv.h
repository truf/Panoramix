// ============================================================================
// CVS tag $Name: not supported by cvs2svn $
// ============================================================================
#ifndef     SORICHREC_SOMCRICHPIXELCNV_H
#define     SORICHREC_SOMCRICHPIXELCNV_H 1

// SoRich
#include "SoRichBaseCnv.h"

// forward decalarations
template <class CNV> class CnvFactory;

/**  @class SoRichRecPixelCnv  SoRichRecPixelCnv.h
 *
 *   Converter for visualization of
 *   container of RichRecPixel objects
 *
 *   @author  Chris Jones   Christopher.Rob.Jones@cern.ch
 *   @date    09/05/2004
 */

class SoRichRecPixelCnv : public SoRichBaseCnv 
{

  friend class CnvFactory<SoRichRecPixelCnv>;

public:

  /// standard initialization method
  virtual StatusCode initialize ();

  /// standard finalization  method
  virtual StatusCode finalize   ();

  virtual long repSvcType() const;

  /// the only one essential method
  virtual StatusCode createRep( DataObject* /* Object */ ,
                                IOpaqueAddress*&  /* Address */ );

  /// Class ID for created object == class ID for this specific converter
  static const CLID&  classID();

  /// storage Type
  static unsigned char storageType () ;

protected:

  /// standard constructor
  SoRichRecPixelCnv( ISvcLocator* svcLoc );

  /// virtual destructor
  virtual ~SoRichRecPixelCnv();

private:

  /// default constructor is disabled
  SoRichRecPixelCnv           (                       ) ;

  /// copy constructor is disabled
  SoRichRecPixelCnv           ( const SoRichRecPixelCnv& ) ;

  /// assignment is disabled
  SoRichRecPixelCnv& operator=( const SoRichRecPixelCnv& ) ;

};


// ============================================================================
#endif  //  SORICHREC_SOMCRICHPIXELCNV_H
// ============================================================================













