
// it read an LCHb .hiv file and produce a big .jpeg file.

//the below before Inventor to avoid : warning: "HAVE_STDDEF_H" redefined
#include <exlib/jpeg>

#ifdef HAVE_STDDEF_H
#undef HAVE_STDDEF_H
#endif

#include <Inventor/SoDB.h>
#include <Inventor/SoInteraction.h>
#include <Inventor/nodekits/SoNodeKit.h>
#include <exlib/iv/hepvis> //to read LHCb .hiv files.

#include <inlib/sg/render_action>
#include <inlib/sg/group>
#include <inlib/img>

//#include <exlib/sg/ortho>
#include <exlib/sg/torche>
#include <exlib/sg/lrbt>

#include <exlib/iv/file>

class iv {
  inlib::sg::sf<unsigned int> ww;
  inlib::sg::sf<unsigned int> wh;
public:
  inlib::sg::sf<unsigned int> col;
  inlib::sg::sf<unsigned int> row;
public:
  iv(std::ostream& a_out,
     const std::string& a_file,
     unsigned int a_cols,unsigned int a_rows)
  :ww(0),wh(0)
  ,col(0),row(0)
  ,m_out(a_out)
  ,m_background(0,0,0)
  ,m_cols(a_cols)
  ,m_rows(a_rows)
  ,m_iv_node(0)
  ,m_produce_imgs(false)
  {
    m_iv_node = new exlib::iv::file();
    m_iv_node->name = a_file;
  }
  virtual ~iv() {
    m_sg.remove(m_iv_node);
    m_sg.clear();
    delete m_iv_node;
    m_imgs.clear();
  }
private:
  iv(const iv& a_from)
  :ww(a_from.ww)
  ,wh(a_from.wh)
  ,col(a_from.col),row(a_from.row)
  ,m_out(a_from.m_out)
  {}
  iv& operator=(const iv&){return *this;}
public:
  inline void resize_window(unsigned int a_w,unsigned int a_h) {
    //m_out << "debug : resize_window :"
    //      << " ww " << a_w << " wh " << a_h
    //      << std::endl;
    ww = a_w;
    wh = a_h;    
  }

  inline void render() {
    //m_out << "debug : render :" << std::endl;

    if(!ww.value()) return;
    if(!wh.value()) return;

    ::glEnable(GL_DEPTH_TEST);
    ::glFrontFace(GL_CCW);
    ::glEnable(GL_CULL_FACE);
    //  So that lightning is correctly computed 
    // when some scaling is applied.
    ::glEnable(GL_NORMALIZE);
    ::glShadeModel(GL_FLAT);

    ::glDisable(GL_LIGHTING);

    //printf("debug : render : %d %d\n",ww.value(),wh.value());

    //::glEnable(GL_SCISSOR_IV);

    ::glViewport(0,0,ww.value(),wh.value());
    //::glScissor(0,0,ww.value(),wh.value());

    // Better to clear after glViewport and glScissor 
    // (else problems with Mesa).
    ::glClearColor(m_background.r(),
                   m_background.g(),
                   m_background.b(),0);
    ::glClear(GL_COLOR_BUFFER_BIT);
    ::glClear(GL_DEPTH_BUFFER_BIT);

    ::glMatrixMode(GL_PROJECTION);
    ::glLoadIdentity();

    ::glMatrixMode(GL_MODELVIEW);
    ::glLoadIdentity();

    if( ww.touched() || wh.touched()   ||
        col.touched() || row.touched() ){
      update_sg(); //create scene graph.
      ww.reset_touched();
      wh.reset_touched();
      col.reset_touched();
      row.reset_touched();
    }

    inlib::sg::render_action action(m_out,ww.value(),wh.value());
    m_sg.render(action);

    //after_render();
    if(m_produce_imgs) {
      unsigned int w,h,bpp;
      unsigned char* buffer = get_image(w,h,bpp);
      if(!buffer) {
        m_out << " can't get image." << std::endl;
        m_produce_imgs = false;
        m_imgs.clear();
      } else {
        m_imgs.push_back(inlib::img_byte(w,h,bpp,buffer,true));
      }      
    }

    ::glFinish();
  }

private:
  inline void update_sg() {
    m_sg.remove(m_iv_node);
    m_sg.clear();

   {exlib::sg::torche* light = new exlib::sg::torche;
    light->direction = inlib::vec3f(1,-1,-10);
    m_sg.add(light);}

    //float hcam = 30000;
    float hcam = 12000;

    exlib::sg::lrbt* camera = create_camera_lrbt(m_cols,m_rows,
                                                 col.value(),row.value(),
                                                 0,0,
						 ww.value(),wh.value(),hcam);
    //exlib::sg::ortho* camera = new exlib::sg::ortho;
    //camera->height.value(hcam);    
    camera->znear.value(0.1*hcam);
    camera->zfar.value(100*hcam);
// guy's values       camera->position.value(inlib::vec3f(-10000,0,9000));    
    camera->position.value(inlib::vec3f(0., 30000., 10000.));    
    camera->orientation.value
// guy's values      (inlib::rotf(inlib::vec3f(0,1,0),-inlib::fhalf_pi()));
      (inlib::rotf(inlib::vec3f(-0.57735, -0.57735, -0.57735),4./3.*inlib::fhalf_pi()));

    m_sg.add(camera);

    m_sg.add(m_iv_node);
  }
private:
  inline exlib::sg::lrbt* create_camera_lrbt(
                         unsigned int a_cols,unsigned int a_rows,
    		         unsigned int a_col,unsigned int a_row,
                         unsigned int a_bw,unsigned int a_bh,
                         unsigned int a_ww,unsigned int a_wh,
                         float a_near_height){

    exlib::sg::lrbt* camera = new exlib::sg::lrbt;

    float height = a_near_height;

    //NOTE : the camera height maps the whole MUSC screen height.
    height /= a_rows; //height of the world seen by one screen as if borders
                      //had been visible pixels.

    // aspect of one screen as if borders had been visible pixels.
    float aspect = ((float)(a_ww+2*a_bw))/((float)(a_wh+2*a_bh));
    float width = aspect * height;

    // left,bottom,right,top as if borders had been visibled pixels.
    float left = -((float)a_cols)*width*0.5F + ((float)a_col) * width;
    float bottom = -((float)a_rows)*height*0.5F + ((float)a_row) * height;
    float right = left+width;
    float top = bottom+height;
  
    // we evaluate the border in WC.
    float bh = float(a_bh) * height/float(a_wh+2*a_bh);
    float bw = float(a_bw) * width/float(a_ww+2*a_bw); 
  
    // we remove the border from the world seen by one screen.
    left += bw;
    right -= bw;
    bottom += bh;
    top -= bh;
  
    camera->left.value(left);
    camera->right.value(right);
    camera->bottom.value(bottom);
    camera->top.value(top);
  
    return camera;
  }
  inline unsigned char* get_image(unsigned int& a_w,unsigned int& a_h,unsigned int& a_bpp) {
    if(!ww.value()) {a_w=0;a_h=0;a_bpp=0;return 0;}
    if(!wh.value()) {a_w=0;a_h=0;a_bpp=0;return 0;}
    unsigned char* buffer = new unsigned char[3 * ww.value() * wh.value()];
    if(!buffer) {a_w=0;a_h=0;a_bpp=0;return 0;}
    ::glPixelStorei(GL_PACK_ALIGNMENT,1); //needed with Cocoa.
    ::glReadPixels(0,0,ww.value(),wh.value(),GL_RGB,GL_UNSIGNED_BYTE,buffer);
    a_w = ww.value();
    a_h = wh.value();
    a_bpp = 3;
    return buffer;
  }
    /*
  inline void after_render() {
    //WARNING : it does OpenGL. Under Android it should be executed
    //          in the OpenGL thread.
    if(!m_produce_out_jpg) return;
    m_produce_out_jpg = false;

    unsigned int w,h,bpp;
    unsigned char* buffer = get_image(w,h,bpp);
    if(!buffer) {
      m_out << " can't get image." << std::endl;
      return;
    }
    std::string file;
    file += "out.jpg";
    //m_out << "debug : gui_viewer::out_jpg : file " << inlib::sout(file)
    //           << std::endl;
    if(!exlib::jpeg::write(m_out,file,buffer,w,h,bpp,100)) {
    }
    delete [] buffer;      
  }
    */

private: //viewer
  std::ostream& m_out;
  inlib::colorf m_background;
  inlib::sg::group m_sg;
  unsigned int m_cols;
  unsigned int m_rows;
  exlib::iv::file* m_iv_node;
public:
  bool m_produce_imgs;
  std::vector<inlib::img_byte> m_imgs;
};

#include <exlib/X11/session>

inline void out_jpeg(std::ostream& a_out,iv& a_iv,
                     unsigned int a_cols,unsigned int a_rows,
                     Display* a_display,Window a_win,
                     const std::string& a_out_file,
                     bool a_verbose){
  a_iv.m_produce_imgs = true;
  a_iv.m_imgs.clear();
  for(unsigned int row=0;row<a_rows;row++) {
    for(unsigned int col=0;col<a_cols;col++) {
      if(a_verbose) a_out << "row " << row << " col " << col << std::endl;
      a_iv.col = col;
      a_iv.row = row;
      a_iv.render();
      ::glXSwapBuffers(a_display,a_win);
    }
  }
  if(a_verbose) a_out << "imgs " << a_iv.m_imgs.size() << std::endl;

  if(!a_iv.m_imgs.size()) {
    a_out << "no image." << std::endl;
    return;
  }

  //produce (bug) jpeg :
  inlib::img_byte img(3);
  inlib::img_byte::concatenate(a_iv.m_imgs,a_cols,a_rows,0,0,0,img);

  if(a_verbose) a_out << "produce " << a_out_file << " ..." << std::endl;
  if(!exlib::jpeg::write(a_out,a_out_file,
        img.buffer(),img.width(),img.height(),img.bpp(),100)) {
    a_out << "failed." << std::endl;
  } else {
    if(a_verbose) a_out << "done." << std::endl;
  }
  a_iv.m_produce_imgs = false;
  a_iv.m_imgs.clear();
}

#include <inlib/args>

#include <iostream>
#include <cstdlib>

int main(int argc,char** argv) {

 {exlib::X11::session x11(std::cout);
  if(!x11.display()) return EXIT_FAILURE;

  inlib::args args(argc,argv);

  if(args.is_arg("-h")) {
    std::cout << "<program> [options] <inventor file>." << std::endl;
    std::cout << "options :" << std::endl;
    std::cout << " -verbose : to dump the (row,col) of tiles." << std::endl;
    std::cout << " -cols=<unsigned int> : number of x tiles." << std::endl;
    std::cout << " -rows=<uint> : number of y tiles." << std::endl;
    std::cout << " -ww=<uint> : window width for one tile." << std::endl;
    std::cout << " -wh=<uint> : window height for one tile." << std::endl;
    std::cout << " -out=<string> : output file name." << std::endl;
    return EXIT_SUCCESS;
  }

  bool verbose = args.is_arg("-verbose");

  std::string iv_file;
  if(!args.file(iv_file)) {
    std::cout << " give an inventor file." << std::endl;
    return EXIT_FAILURE;
  }

  std::string out_file;
  if(!args.find("-out",out_file)) out_file = "out.jpg";

  // big picture is around : 23000 x 7000
  unsigned int ww = 720;
  if(args.is_arg("-ww")) {
    if(!args.find<unsigned int>("-ww",ww)) {
      std::cout << " problem with -ww." << std::endl;
      return EXIT_FAILURE;
    }
  }

  unsigned int wh = 875;
  if(args.is_arg("-wh")) {
    if(!args.find<unsigned int>("-wh",wh)) {
      std::cout << " problem with -wh." << std::endl;
      return EXIT_FAILURE;
    }
  }

  unsigned int cols = 32; //iww = 32*720 = 23040
  if(args.is_arg("-cols")) {
    if(!args.find<unsigned int>("-cols",cols)) {
      std::cout << " problem with -cols." << std::endl;
      return EXIT_FAILURE;
    }
  }
  unsigned int rows = 8; //wh = 8*875 = 7000;
  if(args.is_arg("-rows")) {
    if(!args.find<unsigned int>("-rows",rows)) {
      std::cout << " problem with -rows." << std::endl;
      return EXIT_FAILURE;
    }
  }

  //bool one_tile = true;
  if(args.is_arg("-one_tile")) {
    unsigned int iww = ww*cols;
    unsigned int iwh = wh*rows;
    wh = (unsigned int)(ww*float(iwh)/float(iww));
    cols = 1;rows = 1;
  }

  Window win = x11.create_window("LHCb_artist",0,0,ww,wh);
  if(win==0L) return EXIT_FAILURE;
  x11.show_window(win);

  if(::glXMakeCurrent(x11.display(),win,x11.context())==False){
    std::cout << "glXMakeCurrent failed." << std::endl;
    x11.delete_window(win);
    return EXIT_FAILURE;
  }

 {if(!SoDB::isInitialized()) SoDB::init();
  SoNodeKit::init();
  SoInteraction::init();
  exlib::iv::hepvis::initialize();
  iv exa(std::cout,iv_file,cols,rows);

  //after x11.show_window, we should have a valid window size :
 {int width,height;
  x11.window_size(win,width,height);
  if(width!=int(ww)) {
    std::cout << "WARNING : problem with window width." << std::endl;
    std::cout << "  asked ww " << ww << ", got from X11 " << width
              << std::endl;
  }
  if(height!=int(wh)) {
    std::cout << "WARNING : problem with window height." << std::endl;
    std::cout << "  asked wh " << wh << ", got from X11 " << height
              << std::endl;
  }
  exa.resize_window(width,height);
  out_jpeg(std::cout,exa,cols,rows,x11.display(),win,out_file,verbose);}

 /*
  Atom atom = ::XInternAtom(x11.display(),"WM_DELETE_WINDOW",False);
  while(true) { 
    XEvent xevent;
    if(::XPending(x11.display())) {
      ::XNextEvent(x11.display(),&xevent);

      if(xevent.xclient.data.l[0]==(long)atom) { 
        break;        
      } else if(xevent.type==Expose) {
        //std::cout << "main : xevent Expose." << std::endl;

        exa.render();
        ::glXSwapBuffers(x11.display(),win);

      } else if(xevent.type==ConfigureNotify) {
        int width,height;
        x11.window_size(win,width,height);
        exa.resize_window(width,height);
        //std::cout << "main : xevent ConfigureNotify." << std::endl;

      } else if(xevent.type==ButtonPress && xevent.xbutton.button==1) {
        //out_jpeg(std::cout,exa,cols,rows,x11.display(),win,out_file);
      }
    }
  }
  */
  }

  if(::glXMakeCurrent(x11.display(),None,0)==False){}
  x11.hide_window(win);
  x11.delete_window(win);}

  //std::cout << "main : exit..." << std::endl;

  return EXIT_SUCCESS;
}
