//
//  All functions here should be OnX callbacks, that is to say
// functions with signature :
//   extern "C" {
//     void callback_without_arguments(IUI&);
//     void callback_with_arguments(IUI&,const std::vector<std::string>&);
//   }
//


// Panoramix :
#include "Helpers.h"

// Lib :
#include <Lib/smanip.h>
#include <Lib/System.h>
#include <Lib/Out.h>
#include <Lib/Randoms.h>

// OnX :
#include <OnX/Interfaces/IUI.h>
#include <OnX/Interfaces/ICyclic.h>
#include <OnX/Helpers/Inventor.h>

// Gaudi :
#include <GaudiKernel/IAlgorithm.h>
#include <GaudiKernel/IProperty.h>
#include <GaudiKernel/Property.h>
#include <GaudiKernel/SmartIF.h>
#include <iostream> //For IHistogramSvc.h

// AIDA :
#include <AIDA/IHistogram1D.h>

// SoUtils :
extern "C" {
  void SoUtils_initClasses();
}

// Inventor :
#include <Inventor/nodes/SoOrthographicCamera.h>
#include <Inventor/nodes/SoSeparator.h>

// HEPVis :
//#include <HEPVis/misc/SoStyleCache.h>
#include <HEPVis/nodekits/SoPage.h>
#include <HEPVis/nodekits/SoRegion.h>
#include <HEPVis/nodekits/SoDisplayRegion.h>

// Panoramix :
#include "GaudiManager.h"
#include <typeinfo>
static bool propertyToString(const Property&,std::string&);

#include <Slash/Version.h>
#if (SLASH_MAJOR_VERSION <= 1) && (SLASH_MINOR_VERSION <= 4 )
#else
inline void data_visualize(Slash::Core::ISession& aSession){
  data_visualize(aSession,"@current@");
}
#endif

#define s_SceneGraph  "SceneGraph(@current@)"


// OnX Callbacks :

extern "C" {

  void erase_event(IUI&);

  //////////////////////////////////////////////////////////////////////////////
  void Panoramix_Initialize(
                            IUI& aUI
                            )
    //////////////////////////////////////////////////////////////////////////////
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
  {
    ISession& session = aUI.session();

    if(!session.findManager("SvcLocatorManager")) {
      // Panoramix started with the OnX_onx.exe program :
      aUI.echo("Panoramix_Initialize : OnX startup.");
      //FIXME : pass job opt : OnXSvc.Toolkit = "NONE";
      //  directly to Gaudi without passing by a file.
      //  (Can we do that ? How ?)
      std::string joboptions =
        Lib::System::getenv("PANORAMIXROOT") + "/options/OnX.opts";
      if(!session.findManager("GaudiManager")) {
        session.addManager(new GaudiManager(session,joboptions));
      }
    }

    // Initialize Panoramix Inventor classes :
    SoUtils_initClasses();

    /////////////////////////////////////////////////////////
    // Rich1 :
    /////////////////////////////////////////////////////////
    // SbPolyhedron rotation steps modeling (for sphere).
    // Have 12 instead of 24 by default.
    session.setParameter("modeling.rotationSteps","12");

    /////////////////////////////////////////////////////////
    // SoCalo initialization :
    /////////////////////////////////////////////////////////

    // Calo clusters addresses :
    session.setParameter("EcalClusters","/Event/Rec/Calo/EcalClusters");

    // Calo digits addresses :
    session.setParameter("EcalDigits","/Event/Raw/Ecal/Digits");
    session.setParameter("HcalDigits","/Event/Raw/Hcal/Digits");
    session.setParameter("PrsDigits", "/Event/Raw/Prs/Digits");
    session.setParameter("SpdDigits", "/Event/Raw/Spd/Digits");

    // Cluster parameters :
    session.setParameter("CaloEtVis","FALSE");
    session.setParameter("CaloLogVis","FALSE");
    session.setParameter("CaloScale","30.0");

    // Warning :
    //  Some cluster may not have the associate digits
    //  Then requesting a cluster visualition with the
    //  CaloVisDigits ONLY will produce nothing on the screen !
    session.setParameter("CaloVisDigits","TRUE");
    session.setParameter("CaloVisShared","FALSE");
    session.setParameter("CaloVisStructure","FALSE");
    session.setParameter("CaloVisCovariance","FALSE");
    session.setParameter("CaloVisSpread","TRUE");
    session.setParameter("CaloVisPrism","FALSE");

    // Just in case OnXLab_Initialize had been executed.
    Slash::UI::ISoViewer* soViewer = ui_SoViewer(aUI);
    if(soViewer) {
      soViewer->setDecoration(true);
      soViewer->setViewing(true);
    }
  }
  //////////////////////////////////////////////////////////////////////////////
  void Panoramix_Finalize(
                          IUI&
                          )
    //////////////////////////////////////////////////////////////////////////////
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
  {
    // Nothing for the moment.
  }
  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
  void Panoramix_File_open(
                           IUI& aUI
                           )
    //////////////////////////////////////////////////////////////////////////////
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
  {
    {Slash::UI::IWidget* w = aUI.findWidget("fileChooser");
    if(w) w->hide();}


    std::string file = aUI.callbackValue();
    if(file=="") return;

    ISession& session = aUI.session();

    IEvtSelector* eventSelector = find_eventSelector(session);
    if(!eventSelector) {
      aUI.echo("Panoramix_File_open : unable to find EventSelector.");
      return;
    }

    erase_event(aUI);

    SmartIF<IProperty> prp(eventSelector);
    // The [" and "] seems needed. Don't ask why...
    std::string prop = std::string("[\"DATAFILE='") + file + "' TYP='ROOT' OPT='READ'\"]";
    prop += file + "' TYP='ROOT' OPT='READ'\"]";
    prp->setProperty("Input",prop);

    SmartIF<IService> prp2(eventSelector);
    prp2->reinitialize();

    IService* eventLoopMgr = find_service(session,"EventLoopMgr");
    if(!eventLoopMgr) {
      aUI.echo(" EventLoopMgr not found.");
      return;
    }
    eventLoopMgr->reinitialize();
  }
  //////////////////////////////////////////////////////////////////////////////
  void next_event(
                  IUI& aUI
                  )
    //////////////////////////////////////////////////////////////////////////////
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
  {
    ISession& session = aUI.session();

    erase_event(aUI);

    IAppMgrUI* appMgrUI = find_appMgrUI(session);
    if(!appMgrUI) {
      aUI.echo("next_event : unable to find ApplicationMgr.");
      return;
    }
    appMgrUI->nextEvent(1);

    //ui.setParameter('mainTree.items',uiSvc.writeToString('/Event').c_str())

    //data_collect(session,"Event");
    //data_dump(session);
    //aUI.executeScript("DLD","SoEvent_DLD SoEvent_dump_header");
    aUI.executeScript("Python","import pmx;pmx.x('print_header')");

    // The next event script could be given interactively
    // with the "Event/Set a next event script" input dialog.
    std::string eventScript;
    if(!session.parameterValue("panoramix.eventScript",eventScript)) return;
    if( (eventScript!="") && (eventScript!="none"))
      aUI.executeScript("DLD",eventScript);

  }
  //////////////////////////////////////////////////////////////////////////////
  void execute_eventScript(
                           IUI& aUI
                           )
    //////////////////////////////////////////////////////////////////////////////
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
  {
    std::string eventScript;
    if(!aUI.session().parameterValue("panoramix.eventScript",eventScript))
      return;
    if( (eventScript!="") && (eventScript!="none"))
      aUI.executeScript("DLD",eventScript);
  }
  //////////////////////////////////////////////////////////////////////////////
  void event_infos(
                   IUI& aUI
                   )
    //////////////////////////////////////////////////////////////////////////////
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
  {
    aUI.executeScript("Python","x(\'Event_info\')");
  }
  //////////////////////////////////////////////////////////////////////////////
  void visualize(
                 IUI& aUI
                 ,const std::vector<std::string>& aArgs
                 )
    //////////////////////////////////////////////////////////////////////////////
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
  {
    std::string line;
    for(unsigned int index=0;index<aArgs.size();index++) {
      if(index) line += " ";
      line += aArgs[index];
    }
    IUserInterfaceSvc* uiSvc = find_uiSvc(aUI.session());
    if(!uiSvc) return;
    uiSvc->visualize(line);
  }
  //////////////////////////////////////////////////////////////////////////////
  void ls(
          IUI& aUI
          ,const std::vector<std::string>& aArgs
          )
    //////////////////////////////////////////////////////////////////////////////
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
  {
    IUserInterfaceSvc* uiSvc = find_uiSvc(aUI.session());
    if(!uiSvc) return;
    if(aArgs.size()==1) {
      uiSvc->ls(aArgs[0]);
    } else if(aArgs.size()==2) {
      int depth;
      if(!Lib::smanip::toint(aArgs[0],depth)) return;
      uiSvc->ls(aArgs[1],depth);
    }
  }
  //////////////////////////////////////////////////////////////////////////////
  void erase_event(
                   IUI& aUI
                   )
    //////////////////////////////////////////////////////////////////////////////
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
  {
    SoPage* soPage = ui_SoPage(aUI);
    if(!soPage) return;
    SoRegion* soRegion = ui_SoRegion(aUI);
    if(!soRegion) return;

    if(!soRegion->isOfType(SoDisplayRegion::getClassTypeId())) return;
    SoDisplayRegion* displayRegion = (SoDisplayRegion*)soRegion;
    displayRegion->getDynamicScene()->removeAllChildren();

    //SoStyleCache* soStyleCache = soRegion->styleCache(); 
    //soStyleCache->clearCache();
  }
  //////////////////////////////////////////////////////////////////////////////
  void erase_detector(
                      IUI& aUI
                      )
    //////////////////////////////////////////////////////////////////////////////
    // Erase detector scene graph, but also clear the
    // XML detector caches.
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
  {
    SoPage* soPage = ui_SoPage(aUI);
    if(!soPage) return;
    SoRegion* soRegion = ui_SoRegion(aUI);
    if(!soRegion) return;
    if(!soRegion->isOfType(SoDisplayRegion::getClassTypeId())) return;
    SoDisplayRegion* displayRegion = (SoDisplayRegion*)soRegion;
    displayRegion->getStaticScene()->removeAllChildren();

    // Clear XML detector caches :
    IUserInterfaceSvc* uiSvc = find_uiSvc(aUI.session());
    if(!uiSvc) return;
    uiSvc->clearDetectorStore();
  }
  //////////////////////////////////////////////////////////////////////////////
  void visualize_velo(
                      IUI& aUI
                      )
    //////////////////////////////////////////////////////////////////////////////
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
  {
    IUserInterfaceSvc* uiSvc = find_uiSvc(aUI.session());
    if(!uiSvc) return;

    std::string svelo = "/dd/Structure/LHCb/BeforeMagnetRegion/Velo";

    int i;
    for(i=0;i<=40;i+=2) {
      std::string s;
      std::string fmt = svelo + "/VeloLeft/Module%02d";
      Lib::smanip::printf(s,128,fmt.c_str(),i);
      uiSvc->visualize(s);
    }

    for(i=1;i<=41;i+=2) {
      std::string s;
      std::string fmt = svelo + "/VeloRight/Module%02d";
      Lib::smanip::printf(s,128,fmt.c_str(),i);
      uiSvc->visualize(s);
    }

    uiSvc->visualize("/dd/Structure/LHCb/BeforeMagnetRegion/Velo/VeloRight/ModulePU01");
    uiSvc->visualize("/dd/Structure/LHCb/BeforeMagnetRegion/Velo/VeloRight/ModulePU03");
    uiSvc->visualize("/dd/Structure/LHCb/BeforeMagnetRegion/Velo/VeloLeft/ModulePU00");
    uiSvc->visualize("/dd/Structure/LHCb/BeforeMagnetRegion/Velo/VeloLeft/ModulePU02");
  }
  //////////////////////////////////////////////////////////////////////////////
  void visualize_LHCb(
                      IUI& aUI
                      )
    //////////////////////////////////////////////////////////////////////////////
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
  {
    IUserInterfaceSvc* uiSvc = find_uiSvc(aUI.session());
    if(!uiSvc) return;

    visualize_velo(aUI);

    uiSvc->visualize("/dd/Structure/LHCb/BeforeMagnetRegion/Velo");
    uiSvc->visualize("/dd/Structure/LHCb/BeforeMagnetRegion/Velo2Rich1");
    uiSvc->visualize("/dd/Structure/LHCb/BeforeMagnetRegion/Rich1");
    uiSvc->visualize("/dd/Structure/LHCb/MagnetRegion/Magnet");
    uiSvc->visualize("/dd/Structure/LHCb/AfterMagnetRegion/T/IT");
    uiSvc->visualize("/dd/Structure/LHCb/AfterMagnetRegion/T/OT");
    uiSvc->visualize("/dd/Structure/LHCb/AfterMagnetRegion/Rich2");
    uiSvc->visualize("/dd/Structure/LHCb/DownstreamRegion/Prs");
    uiSvc->visualize("/dd/Structure/LHCb/DownstreamRegion/Spd");
    uiSvc->visualize("/dd/Structure/LHCb/DownstreamRegion/Ecal");
    uiSvc->visualize("/dd/Structure/LHCb/DownstreamRegion/Hcal");
    uiSvc->visualize("/dd/Structure/LHCb/DownstreamRegion/Muon");

  }
  //////////////////////////////////////////////////////////////////////////////
  void visualize_magneticField(
                               IUI& aUI
                               )
    //////////////////////////////////////////////////////////////////////////////
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
  {
    ISession& session = aUI.session();

    // enable draggers :
    session.setParameter("modeling.magneticField.editable","true");

    //
    // We define a cutting plan.
    // In the cutting plan we define a grid of cells.
    // The magnetic field is computed at the center of each cell.
    // The cell is colored according field strength.
    //

    ////////////////////////////////////////////////////////////////////////////
    // Coloring parameters :
    //  A red coloring gradient is applied.
    ////////////////////////////////////////////////////////////////////////////
    session.setParameter("modeling.magneticField.min","0.0001");
    session.setParameter("modeling.magneticField.max","0.003");
    session.setParameter("modeling.magneticField.colorMin","0.3");
    session.setParameter("modeling.magneticField.colorMax","1");
    //session.setParameter("modeling.transparency","0.3");

    ////////////////////////////////////////////////////////////////////////////
    // Cut plan in the XY plan. Cover the magnet.
    ////////////////////////////////////////////////////////////////////////////
    int nx = 120; // Number of cell in x axis of the grid in the plan.
    int ny = 90;  // Number of cell in y axis of the grid in the plan.
    double sx = 100; // Size of a cell in x.
    double sy = 100; // Size of a cell in y.

    double posx = -(sx * nx)/2;
    double posy = -(sy * ny)/2;
    double posz = 4000;

    std::string s;
    Lib::smanip::printf(s,128,"%g %g %g",posx,posy,posz);
    session.setParameter("modeling.magneticField.gridPosition",s);
    session.setParameter("modeling.magneticField.gridX","1 0 0");
    session.setParameter("modeling.magneticField.gridY","0 1 0");

    Lib::smanip::printf(s,128,"%d %d",nx,ny);
    session.setParameter("modeling.magneticField.gridCells",s);

    Lib::smanip::printf(s,128,"%g %g",sx,sy);
    session.setParameter("modeling.magneticField.gridCellSize",s);

    data_collect(session,"MagneticField");
    data_visualize(session);

    ////////////////////////////////////////////////////////////////////////////
    // Cut plan in the ZY plan. Cover the magnet.
    ////////////////////////////////////////////////////////////////////////////
    nx = 100;
    ny = 90;
    posx = 0;
    posy = -(sy * ny)/2.;
    posz = 0;

    Lib::smanip::printf(s,128,"%g %g %g",posx,posy,posz);
    session.setParameter("modeling.magneticField.gridPosition",s);
    session.setParameter("modeling.magneticField.gridX","0 0 1");
    session.setParameter("modeling.magneticField.gridY","0 1 0");

    Lib::smanip::printf(s,128,"%d %d",nx,ny);
    session.setParameter("modeling.magneticField.gridCells",s);

    Lib::smanip::printf(s,128,"%g %g",sx,sy);
    session.setParameter("modeling.magneticField.gridCellSize",s);

    data_collect(session,"MagneticField");
    data_visualize(session);
  }
  //////////////////////////////////////////////////////////////////////////////
  void ecal_digits(
                   IUI& aUI
                   )
    //////////////////////////////////////////////////////////////////////////////
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
  {
    ISession& session = aUI.session();

    session_setColor(session,"yellow");

    data_collect(session,"EcalDigits","(e>=1000)&&(e<2000)");

    data_visualize(session);

    session_setColor(session,"red");

    data_collect(session,"EcalDigits","e>=2000");

    data_visualize(session);
  }
  //////////////////////////////////////////////////////////////////////////////
  void ecal_clusters(
                     IUI& aUI
                     )
    //////////////////////////////////////////////////////////////////////////////
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
  {
    ISession& session = aUI.session();

    session_setColor(session,"yellow");

    data_collect(session,"EcalClusters");

    data_visualize(session);
  }
  //////////////////////////////////////////////////////////////////////////////
  void B_decay(
               IUI& aUI
               )
    //////////////////////////////////////////////////////////////////////////////
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
  {
    ISession& session = aUI.session();

    //
    //  Particles defined in $LHCBDBASE/cdf/particle.cdf
    //

    session.setParameter("modeling.showText","false");

    // size of text is an integer. It corresponds to the
    // "number of points" of a TTF font. This number
    // goes directly to the SoTextTTF node that passes it
    // to the freetype font renderer.
    session.setParameter("modeling.sizeText","20");

    // B :
    session.setParameter("modeling.color","white");
    data_collect(session,"MCParticle","particle==\"B0\" || particle==\"B~0\" || particle==\"B_s0\" || particle==\"B_s~0\"");
    session.setParameter("modeling.showText","true");
    session.setParameter("modeling.posText","medium");
    data_visualize(session);
    session.setParameter("modeling.showText","false");

    // psi :
    session.setParameter("modeling.color","magenta");
    data_collect(session,"MCParticle","particle==\"J/psi(1S)\"");
    data_visualize(session);

    // KS0 from B :
    session.setParameter("modeling.color","yellow");
    data_collect(session,"MCParticle","particle==\"KS0\" && (parent==\"B0\"||parent==\"B~0\"||parent==\"B_s0\"||parent==\"B_s~0\")");
    data_visualize(session);

    // D_s+, D_s- from B :
    session.setParameter("modeling.color","yellow");
    data_collect(session,"MCParticle","(particle==\"D_s+\"||particle==\"D_s-\") && (parent==\"B0\"||parent==\"B~0\"||parent==\"B_s0\"||parent==\"B_s~0\")");
    session.setParameter("modeling.showText","true");
    session.setParameter("modeling.posText","medium");
    data_visualize(session);
    session.setParameter("modeling.showText","false");

    // pi+, pi- from B :
    session.setParameter("modeling.color","violet");
    data_collect(session,"MCParticle","(particle==\"pi+\"||particle==\"pi-\") && (parent==\"B0\"||parent==\"B~0\"||parent==\"B_s0\"||parent==\"B_s~0\")");
    session.setParameter("modeling.showText","true");
    session.setParameter("modeling.posText","3");
    data_visualize(session);
    session.setParameter("modeling.showText","false");

    // e from psi :
    session.setParameter("modeling.color","cyan");
    data_collect(session,"MCParticle","(particle==\"e+\"||particle==\"e-\") && (parent==\"J/psi(1S)\") && (parent2==\"B0\"||parent2==\"B~0\"||parent2==\"B_s0\"||parent2==\"B_s~0\")");
    data_visualize(session);

    // mu from psi :
    session.setParameter("modeling.color","blue");
    data_collect(session,"MCParticle","(particle==\"mu+\"||particle==\"mu-\") && (parent==\"J/psi(1S)\") && (parent2==\"B0\"||parent2==\"B~0\"||parent2==\"B_s0\"||parent2==\"B_s~0\")");
    data_visualize(session);

    // pi+, pi- from KS0 :
    session.setParameter("modeling.color","violet");
    data_collect(session,"MCParticle","(particle==\"pi+\"||particle==\"pi-\") && (parent==\"KS0\") && (parent2==\"B0\"||parent2==\"B~0\"||parent2==\"B_s0\"||parent2==\"B_s~0\")");
    data_visualize(session);

    // pi0 from KS0 :
    session.setParameter("modeling.color","skyblue");
    data_collect(session,"MCParticle","particle==\"pi0\" && parent==\"KS0\" && (parent2==\"B0\"||parent2==\"B~0\"||parent2==\"B_s0\"||parent2==\"B_s~0\")");
    data_visualize(session);

    // K+, K- from D_s+ D_s- :
    session.setParameter("modeling.color","lightblue");
    data_collect(session,"MCParticle","(particle==\"K+\" || particle==\"K-\") && (parent==\"D_s+\"||parent==\"D_s-\") && (parent2==\"B0\"||parent2==\"B~0\"||parent2==\"B_s0\"||parent2==\"B_s~0\")");
    session.setParameter("modeling.showText","true");
    session.setParameter("modeling.posText","3");
    data_visualize(session);
    session.setParameter("modeling.showText","false");

    // pi+ pi- from D_s+ D_s- :
    session.setParameter("modeling.color","violet");
    data_collect(session,"MCParticle","(particle==\"pi+\" || particle==\"pi-\" ) && (parent==\"D_s+\"||parent==\"D_s-\") && (parent2==\"B0\"||parent2==\"B~0\"||parent2==\"B_s0\"||parent2==\"B_s~0\")");
    session.setParameter("modeling.showText","true");
    session.setParameter("modeling.posText","3");
    data_visualize(session);
    session.setParameter("modeling.showText","false");

    // K+, K- from K*(892)0 :
    session.setParameter("modeling.color","lightblue");
    data_collect(session,"MCParticle","(particle==\"K+\" || particle==\"K-\" ) && (parent==\"K*(892)0\") && (parent2==\"B0\"||parent2==\"B~0\"||parent2==\"B_s0\"||parent2==\"B_s~0\")");
    data_visualize(session);

    // pi+, pi- from K*(892)0 :
    session.setParameter("modeling.color","violet");
    data_collect(session,"MCParticle","(particle==\"pi+\" || particle==\"pi-\" ) && (parent==\"K*(892)0\") && (parent2==\"B0\"||parent2==\"B~0\"||parent2==\"B_s0\"||parent2==\"B_s~0\")");
    data_visualize(session);
  }
  //////////////////////////////////////////////////////////////////////////////
  void gaudi_histo_example(
                           IUI& aUI
                           )
    //////////////////////////////////////////////////////////////////////////////
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
  {
    ISession& session = aUI.session();

    IHistogramSvc* histogramSvc = find_histogramSvc(session);
    if(!histogramSvc) {
      aUI.echo("gaudi_histo_example : unable to find HistogramSvc.");
      return;
    }

    IHistogram1D* histo = 0;
    if(histogramSvc->findObject
       ("panoramix_Gaudi_histo",histo)!=StatusCode::SUCCESS) {
      histo = histogramSvc->book("panoramix_Gaudi_histo",
                                 "Gaudi histo from DLD",100,-5,5);
    }
    if(!histo) return;

    Lib::RandomGauss rg = Lib::RandomGauss(0,1);
    for(int index=0;index<10000;index++) histo->fill(rg.shoot(),1);

  }
  //////////////////////////////////////////////////////////////////////////////
  /// Some event scripts ///////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
  void event_rec_0(
                   IUI& aUI
                   )
    //////////////////////////////////////////////////////////////////////////////
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
  {
    ISession& session = aUI.session();
    session.setParameter("modeling.magneticField.min","0.0001");

    std::string filter = "(match==true)&&(unique==true)";

    //ui.echo(" Visualize Tracks "+filter+" (cyan)");
    //ui.echo("with their measurements (magenta) and ");
    //ui.echo("their associated MCParticles (yellow).");

    session.setParameter("modeling.showCurve","true");
    session.setParameter("modeling.useExtrapolator","true");
    session_setColor(session,"cyan");
    data_collect(session,"Track",filter);
    data_visualize(session);

    session.setParameter("modeling.what","clusters");
    session_setColor(session,"magenta");
    data_collect(session,"Track",filter);
    data_visualize(session);
    session.setParameter("modeling.what","no");

    session.setParameter("modeling.what","MCParticle");
    session_setColor(session,"yellow");
    data_collect(session,"Track",filter);
    data_visualize(session);
    session.setParameter("modeling.what","no");

    session.setParameter("modeling.what","OTMeasurements");
    session_setColor(session,"red");
    data_collect(session,"Track",filter);
    data_visualize(session);
    session.setParameter("modeling.what","no");

    session.setParameter("modeling.what","TTMeasurements");
    session_setColor(session,"magenta");
    data_collect(session,"Track",filter);
    data_visualize(session);
    session.setParameter("modeling.what","no");

    session.setParameter("modeling.what","ITMeasurements");
    session_setColor(session,"magenta");
    data_collect(session,"Track",filter);
    data_visualize(session);
    session.setParameter("modeling.what","VeloMeasurements");
    session_setColor(session,"cyan");
    data_collect(session,"Track",filter);
    data_visualize(session);
    session.setParameter("modeling.what","no");

  }
  //////////////////////////////////////////////////////////////////////////////
  void event_velo(
                  IUI& aUI
                  )
    //////////////////////////////////////////////////////////////////////////////
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
  {
    ISession& session = aUI.session();

    // With ZR projection, should be a point :
    session_setColor(session,"cyan");
    data_collect(session,"VeloCluster","isR==true");
    data_visualize(session);

    session_setColor(session,"red");
    data_collect(session,"VeloCluster","isR==false");
    data_visualize(session);

  }
  //////////////////////////////////////////////////////////////////////////////
  void event_raw_all(
                     IUI& aUI
                     )
    //////////////////////////////////////////////////////////////////////////////
    // This callback is to check the overall speed.
    // Try to have a maximum of "Raw" data.
    // If possible, use the data_collect to check speed.
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
  {
    ISession& session = aUI.session();

    IUserInterfaceSvc* uiSvc = find_uiSvc(session);
    if(!uiSvc) return;

    session_setColor(session,"red");
    uiSvc->visualize("/Event/Raw/TT/Clusters");

    session_setColor(session,"green");
    uiSvc->visualize("/Event/Raw/IT/Clusters");

    session_setColor(session,"blue");
    uiSvc->visualize("/Event/Raw/OT/Times");

    session_setColor(session,"yellow");
    data_collect(session,"VeloCluster");
    data_visualize(session);

    session_setColor(session,"magenta");
    data_collect(session,"EcalDigits");
    data_visualize(session);

    session_setColor(session,"cyan");
    data_collect(session,"HcalDigits");
    data_visualize(session);

    session_setColor(session,"red");
    data_collect(session,"PrsDigits");
    data_visualize(session);

    session_setColor(session,"green");
    data_collect(session,"SpdDigits");
    data_visualize(session);

    session_setColor(session,"blue");
    data_collect(session,"MuonCoord");
    data_visualize(session);

  }
  void Panoramix_tree_create(IUI&);
  //////////////////////////////////////////////////////////////////////////////
  void Panoramix_tree_option(
                             IUI& aUI
                             )
    //////////////////////////////////////////////////////////////////////////////
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
  {
    ISession& session = aUI.session();
    std::string item = aUI.callbackValue();

    if(item=="Detector") {
      Panoramix_tree_create(aUI);

    } else if(item == "Event") {
      Panoramix_tree_create(aUI);

    } else if(item == "Stat") {
      Panoramix_tree_create(aUI);

    } else if(item == "Algorithms") {

      IAlgManager* algorithmManager = find_algorithmManager(session);
      if(!algorithmManager) {
        Lib::Out out(session.printer());
        out << "Panoramix_tree_option :"
            << " unable to find algorithm manager."
            << Lib::endl;
        return;
      }

      std::string out = "<tree>";
      for(const auto& i : algorithmManager->getAlgorithms() ) { 
        out +=  "<treeItem><label>" + i->name() + "</label></treeItem>";
      }
      out += "</tree>";

      aUI.setParameter("mainTree.items",out);

    } else if(item=="AIDA trees") {

      aUI.executeScript("DLD","OnX ui_set_parameter @{OnXLab.tree}@ @cat@ .items @session@hierarchy@{OnXLab_tree.what}@");

    }
  }
  //////////////////////////////////////////////////////////////////////////////
  void Panoramix_tree_select(
                             IUI& aUI
                             )
    //////////////////////////////////////////////////////////////////////////////
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
  {
    ISession& session = aUI.session();

    std::string what;
    if(!aUI.parameterValue("optionTree.value",what)) return;

    if(what == "AIDA trees") {

      aUI.executeScript("DLD","OnXLab OnXLab_tree_selection @this.value@ @current@ @{OnXLab_tree.append}@ @{OnXLab_tree.show_palette}@ @{OnXLab_tree.selection_plot}@");

    } else if(what == "Algorithms") {
      IAlgManager* algorithmManager = find_algorithmManager(session);
      if(!algorithmManager) {
        Lib::Out out(session.printer());
        out << "Panoramix_tree_select :"
            << " unable to find algorithm manager."
            << Lib::endl;
        return;
      }

      std::string selection;
      if(!aUI.parameterValue("mainTree.selection",selection)) return;

      // search for the algorithm
      IAlgorithm* algorithm = 0;
      StatusCode status = algorithmManager->getAlgorithm( selection, algorithm );
      if((status!=StatusCode::SUCCESS) || (!algorithm)) {
        Lib::Out out(session.printer());
        out << "Panoramix_tree_select :"
            << " unable to find algorithm : " << selection
            << Lib::endl;
        return;
      }

      // look for properties interface
      IProperty* prop = 0;
      status = algorithm->queryInterface( IProperty::interfaceID(), (void**) &prop );
      if( (status!=StatusCode::SUCCESS) || (!prop)) {
        Lib::Out out(session.printer());
        out << "Panoramix_tree_select :"
            << " algorithm : " << selection
            << " has no property interface."
            << Lib::endl;
        return;
      }

      const std::vector<Property*>& props = prop->getProperties();
      Lib::Out out(session.printer());
      out << "Panoramix_tree_select :"
          << " algorithm : " << selection
          << " properties (" << (int)(props.size()) << ") : "
          << Lib::endl;

      int lname = 0;
      std::vector<Property*>::const_iterator it;
      for(it=props.begin();it!=props.end();++it) {
        int l = (*it)->name().size();
        lname = l>lname ? l : lname;
      }

      for(it=props.begin();it!=props.end();++it) {
        std::string type = (*it)->type();
        Lib::smanip::justify(type,5);
        std::string name = (*it)->name();
        Lib::smanip::justify(name,lname+1);
        std::string value;
        propertyToString(*(*it),value);
        out << " type : " << type
            << ", name : " << name
            << ", value : " << value
            << Lib::endl;
      }

    } else {
      IUserInterfaceSvc* uiSvc = find_uiSvc(aUI.session());
      if(!uiSvc) return;
      std::string selection;
      if(!aUI.parameterValue("mainTree.selection",selection)) return;
      Lib::smanip::replace(selection,"\n","/");
      std::string s = "/" + selection;
      //FIXME : GB : 
      //        in PMX GUI, an "open" induces a "selectBranch" ! This is not correct.
      //        Then "open" induces noisy message on "/".
      //        Waiting an understanding of the problem, we simply
      //        bypass the "/" case here.
      if(s!="/") { 
        aUI.echo("Panoramix_tree_select : " + s);
        uiSvc->visualize(s);
      }
    }
  }
  //////////////////////////////////////////////////////////////////////////////
  /// Input dialogs ////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
  void Panoramix_InputLayout_ok(
                                IUI& aUI
                                )
    //////////////////////////////////////////////////////////////////////////////
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
  {
    std::string layout;
    if(!aUI.parameterValue("Panoramix_InputLayout_input_layout.value",layout))
      return;
    aUI.executeScript("DLD",layout);
  }
  //////////////////////////////////////////////////////////////////////////////
  void Panoramix_InputSlice_ok(
                               IUI& aUI
                               )
    //////////////////////////////////////////////////////////////////////////////
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
  {
    double z;
    std::string sz;
    if(!aUI.parameterValue("Panoramix_InputSlice_input_z.value",sz)) return;
    if(!Lib::smanip::todouble(sz,z)) return;

    double width;
    std::string swidth;
    if(!aUI.parameterValue("Panoramix_InputSlice_input_width.value",swidth))
      return;
    if(!Lib::smanip::todouble(swidth,width)) return;

    SoPage* soPage = ui_SoPage(aUI);
    if(!soPage) return;
    SoRegion* soRegion = ui_SoRegion(aUI);
    if(!soRegion) return;
    SoCamera* soCamera = soRegion->getCamera();
    if(!soCamera->isOfType(SoOrthographicCamera::getClassTypeId())) return;
    SoOrthographicCamera* camera = (SoOrthographicCamera*)soCamera;

    camera->position.setValue(0,0,(float)z);
    camera->orientation.setValue(SbRotation(SbVec3f(0,1,0),0));
    //camera->height.setValue(10);
    camera->nearDistance.setValue(0);
    camera->farDistance.setValue(float(width));
    camera->focalDistance.setValue((float)z);

    Slash::UI::ISoViewer* soViewer = ui_SoViewer(aUI);
    if(soViewer) {
      soViewer->setAutoClipping(false);
    }
  }
  //////////////////////////////////////////////////////////////////////////////
  void Panoramix_InputEventLoop_ok(
                                   IUI& aUI
                                   )
    //////////////////////////////////////////////////////////////////////////////
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
  {
    int number;
    if(!ui_parameterValue(aUI,"Panoramix_InputEventLoop_input_number.value",
                          number))
      return;

    ISession& session = aUI.session();

    session_setParameter(session,"Panoramix.indexOfEvent",0);
    session_setParameter(session,"Panoramix.numberOfEvent",number);

    ICyclic* cyclic = aUI.findCyclic("Panoramix_eventLoop");
    if(!cyclic) cyclic = aUI.createCyclic("Panoramix_eventLoop",0,"","");
    if(!cyclic) return;
    cyclic->setDelay(1000);
    cyclic->setScript("DLD","Panoramix cyclic_next_event");
    cyclic->start();

  }
  //////////////////////////////////////////////////////////////////////////////
  void cyclic_next_event(
                         IUI& aUI
                         )
    //////////////////////////////////////////////////////////////////////////////
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
  {
    ISession& session = aUI.session();

    int index;
    if(!session_parameterValue(session,"Panoramix.indexOfEvent",index)) return;
    int number;
    if(!session_parameterValue(session,"Panoramix.numberOfEvent",number)) return;

    std::string s;
    Lib::smanip::printf(s,128,"event loop %d (%d requested)",index,number);
    aUI.echo(s);

    if (index < number) {
      next_event(aUI);
      index++;
      session_setParameter(session,"Panoramix.indexOfEvent",index);
    } else {
      // Stop the cyclic :
      ICyclic* cyclic = aUI.findCyclic("Panoramix_eventLoop");
      if(!cyclic) return;
      cyclic->stop();
    }

  }
  //////////////////////////////////////////////////////////////////////////////
  void Panoramix_InputNextEventScript_ok(
                                         IUI& aUI
                                         )
    //////////////////////////////////////////////////////////////////////////////
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
  {
    std::string script;
    if(!aUI.parameterValue("Panoramix_InputNextEventScript_input_script.value",
                           script)) return;

    ISession& session = aUI.session();

    session.setParameter("panoramix.eventScript",script);

    aUI.echo("Panoramix_InputNextEventScript_ok : eventScript set to \""+script+"\"");
  }
  //////////////////////////////////////////////////////////////////////////////
  void Panoramix_Algorithms_update(
                                   IUI& aUI
                                   )
    //////////////////////////////////////////////////////////////////////////////
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
  {
    ISession& session = aUI.session();

    IAlgManager* algorithmManager = find_algorithmManager(session);
    if(!algorithmManager) {
      Lib::Out out(session.printer());
      out << "Panoramix_Algorithms_update :"
          << " unable to find algorithm manager."
          << Lib::endl;
      aUI.setParameter("Panoramix_Algorithms_list.items","");
      return;
    }

    std::string out;
    for(const auto& i : algorithmManager->getAlgorithms()) {
      out +=  i->name() + "\n";
    }

    aUI.setParameter("Panoramix_Algorithms_list.items",out);
  }
  //////////////////////////////////////////////////////////////////////////////
  void Panoramix_Algorithms_select(
                                   IUI& aUI
                                   )
    //////////////////////////////////////////////////////////////////////////////
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
  {
    ISession& session = aUI.session();

    std::string selection = aUI.callbackValue();

    IAlgManager* algorithmManager = find_algorithmManager(session);
    if(!algorithmManager) {
      Lib::Out out(session.printer());
      out << "Panoramix_Algorithms_select :"
          << " unable to find algorithm manager."
          << Lib::endl;
      return;
    }

    // search for the algorithm
    IAlgorithm* algorithm = 0;
    StatusCode status = algorithmManager->getAlgorithm( selection, algorithm );
    if((status!=StatusCode::SUCCESS) || (!algorithm)) {
      Lib::Out out(session.printer());
      out << "Panoramix_Algorithms_select :"
          << " unable to find algorithm : " << selection
          << Lib::endl;
      return;
    }

    // look for properties interface
    IProperty* prop = 0;
    status = algorithm->queryInterface( IProperty::interfaceID(), (void**) &prop );
    if( (status!=StatusCode::SUCCESS) || (!prop)) {
      Lib::Out out(session.printer());
      out << "Panoramix_Algorithms_select :"
          << " algorithm : " << selection
          << " has no property interface."
          << Lib::endl;
      return;
    }

    const std::vector<Property*>& props = prop->getProperties();
    Lib::Out out(session.printer());
    out << "Panoramix_Algorithms_select :"
        << " algorithm : " << selection
        << " properties (" << (int)(props.size()) << ") : "
        << Lib::endl;

    int lname = 0;
    std::vector<Property*>::const_iterator it;
    for(it=props.begin();it!=props.end();++it) {
      int l = (*it)->name().size();
      lname = l>lname ? l : lname;
    }

    for(it=props.begin();it!=props.end();++it) {
      std::string type = (*it)->type();
      Lib::smanip::justify(type,5);
      std::string name = (*it)->name();
      Lib::smanip::justify(name,lname+1);
      std::string value;
      propertyToString(*(*it),value);
      out << " type : " << type
          << ", name : " << name
          << ", value : " << value
          << Lib::endl;
    }
  }
  //////////////////////////////////////////////////////////////////////////////
  void Panoramix_Algorithms_do(
                               IUI& aUI
                               ,const std::vector<std::string>& aArgs
                               )
    //////////////////////////////////////////////////////////////////////////////
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
  {
    if(!aArgs.size()) return;

    ISession& session = aUI.session();

    std::string selection;
    if(!aUI.parameterValue("Panoramix_Algorithms_list.selection",selection))
      return;

    IAlgManager* algorithmManager = find_algorithmManager(session);
    if(!algorithmManager) {
      Lib::Out out(session.printer());
      out << "Panoramix_Algorithms_initialize :"
          << " unable to find algorithm manager."
          << Lib::endl;
      return;
    }

    // search for the algorithm
    IAlgorithm* algorithm = 0;
    StatusCode status = algorithmManager->getAlgorithm( selection, algorithm );
    if((status!=StatusCode::SUCCESS) || (!algorithm)) {
      Lib::Out out(session.printer());
      out << "Panoramix_Algorithms_initialize :"
          << " unable to find algorithm : " << selection
          << Lib::endl;
      return;
    }

    if(aArgs[0]=="initialize") {
      algorithm->initialize();
    } else if(aArgs[0]=="execute") {
      algorithm->execute();
    } else if(aArgs[0]=="finalize") {
      algorithm->finalize();

    } else if(aArgs[0]=="sysInitialize") {
      algorithm->sysInitialize();
    } else if(aArgs[0]=="sysExecute") {
      algorithm->sysExecute();
    } else if(aArgs[0]=="sysFinalize") {
      algorithm->sysFinalize();

    } else if(aArgs[0]=="beginRun") {
      algorithm->finalize();
    } else if(aArgs[0]=="endRun") {
      algorithm->finalize();
    }

  }
  //////////////////////////////////////////////////////////////////////////////
  void Panoramix_Services_update(
                                 IUI& aUI
                                 )
    //////////////////////////////////////////////////////////////////////////////
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
  {
    ISession& session = aUI.session();

    ISvcLocator* svcLocator = find_svcLocator(session);
    if(!svcLocator) {
      Lib::Out out(session.printer());
      out << "Panoramix_Services_update :"
          << " unable to find service locator."
          << Lib::endl;
      aUI.setParameter("Panoramix_Services_list.items","");
      return;
    }

    std::string out;
    const std::list<IService*>& services = svcLocator->getServices();
    std::list< IService*>::const_iterator it;
    for(it=services.begin();it!=services.end();it++) {
      out +=  (*it)->name() + "\n";
    }

    aUI.setParameter("Panoramix_Services_list.items",out);
  }
  //////////////////////////////////////////////////////////////////////////////
  void Panoramix_Services_select(
                                 IUI& aUI
                                 )
    //////////////////////////////////////////////////////////////////////////////
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
  {
    ISession& session = aUI.session();

    std::string selection = aUI.callbackValue();

    IService* service = find_service(session,selection);
    if(!service) return;

    Lib::Out out(session.printer());
    out << "Panoramix_Services_select :"
        << " name : " << service->name()
        << Lib::endl;

  }
  //////////////////////////////////////////////////////////////////////////////
  void Panoramix_Services_do(
                             IUI& aUI
                             ,const std::vector<std::string>& aArgs
                             )
    //////////////////////////////////////////////////////////////////////////////
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
  {
    if(!aArgs.size()) return;

    ISession& session = aUI.session();

    std::string selection;
    if(!aUI.parameterValue("Panoramix_Services_list.selection",selection))
      return;

    IService* service = find_service(session,selection);
    if(!service) return;

    if(aArgs[0]=="initialize") {
      service->initialize();
    } else if(aArgs[0]=="reinitialize") {
      service->reinitialize();
    } else if(aArgs[0]=="finalize") {
      service->finalize();
    }

  }
  //////////////////////////////////////////////////////////////////////////////
  /// SoEvent //////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
  void SoEvent_MCParticle_ok(
                             IUI& aUI
                             )
    //////////////////////////////////////////////////////////////////////////////
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
  {
    // Dialog inputs :
    std::string container;
    if(!aUI.parameterValue("SoEvent_MCParticle_input_container.value",container)) return;
    std::string stable;
    if(!aUI.parameterValue("SoEvent_MCParticle_input_stable.value",stable)) return;
    std::string kind;
    if(!aUI.parameterValue("SoEvent_MCParticle_input_kind.value",kind)) return;
    std::string energy;
    if(!aUI.parameterValue("SoEvent_MCParticle_input_energy.value",energy)) return;
    std::string scolor;
    if(!aUI.parameterValue("SoEvent_MCParticle_input_color.value",scolor)) return;
    std::string linewidth;
    if(!aUI.parameterValue("SoEvent_MCParticle_input_linewidth.value",linewidth)) return;
    std::string text;
    if(!aUI.parameterValue("SoEvent_MCParticle_input_text.value",text)) return;
    std::string textsize;
    if(!aUI.parameterValue("SoEvent_MCParticle_input_textsize.value",textsize)) return;
    std::string textpos;
    if(!aUI.parameterValue("SoEvent_MCParticle_input_textpos.value",textpos)) return;
    std::string action;
    if(!aUI.parameterValue("SoEvent_MCParticle_input_action.value",action)) return;

    ISession& session = aUI.session();

    // Visualization parameters :
    std::string srgb;
    Lib::smanip::torgbs(scolor,srgb);
    session.setParameter("modeling.color",srgb);
    session.setParameter("modeling.lineWidth",linewidth);
    session.setParameter("modeling.sizeText",textsize);
    session.setParameter("modeling.posText",textpos);
    if ( text == "yes" ) {
      session.setParameter("modeling.showText","true");
    } else  {
      session.setParameter("modeling.showText","false");
    }

    // Data filtering :
    std::string filter = "";
    if (energy != "" ){
      filter = "(energy>"+energy+")";
    }
    if (kind == "charged") {
      filter = "("+filter+")&&(charge!=0)";
    } else if(kind == "neutral") {
      filter = "("+filter+")&&(charge==0)";
    } else if(kind == "b-quark") {
      filter = "("+filter+")&&(bcflag>9)";
    } else if(kind == "c-quark") {
      filter = "("+filter+")&&(bcflag>0)&&(bcflag<10)";
    }
    if (stable != "" ){
      filter = filter + "&&(decayLength>"+stable+")";
    }

    data_collect(session,"MCParticle",filter);

    // Action :

    if(action == "visualize") {
      data_visualize(session);
    } else if( action == "dump") {
      data_dump(session);
    }

  }
  //////////////////////////////////////////////////////////////////////////////
  void SoEvent_Track_ok(
                        IUI& aUI
                        )
    //////////////////////////////////////////////////////////////////////////////
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
  {
    // Dialog inputs :
    std::string type;
    if(!aUI.parameterValue("SoEvent_Track_input_type.value",type)) return;
    std::string container;
    if(!aUI.parameterValue("SoEvent_Track_input_container.value",container)) return;
    std::string unique;
    if(!aUI.parameterValue("SoEvent_Track_input_unique.value",
                           unique)) return;
    std::string associations;
    if(!aUI.parameterValue("SoEvent_Track_input_associations.value",
                           associations)) return;
    std::string userTrackRange;
    if(!aUI.parameterValue("SoEvent_Track_input_userTrackRange.value",
                           userTrackRange)) return;
    std::string trackStartz;
    if(!aUI.parameterValue("SoEvent_Track_input_trackStartz.value",
                           trackStartz)) return;
    std::string trackEndz;
    if(!aUI.parameterValue("SoEvent_Track_input_trackEndz.value",
                           trackEndz)) return;
    std::string ptmin;
    if(!aUI.parameterValue("SoEvent_Track_input_ptmin.value",
                           ptmin)) return;
    std::string pvass;
    if(!aUI.parameterValue("SoEvent_Track_input_pvass.value",
                           pvass)) return;
    std::string Xmax;
    if(!aUI.parameterValue("SoEvent_Track_input_xmax.value",
                           Xmax)) return;
    std::string scolor;
    if(!aUI.parameterValue("SoEvent_Track_input_color.value",
                           scolor)) return;
    std::string linewidth;
    if(!aUI.parameterValue("SoEvent_Track_input_linewidth.value",
                           linewidth)) return;
    std::string action;
    if(!aUI.parameterValue("SoEvent_Track_input_action.value",
                           action)) return;
    std::string precision;
    if(!aUI.parameterValue("SoEvent_Track_input_precision.value",
                           precision)) return;

    ISession& session = aUI.session();

    // Visualization parameters :

    if(associations == "clusters") {
      session.setParameter("modeling.what","clusters");
    } else if(associations == "Measurements") {
      session.setParameter("modeling.what","Measurements");
    } else if(associations == "OTMeasurements") {
      session.setParameter("modeling.what","OTMeasurements");
    } else if(associations == "STMeasurements") {
      session.setParameter("modeling.what","STMeasurements");
    } else if(associations == "VeloMeasurements") {
      session.setParameter("modeling.what","VeloMeasurements");
    } else if(associations == "MuonCoords") {
      session.setParameter("modeling.what","Measurements");
    } else if(associations == "MCParticle") {
      session.setParameter("modeling.what","MCParticle");
    } else {
      session.setParameter("modeling.what","this");
    }

    session.setParameter("modeling.userTrackRange",userTrackRange);
    session.setParameter("modeling.trackStartz",trackStartz);
    session.setParameter("modeling.trackEndz",trackEndz);

    std::string srgb;
    Lib::smanip::torgbs(scolor,srgb);
    session.setParameter("modeling.color",srgb);

    session.setParameter("modeling.lineWidth",linewidth);
    session.setParameter("modeling.precision",precision);
    // Data filtering :

    std::string sunique;
    if(unique=="true") {
      sunique = "and(unique==true)";
    } else if(unique=="false") {
      sunique = "and(unique==false)";
    } // else all
    if      (pvass=="separated_any"){ sunique+="and(pvass==false)"; }
    else if ( pvass=="matched_any") { sunique+="and(pvass==true)"; }

    std::string ptfilter;
    ptfilter = "and(pt>"+ptmin+")";
    std::string chi2filter;
    chi2filter = "";
    double chi2cut = 0;
    Lib::smanip::todouble(Xmax,chi2cut);
    if (chi2cut>0){  chi2filter = "and(chi2dof<"+Xmax+")"; }

    session.setParameter("Track.location",container);

    if(type == "Velo") {
      data_collect(session,"Track","(velo==true)"+sunique+chi2filter);
    } else if(type == "VeloR") {
      data_collect(session,"Track","(velor==true)"+sunique);
    } else if(type == "Upstream") {
      data_collect(session,"Track","(upstream==true)"+sunique+ptfilter+chi2filter);
    } else if(type == "Long") {
      data_collect(session,"Track","(long==true)"+sunique+ptfilter+chi2filter);
    } else if(type == "Ttrack") {
      data_collect(session,"Track","(ttrack==true)"+sunique+chi2filter);
    } else if(type == "Downstream") {
      data_collect(session,"Track","(downstream==true)"+sunique+ptfilter+chi2filter);
    } else if(type == "Muon") {
      data_collect(session,"Track","(muon==true)"+sunique+ptfilter+chi2filter);
    } else if(type == "all") {
      data_collect(session,"Track","(true==true)"+sunique+ptfilter+chi2filter);
    } else if(type == "highlighted") {
      data_collect(session,s_SceneGraph,"highlight==true");
      data_filter(session,"name");
    } else if(type == "not_highlighted") {
      data_collect(session,s_SceneGraph,"highlight==false");
      data_filter(session,"name");
    }

    // Action :

    if(action == "visualize") {
      data_visualize(session);
      session.setParameter("modeling.userTrackRange","false");
    } else if( action == "number") {
      data_number(session);
    } else if( action == "dump") {
      data_dump(session);
    } else if( action == "dump_states") {
      data_filter(session,"states");
      data_dump(session);
    } else if(action == "erase tracks") {
      data_collect(session,s_SceneGraph,"name==\"Track*\"");
      data_destroy(session);
    } else if(action == "erase clusters") {
      data_collect(session,s_SceneGraph,"name==\"*Cluster*\"");
      data_destroy(session);
    }

  }
  //////////////////////////////////////////////////////////////////////////////
  void SoEvent_InputParticle_ok(
                                IUI& aUI
                                )
    //////////////////////////////////////////////////////////////////////////////
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
  {
    // Dialog inputs :
    std::string location;
    if(!aUI.parameterValue("SoEvent_InputParticle_input_location.value",location)) return;
    std::string scolor;
    if(!aUI.parameterValue("SoEvent_InputParticle_input_color.value",
                           scolor)) return;
    std::string action;
    if(!aUI.parameterValue("SoEvent_InputParticle_input_action.value",
                           action)) return;

    ISession& session = aUI.session();

    // Action :

    if(action == "visualize") {
      session.setParameter("Particle.location",location);

      // Visualization parameters :
      session.setParameter("modeling.what","this");
      std::string srgb;
      Lib::smanip::torgbs(scolor,srgb);
      session.setParameter("modeling.color",srgb);

      data_collect(session,"Particle");
      data_visualize(session);
    } else if( action == "number") {
      session.setParameter("Particle.location",location);

      data_collect(session,"Particle");
      data_number(session);
    } else if(action == "erase") {
      data_collect(session,s_SceneGraph,"name==\"Particle*\"");
      data_destroy(session);
    } else if(action == "update_locations") {
      aUI.executeScript("DLD","Panoramix InputParticle_update_locations");
    }

  }
  //////////////////////////////////////////////////////////////////////////////
  void SoEvent_InputVertex_ok(
                              IUI& aUI
                              )
    //////////////////////////////////////////////////////////////////////////////
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
  {
    // Dialog inputs :
    std::string location;
    if(!aUI.parameterValue("SoEvent_InputVertex_input_location.value",location)) return;
    std::string scolor;
    if(!aUI.parameterValue("SoEvent_InputVertex_input_color.value",
                           scolor)) return;
    std::string action;
    if(!aUI.parameterValue("SoEvent_InputVertex_input_action.value",
                           action)) return;

    ISession& session = aUI.session();

    // Action :

    if(action == "visualize") {
      session.setParameter("modeling.what","this");
      // Visualization parameters :
      std::string srgb;
      Lib::smanip::torgbs(scolor,srgb);
      session.setParameter("modeling.color",srgb);
      session.setParameter("Vertex.location",location);

      data_collect(session,"Vertex");
      data_visualize(session);
    } else if( action == "number") {
      data_number(session);
    } else if(action == "erase") {
      data_collect(session,s_SceneGraph,"name==\"Vertex*\"");
      data_destroy(session);
    } else if(action == "update_locations") {
      aUI.executeScript("DLD","Panoramix InputParticle_update_locations");
    }

  }
  //////////////////////////////////////////////////////////////////////////////
  /// SoHepMC //////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
  void SoHepMC_example(
                       IUI& aUI
                       )
    /////////////////////////////////////////////////////////////////////;/////////
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
  {
    ISession& session = aUI.session();

    //session.setParameter("modeling.sizeText","0.06");
    session.setParameter("modeling.showText","TRUE");
    session.setParameter("modeling.posText","medium");

    session_setColor(session,"yellow");
    data_collect(session,"GenParticle",
                 "particle!=\"gamma\" && beg_vtx==true && end_vtx==false && beg_vtx_d!=0");
    data_visualize(session);

    data_dump(session);
  }
  //////////////////////////////////////////////////////////////////////////////
  void SoHepMC_incoming(
                        IUI& aUI
                        )
    //////////////////////////////////////////////////////////////////////////////
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
  {
    ISession& session = aUI.session();

    data_collect(session,s_SceneGraph,"highlight==true");
    data_filter(session,"name");

    if(data_number(session) != 1) {
      aUI.echo("SoHepMC_incoming : you must have only one highlighted object.");
    } else {
#if (SLASH_MAJOR_VERSION <= 1) && (SLASH_MINOR_VERSION <= 4 )
      std::vector<std::string> vs = data_values(session,"beg_vtx_barcode");
#else
      std::vector<std::string> vs = data_values_vector(session,"beg_vtx_barcode");
#endif
      if(!vs.size()) {
        aUI.echo("SoHepMC_incoming : no beg_vtx_barcode found.");
      } else {
        data_collect(session,"GenParticle","end_vtx_barcode=="+vs[0]);
        data_visualize(session);
      }
    }
  }
  //////////////////////////////////////////////////////////////////////////////
  void SoHepMC_pdg_id(
                      IUI& aUI
                      ,const std::vector<std::string>& aArgs
                      )
    //////////////////////////////////////////////////////////////////////////////
    // aArgs[0] pdg_id (an integer, for exa 511, -511)
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
  {
    if(!aArgs.size()) return;

    ISession& session = aUI.session();

    //session.setParameter("modeling.sizeText","0.06");
    session.setParameter("modeling.showText","TRUE");
    session.setParameter("modeling.posText","medium");

    session.setParameter("modeling.showDecay","TRUE");
    session_setColor(session,"yellow");
    data_collect(session,"GenParticle","pdg_id=="+aArgs[0]);
    data_visualize(session);
    data_dump(session);

    session.setParameter("modeling.showDecay","FALSE");
    session_setColor(session,"red");
    data_collect(session,"GenParticle","pdg_id=="+aArgs[0]);
    data_visualize(session);
    data_dump(session);


  }
  //////////////////////////////////////////////////////////////////////////////
  void Panoramix_InputModelingRotationSteps_ok(
                                               IUI& aUI
                                               )
    //////////////////////////////////////////////////////////////////////////////
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
  {
    int number;
    if(!ui_parameterValue(aUI,
                          "Panoramix_InputModelingRotationSteps_input_number.value",number)) return;

    ISession& session = aUI.session();

    session_setParameter(session,"modeling.rotationSteps",number);
  }

} // extern "C"

  /// Code to handle the "on demand expension widget tree".

#include <Lib/ItemML.h>
#include <Lib/BaseML.h>

extern "C" {
  //////////////////////////////////////////////////////////////////////////////
  void Panoramix_tree_create(
                             IUI& aUI
                             )
    //////////////////////////////////////////////////////////////////////////////
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
  {
    std::string out;
    out += "<tree>";
    out += "  <treeItem>";
    out += "    <label>dd</label>";
    out += "    <treeItem><label></label></treeItem>";
    out += "  </treeItem>";
    out += "  <treeItem>";
    out += "    <label>Event</label>";
    out += "    <treeItem><label></label></treeItem>";
    out += "  </treeItem>";
    out += "  <treeItem>";
    out += "    <label>stat</label>";
    out += "    <treeItem><label></label></treeItem>";
    out += "  </treeItem>";
    /*
      out += "  <treeItem>";
      out += "    <label>Algorithms</label>";
      out += "    <treeItem><label></label></treeItem>";
      out += "  </treeItem>";
      out += "  <treeItem>";
      out += "    <label>AIDA trees</label>";
      out += "    <treeItem><label></label></treeItem>";
      out += "  </treeItem>";
    */
    out += "</tree>";
    aUI.setParameter("mainTree.items",out);
  }
  //////////////////////////////////////////////////////////////////////////////
  inline Lib::ItemML* Lib_ItemML_findItemFromPath(
                                                  Lib::ItemML& aItemML
                                                  ,const std::vector<std::string>& aPath
                                                  ,unsigned int& aIndex
                                                  )
    //////////////////////////////////////////////////////////////////////////////
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
  {
    if(aIndex>=aPath.size()) return 0;
    std::string s;
    if(aItemML.propertyValue("label",s)) { //if ! it is a <tree>, continue.
      if(aPath[aIndex]!=s) return 0; //Bad path, do not jump in children.
      // Matching item.
      if(aIndex==(aPath.size()-1)) return &(aItemML); //End of aPath.
      aIndex++;
    }
    // Look children :
    const std::list<Lib::ItemML*>& children = aItemML.children();
    std::list<Lib::ItemML*>::const_iterator it;
    for(it=children.begin();it!=children.end();++it) {
      Lib::ItemML* item = Lib_ItemML_findItemFromPath(*(*it),aPath,aIndex);
      if(item) return item;
    }
    return 0;
  }
  //////////////////////////////////////////////////////////////////////////////
  inline bool Lib_ItemML_visitToXML(
                                    Lib::ItemML& aItemML
                                    ,std::string& aOut
                                    )
    //////////////////////////////////////////////////////////////////////////////
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
  {
    std::string s;
    bool hasLabel = aItemML.propertyValue("label",s);
    // !hasLabel if head <tree>.
    if(hasLabel) aOut +=  "<treeItem><label>" + s + "</label>";
    const std::list<Lib::ItemML*>& children = aItemML.children();
    std::list<Lib::ItemML*>::const_iterator it;
    for(it=children.begin();it!=children.end();++it) {
      if(!Lib_ItemML_visitToXML(*(*it),aOut)) return false;
    }
    if(hasLabel) aOut += "</treeItem>";
    return true;
  }
  //////////////////////////////////////////////////////////////////////////////
  void Panoramix_tree_open_branch(
                                  IUI& aUI
                                  )
    //////////////////////////////////////////////////////////////////////////////
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
  {
    ISession& session = aUI.session();

    std::string branch = aUI.callbackValue();
    //printf("debug : branch + \"%s\"\n",branch.c_str());

    std::vector<std::string> items;
    Lib::smanip::words(branch,"\n",items,true);
    if(!items.size()) return;

    std::string what = items[0];
    if((what=="dd")||(what=="Event")||(what=="stat")) {
      IUserInterfaceSvc* uiSvc = find_uiSvc(session);
      if(!uiSvc) return;
      Lib::smanip::replace(branch,"\n","/");
      std::string s = "/" + branch;

      //aUI.echo("Panoramix_tree_open_branch : " + s);

      std::vector<std::string> text = uiSvc->dataChildren(s);
      if(!text.size()) return; //no children.

      std::string svalue;
      aUI.parameterValue("mainTree.items",svalue);
      //printf("debug : items : \"%s\"\n",svalue.c_str());

      Lib::ItemMLFactory factory;
      Lib::BaseML treeML(factory);
      std::vector<std::string> tags;
      tags.push_back("tree");
      tags.push_back("treeItem");
      treeML.setTags(tags);
      if(!treeML.loadString(svalue,session.printer())) return;
      Lib::ItemML* top = treeML.topItem();
      if(!top) return;
      unsigned int pathi = 0;
      Lib::ItemML* branchML = Lib_ItemML_findItemFromPath(*top,items,pathi);
      if(!branchML) return;
      branchML->deleteChildren();
      for(unsigned int index=0;index<text.size();index++) {
        //out +=  "<treeItem><label>" + text[index] + "</label></treeItem>";
        std::vector<Lib::ItemML::Attribute> atbs;
        Lib::ItemML* childML = factory.create("",atbs,branchML);
        if(!childML) return;
        std::string p,n,s;
        Lib::smanip::pathNameSuffix(text[index],p,n,s);

        childML->addProperty("label",atbs,n);
        branchML->addChild(childML);

        if(uiSvc->dataChildren(text[index]).size()) { // Is it a leaf ?
          // Add an empty item for the moment :
          std::vector<Lib::ItemML::Attribute> atbs;
          Lib::ItemML* subChildML = factory.create("",atbs,childML);
          if(!subChildML) return;
          subChildML->addProperty("label",atbs,"");
          childML->addChild(subChildML);
        }
      }

      std::string out = "<tree>";
      if(!Lib_ItemML_visitToXML(*top,out)) return;
      out += "</tree>";

      aUI.setParameter("mainTree.items",out);

    }
  }
  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////

} // extern "C"

// Local helpers (do not need to be extern "C") :
//////////////////////////////////////////////////////////////////////////////
static bool propertyToString(
                             const Property& aProperty
                             ,std::string& aValue
                             )
{
  aValue = "";
  Lib::smanip::printf(aValue,32,"%d",aProperty.toString().c_str());
  return true;
}
