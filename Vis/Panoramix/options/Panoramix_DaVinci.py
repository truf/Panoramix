from Gaudi.Configuration import *
# options common to all Panoramix use cases
importOptions("$CALORECOROOT/options/CaloRecoOnDemand.opts")

# General sequence for ProtoParticle recalibration in DaVinci
from Configurables import GaudiSequencer,ChargedProtoCombineDLLsAlg
protoPRecalibration = GaudiSequencer('ProtoPRecalibration')
protoPRecalibration.MeasureTime = True

# Hack until next full release
from Configurables import LHCbApp, DstConf
if LHCbApp().getProp("DataType") == "DC06" :
 # dirty trick
 DstConf().EnableUnpack = False
 importOptions( "$MUONIDROOT/options/MuonID.py" )
 from Configurables import MuonID,MuonRec,UpdateMuonPIDInProtoP
# protoPRecalibration.Members += [MuonRec(),MuonID(),UpdateMuonPIDInProtoP()]
 makeMuonTracks = GaudiSequencer('MakeMuonTracks')  
 makeMuonTracks.Members += [MuonRec(),MuonID(),UpdateMuonPIDInProtoP()]
 DataOnDemandSvc().AlgMap["/Event/Rec/Track/Muon"] = makeMuonTracks

# Fills the combined DLL values in the charged Proto Particles
protoPRecalibration.Members += [ ChargedProtoCombineDLLsAlg() ]

# create Muon Tracks if not present,  old DC06 DSTs 
from Configurables import TESCheck
testMuonTracks        =  TESCheck("TestMuonTracks")
testMuonTracks.Inputs = ["/Event/Rec/Track/Muon"]
protoPRecalibration.Members += [ testMuonTracks ]
            
protoPRecalibration.Members += [ "MuonPIDsFromProtoParticlesAlg/MuonPIDsFromProtos" ]
protoPRecalibration.Members += [ "RichPIDsFromProtoParticlesAlg/RichPIDsFromProtos" ]

ApplicationMgr().TopAlg += [protoPRecalibration]

importOptions("$COMMONPARTICLESROOT/options/StandardOptions.py")
importOptions("$COMMONPARTICLESROOT/options/StandardDC06Options.opts")
importOptions("$DAVINCIASSOCIATORSROOT/options/DaVinciAssociators.opts")


