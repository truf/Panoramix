from Gaudi.Configuration import *
from Configurables import (TESCheck,EventNodeKiller)

cleanSeq = GaudiSequencer("CleanUpSequence")

EvtCheck = TESCheck('EvtCheck')
EvtCheck.Inputs       = ["Link/Rec/Track/Best"]
EventNodeKiller = EventNodeKiller()
EventNodeKiller.Nodes = [ "Rec", "pRec", "Raw", "Link/Rec" ]

cleanSeq.Members += [EvtCheck,EventNodeKiller]

