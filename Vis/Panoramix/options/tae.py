from Gaudi.Configuration import *
import GaudiKernel.SystemOfUnits as units

CosmicReco = True

from Configurables import MagneticFieldSvc
MagneticFieldSvc().UseConstantField = True
MagneticFieldSvc().UseConditions    = False

from Gaudi.Configuration import *

importOptions('$PANORAMIXROOT/options/tae_ondemand.py')
bunch = ["Prev3","Prev2","Prev1","","Next1","Next2","Next3"]

# Patseeding owns private PatSeedingTool
# which owns (public, made private !) tool TStationHitManager
# which owns as (public,made private !) tools den OTHitCreator und den 
# ITHitCreator.


from TrackSys.Configuration import *
## Set of standard fitting options
importOptions( "$TRACKSYSROOT/options/Fitting.py" )

TrackSys().fieldOff = True
#PatSeeding for beam1 events
from Configurables import (OTRawBankDecoder,Tf__OTHitCreator,Tf__STHitCreator_Tf__IT_,PatTStationHitManager,TrackEventFitter,PatSeeding,PatSeedingTool)
pat_conf = {}
for b in bunch : 
 #importOptions('$PATALGORITHMSROOT/options/PatSeedingTool-Cosmics.opts')
 pat_conf['PatSeeding'+b] = PatSeeding("PatSeeding"+b)
 if b != '' :  
  pat_conf['PatSeeding'+b].OutputTracksName = b+'/Rec/Track/Seed'
 else : 
  pat_conf['PatSeeding'+b].OutputTracksName = 'Rec/Track/Seed'
 pat_conf['PatSeeding'+b].addTool(PatSeedingTool, name='PatSeedingTool')
 # options to tune PatSeeding for tracking with B field off
 pat_conf['PatSeeding'+b].PatSeedingTool.xMagTol = 4e2;
 pat_conf['PatSeeding'+b].PatSeedingTool.zMagnet = 0.;
 pat_conf['PatSeeding'+b].PatSeedingTool.FieldOff = True;
 pat_conf['PatSeeding'+b].PatSeedingTool.MinMomentum = 5e4;
 # relax requirements on number of hits/planes
 # (this may need tweaking if some C frames are in "open" position) 
 pat_conf['PatSeeding'+b].PatSeedingTool.MinXPlanes = 4;
 pat_conf['PatSeeding'+b].PatSeedingTool.MinTotalPlanes = 8;
 pat_conf['PatSeeding'+b].PatSeedingTool.OTNHitsLowThresh = 9;
 pat_conf['PatSeeding'+b].PatSeedingTool.MaxMisses = 2;
 # relax pointing requirement (no PV for cosmics!)
 pat_conf['PatSeeding'+b].PatSeedingTool.MaxYAtOrigin = 4e5;
 pat_conf['PatSeeding'+b].PatSeedingTool.MaxYAtOriginLowQual = 8e5;
 pat_conf['PatSeeding'+b].PatSeedingTool.xMagTol = 4e5;
 pat_conf['PatSeeding'+b].PatSeedingTool.MinMomentum = 1e-4; # pointing requirement in disguise!
 # cosmic reconstruction
 if CosmicReco : 
  pat_conf['PatSeeding'+b].PatSeedingTool.Cosmics = True;
 else : 
  pat_conf['PatSeeding'+b].PatSeedingTool.Cosmics = False;
 # detector not yet aligned - chi^2 should not contribute to track quality
 pat_conf['PatSeeding'+b].PatSeedingTool.QualityWeights        = [ 1.0, 0.0 ]
 pat_conf['PatSeeding'+b].PatSeedingTool.addTool(PatTStationHitManager, name='PatTStationHitManager')
 pat_conf['PatSeeding'+b].PatSeedingTool.PatTStationHitManager.addTool(Tf__OTHitCreator('OTHitCreator'))
 pat_conf['PatSeeding'+b].PatSeedingTool.PatTStationHitManager.OTHitCreator.addTool(OTRawBankDecoder, name='OTRawBankDecoder')
 if b == '': 
  pat_conf['PatSeeding'+b].PatSeedingTool.PatTStationHitManager.OTHitCreator.OTRawBankDecoder.RawEventLocation = 'DAQ/RawEvent'
 else : 
  pat_conf['PatSeeding'+b].PatSeedingTool.PatTStationHitManager.OTHitCreator.OTRawBankDecoder.RawEventLocation = b+'/DAQ/RawEvent'
 pat_conf['PatSeeding'+b].PatSeedingTool.PatTStationHitManager.OTHitCreator.NoDriftTimes = True
 pat_conf['PatSeeding'+b].PatSeedingTool.PatTStationHitManager.addTool(Tf__STHitCreator_Tf__IT_('ITHitCreator'))
 if b != '' :  
  pat_conf['PatSeeding'+b].PatSeedingTool.PatTStationHitManager.ITHitCreator.ClusterLocation=b+'/Raw/IT/LiteClusters'
 else : 
  pat_conf['PatSeeding'+b].PatSeedingTool.PatTStationHitManager.ITHitCreator.ClusterLocation='Raw/IT/LiteClusters'
 pat_conf['PatSeedingFit'+b] = TrackEventFitter('PatSeedingFit'+b)
 if b != '' :  
  pat_conf['PatSeedingFit'+b].TracksInContainer = b+'/Rec/Track/Seed'
 else : 
  pat_conf['PatSeedingFit'+b].TracksInContainer = 'Rec/Track/Seed'
 if b != '' :  
  pat_conf['PatSeedingFit'+b].TracksOutContainer = b+'/Rec/Track/Best'
 else : 
  pat_conf['PatSeedingFit'+b].TracksOutContainer = 'Rec/Track/Best'

 ApplicationMgr().TopAlg += [pat_conf['PatSeeding'+b],pat_conf['PatSeedingFit'+b]]

# add Velo tracking sequence:
from Configurables import (
Tf__PatVeloRTracking, Tf__PatVeloSpaceTool,Tf__PatVeloSpaceTracking, Tf__PatVeloGeneralTracking,
Tf__PatVeloTrackTool,PatForward, Tf__PatVeloRHitManager,Tf__DefaultVeloRHitManager,
Tf__PatVeloPhiHitManager,Tf__PatVeloGeneralTracking,PatForwardTool,PatFwdTool)

from Configurables import Tf__PatVeloGeneric
patVeloGeneric = Tf__PatVeloGeneric()
patVeloGeneric.Output = 'Rec/Track/Velo'
ApplicationMgr().TopAlg += [ patVeloGeneric ]

patVeloRTracking     = Tf__PatVeloRTracking("PatVeloRTracking")
patVeloSpaceTracking = Tf__PatVeloSpaceTracking("PatVeloSpaceTracking")
patVeloSpaceTracking.addTool( Tf__PatVeloSpaceTool, 'VeloSpaceTool' )
patVeloSpaceTracking.SpaceToolName = 'VeloSpaceTool'
patVeloSpaceTracking.VeloSpaceTool.MarkClustersUsed = True;

patVeloSpaceTracking.VeloSpaceTool.TrackToolName     = 'VeloTrackTool'
patVeloSpaceTracking.VeloSpaceTool.RHitManagerName   = 'VeloRHitManager'
patVeloSpaceTracking.VeloSpaceTool.PhiHitManagerName = 'VeloPhiHitManager'

patVeloTrackTool = Tf__PatVeloTrackTool('VeloTrackTool')
patVeloTrackTool.RHitManagerName  =  'VeloRHitManager'
patVeloTrackTool.PhiHitManagerName = 'VeloPhiHitManager'

veloGeneral = Tf__PatVeloGeneralTracking("PatVeloGeneralTracking")
# tools already created before
veloGeneral.TrackToolName     = 'VeloTrackTool'
veloGeneral.RHitManagerName   = 'VeloRHitManager'
veloGeneral.PhiHitManagerName = 'VeloPhiHitManager'
veloGeneral.PointErrorMin = 2*units.mm

# special for halo tracks
##veloGeneral.MaxMissedSensors = 2
##patVeloSpaceTracking.VeloSpaceTool.NMissedFirst = 6    
patVeloRTracking.ZVertexMin     = -1.E20 
patVeloRTracking.ZVertexMax     = +1.E20 

veloreco = GaudiSequencer("veloreco")
veloreco.Members += [  patVeloRTracking, patVeloSpaceTracking, veloGeneral ]
ApplicationMgr().TopAlg += [ veloreco ]

from Configurables import ( PatForward, PatForwardTool, PatFwdTool)
patForward = PatForward("PatForward")
PatFwdTool("PatFwdTool").withoutBField  = True;
PatForwardTool("PatForwardTool").WithoutBField = True;
ApplicationMgr().TopAlg += [ patForward ]

from Configurables import ( PatVeloTT)
importOptions('$PATVELOTTROOT/options/PatVeloTT.py')
patVeloTT = PatVeloTT("PatVeloTT")
patVeloTT.InputUsedTracksNames = []
patVeloTT.removeUsedTracks = False
patVeloTT.PatVeloTTTool.minMomentum = 5000.;
patVeloTT.PatVeloTTTool.maxPseudoChi2 = 256;
patVeloTT.maxChi2 = 256.;
patVeloTT.PatVeloTTTool.DxGroupFactor = 0.0;
patVeloTT.fitTracks = False;
#ApplicationMgr().TopAlg += [veloFitter]
#from TrackFitter.ConfiguredFitters import * 
#veloFitter = ConfiguredFitVelo() 
#veloFitter.TracksInContainer = 'Rec/Track/Velo'
#veloFitter.Fitter.ApplyMaterialCorrections = False 
#veloFitter.Fitter.NodeFitter.BiDirectionalFit = False 
#veloFitter.Fitter.ZPositions += [ 770 ]
ApplicationMgr().TopAlg += [patVeloTT]


