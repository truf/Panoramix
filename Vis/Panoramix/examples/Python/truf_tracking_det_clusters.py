from panoramixmodule import *

def execute():
 ui().echo('Visualize Clusters ' )
  
 session().setParameter('modeling.what','no')
 Style().setColor('white')
 data_collect(da(),'VeloCluster','isR==true')
 data_visualize(da())
 Style().setColor('magenta')
 data_collect(da(),'VeloCluster','isR==false')
 data_visualize(da())
 Style().dontUseVisSvc()
 Style().setColor('orange')
 uiSvc().visualize('/Event/Raw/TT/Clusters')
 Style().setColor('red')
 uiSvc().visualize('/Event/Raw/IT/Clusters')
 Style().setColor('green')
 uiSvc().visualize('/Event/Raw/OT/Times')
 Style().useVisSvc()

 ui().echo(' Velo R: white, Velo Phi purple')
 ui().echo(' TT: orange, IT: red, OT: green')

