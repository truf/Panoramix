from panoramixmodule import *
def tobin(x, count=32):
         """
         Integer to binary
         Count is number of bits
         """
         return "".join(map(lambda y:str((x>>y)&1), range(count-1, -1, -1)))  

filename = 'raw_buffer.txt'
m_magic = 0xCBCB

Enums = getEnumNames('LHCb::RawBank')
BankType = Enums['BankType']

f=open(filename,'w')
rb = evt['DAQ/RawEvent']
evh  = evt['Header']

if evh :
 evh  = evt['Header']
 print '+++ Dump raw buffer of size %d to file: %s for run %d and event %d +++' %(rb.currentSize(),filename,evh.runNum(),evh.evtNum())
 f.write('run # = %d, event # = %d, raw buffer size = %d \n' %(evh.runNum(),evh.evtNum(),rb.currentSize() ) )

if rb :
 f.write('  banktype sourceID word     hex    binary   \n'  )
 i=0
 for k in range(len(BankType)) :
  b = BankType[k]
  nTell1 = rb.banks(k).size()  
  for m in range(nTell1) :
    size =  int((rb.banks(k)[m].size() )/4+0.5)
    for l in range(size) :
      word = rb.banks(k)[m].data()[l]
      t2 = (word>>16) & 0x0000ffff
      t1 =   word     & 0x0000ffff
      if word < 0:
        word = t2*2**16+t1     
      f.write('%15s %6d  %.8x %32s ' %(b+' '+str(rb.banks(k)[m].sourceID()),i, word, tobin(word))  )  
      f.write('\n')
      i+=1
f.close()
print 'dumped raw buffer to file raw_buffer.txt'
url = 'raw_buffer.txt'
webbrowser.open(url,new = 1) 
