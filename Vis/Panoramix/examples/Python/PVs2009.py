import pickle,random,os,PRplot
from panoramixmodule import *
XYZPoint = GaudiPython.gbl.ROOT.Math.XYZPoint   

host  = os.environ['HOST']
local = host.find('pctommy')>-1
if local : 
  prefix = '/media/Work/'
else :
  prefix = '/castor/cern.ch/user/t/truf/2009/'


def myPVview(npv=2,vxchi2=999.,nrtr=5):
 f=file(prefix+'primVx.pkl')
 myPVDict = pickle.load(f)   
 f.close()  
 Region().setTransformScale(50.,50.,1.)
 Camera().setPosition( 0.,2000.,500.)
 Camera().setHeight(4000.)
 Camera().setOrientation(-0.57735, -0.57735, -0.57735, 2.0943)
 Camera().setNearFar(-15000.,15000.)
 uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/Velo/VeloLeft')
 uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/Velo/VeloRight')

 ix = 3
 iy = 4
 iz = 5
 for x in myPVDict:
   # myPVDict[evtnr+'|'+tag] = [pvs.size(),nmax[0],pvs[nmax[1]].chi2PerDoF(),pos.x(),pos.y(),pos.z()]
   run = x.split('|')[0]
   bty = x.split('|')[2]
   if run not in ['63949','63801','63807','63809','63813']: continue
   #if run not in ['63813']: continue
   pos = myPVDict[x]
   if pos[ix]*pos[iy]==0 : continue
# remove events with too many PV
   if pos[0]>npv: continue
# cut on chi2 / dof   
   if pos[2]>vxchi2 : continue
# cut on nr of tracks
   if pos[1]<nrtr : continue
   aPoint = XYZPoint(pos[ix],pos[iy],pos[iz])
   if bty == 'bb' : Style().setColor('green')
   if bty == 'be' : Style().setColor('blue')
   if bty == 'eb' : Style().setColor('red')
   if bty == 'ee' : Style().setColor('white')
   
   if bty == 'bb' and abs(pos[iz])<150 and random.random()>0.1: continue
   # if aPoint.rho()>25 : print x,pos
   sc = uiSvc().visualize(aPoint)  
 PRplot.wprint('PVs2009.jpg',format='JPEG',quality='100',info=False)    

