from panoramixmodule import *

if not ui().findWidget('Viewer_XY') :     
 ui().createComponent('Viewer_XY','PageViewer','ViewerTabStack')
 ui().setCallback('Viewer_XY','collect','DLD','OnX viewer_collect @this@')
 ui().setCallback('Viewer_XY','popup','DLD','Panoramix Panoramix_viewer_popup')
 ui().setParameter('Viewer_XY.popupItems','Current region\nNo highlighted\nTrack_MCParticle\nMCParticle_Track\nParticle_MCParticle\nParticle_Daughters\nMCParticle_Clusters\nCluster_MCParticles')
 ui().setParameter('Viewer_XY.viewing','3D');

ui().setCurrentWidget(ui().findWidget('Viewer_XY')) 
session().setParameter('modeling.color','forestgreen')     
ui().executeScript('DLD','Panoramix layout_velo')

Page().setTitle('XY view')
Page().titleVisible(True)
