# Pascal
import PR_Viewer,PRplot
import CoinPython as Inventor

from panoramixmodule import *

def psprint():
 soPage = ui().findSoPage('Viewer')
 soRegion = soPage.currentRegion()
 soDisplayRegion = soRegion.cast_SoDisplayRegion() 
 staticScene = soDisplayRegion.getStaticScene()
 widget = ui().findWidget('Viewer')
 if widget != None:
  soViewer = widget.cast_ISoViewer()
  vpr = soViewer.viewportRegion()
  osr = Inventor.SoOffscreenRenderer(vpr)
  sz = vpr.getWindowSize()
  print sz[0],sz[1]
  vpr.setWindowSize(4*sz[0],4*sz[1])
  if soViewer != None:
    #sg = soViewer.sceneGraph()
    #osr.render(sg)
    sep = soRegion.getTopSeparator()
    osr.render(sep)
    printsize = Inventor.SbVec2f(8.5,11.0) #A4
#    if osr.writeToPostScript("out.ps",printsize) == 0 :
    if osr.writeToPostScript("forRolf.ps") == 0 :
      print 'osr.writeToPostScript failed'
    else :
      print 'production of ps ok.'
  vpr.setWindowSize(sz[0],sz[1])


def execute():
 PR_Viewer.execute(version='single')
 uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/Rich1')
 uiSvc().visualize('/dd/Structure/LHCb/AfterMagnetRegion/Rich2/HPDPanel0')
 uiSvc().visualize('/dd/Structure/LHCb/AfterMagnetRegion/Rich2/HPDPanel1')
 uiSvc().visualize('/dd/Structure/LHCb/AfterMagnetRegion/Rich2/Rich2Gas')
 session().setParameter('modeling.modeling','wireFrame')
 uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/Velo')
 session().setParameter('modeling.modeling','solid')
 PRplot.TT_view()
 Style().setColor('magenta')        
 uiSvc().visualize('/Event/Raw/TT/Clusters')   
# Camera positionning :
 Camera().setOrientation(0.227089,-0.93626, -0.268044, 4.20833)
 Camera().setPosition(10867.6,8491.27,4360.21)
 Camera().setHeight(9945.54)
 Camera().setNearFar(2822.93, 26796.8)
 ui().synchronize()  
 PRplot.wprint('forPascal.jpg')
 #PRplot.wprint('forPascal.ps','GL2PS')

def forRolf():
# region 2: Velo close up, xy
     container = 'Phys/StripBu2JpsiKDet/Particles'
     print container
     rc = session().setParameter('modeling.trackStartz','-500.')
     PR_Viewer.track_logic(2)  
     Camera().setPosition(0.,0.,35000.)
     Camera().setOrientation(0.,0.,1.,0.)
     Camera().setOrientation(0.,0.,1.,0.)
     Camera().setNearFar(15000.,40000.)
     Camera().setHeight(300.)  
     ui().setCurrentWidget(ui().findWidget('Viewer')) 
     aV0 = evt[container][0]
     tmp = session().parameterValue('modeling.lineWidth')
     session().setParameter('modeling.lineWidth','3.')  
     for sdaughter in aV0.daughters():
      daughter = sdaughter.target()
      if daughter.particleID().pid() == 443 : 
       for jdaughter in daughter.daughters():
        amuon = jdaughter.target() 
        Style().setColor('magenta')
        atrack = amuon.proto().track()
        Object_visualize(atrack)
      else:
        Style().setColor('red')
        atrack = daughter.proto().track()
        Object_visualize(atrack)
        print atrack 
     session().setParameter('modeling.lineWidth',tmp) 
     ui().synchronize()
