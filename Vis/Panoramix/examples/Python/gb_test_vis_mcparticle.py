#
# Example script to visualize per MCParticle (and not full collection).
#

import pmx

evtSvc = pmx.evtSvc()
if evtSvc == None:
  print 'gb_test_vis_mcparticle : EventDataSvc not found.'
else:
  print 'gb_test_vis_mcparticle : EventDataSvc found.'
  import gaudimodule
  mcps = gaudimodule.Helper.dataobject(evtSvc,'/Event/MC/Particles')
  if mcps == None:
    print 'gb_test_vis_mcparticle : /Event/MC/Particles not found.'
  else :
    gaudimodule.loaddict('SoEventDict')
    soCnvSvc = pmx.soCnvSvc()
    if soCnvSvc != None:
      print 'gb_test_vis_mcparticle : %d' % (mcps.size())
      cls = gaudimodule.gbl.LHCb.MCParticle
      style = pmx.Style()
      style.setColor('blue')
      for mcp in mcps.containedObjects():
        if mcp.pt() > 1000 :
          print 'gb_test_vis_mcparticle : energy %g, pt %g' % (mcp.momentum().e(),mcp.pt())
          gaudimodule.gbl.SoEvent.KO(cls).visualize(soCnvSvc,mcp)

