from Panoramix import *
import DetView_module

def detView_1() : 
 Page().setTitle('My LHCb')
 Page().titleVisible(True)
 Page().createDisplayRegions(1,1,0)

# Setup region (a page can have multiple drawing region) :
 Page().setCurrentRegion(0)

 Viewer().setFrame()
 Style().setColor('white')
 Style().setSolid()

# Camera positionning :
# Side +z at left :
 Camera().setPosition(1660., 7250., 9570.)
 Camera().setHeight(16000.)
 Camera().setOrientation(-0.513, 0.688, 0.513, 1.936)

 DetView_module.BeforeMagnetRegion()
 DetView_module.MagnetRegion()
 DetView_module.Tstations() 
 DetView_module.Rich2()
 DetView_module.Calo()
 DetView_module.Muon()

 Style().setSolid()
 Style().useVisSvc()
 Style().setTransparency(1.0)


def detView_2() : 
 Page().setTitle('My LHCb')
 Page().titleVisible(True)
 Page().createDisplayRegions(1,1,0)

 # Setup region (a page can have multiple drawing region) :
 Page().setCurrentRegion(0)

 Viewer().setFrame()
 Style().setColor('white')
 Style().setSolid()
 Style().setTransparency(1.0)

  # Camera positionning :
 Camera().setPosition(-1164.9132, 414.41745, 8328.7764)
 Camera().setHeight(16000.)
 Camera().setOrientation(0.1117591, 0.94922966, 0.29406279,  3.9289277)

 DetView_module.BeforeMagnetRegion()
 DetView_module.MagnetRegion()
 DetView_module.Tstations() 
 DetView_module.Rich2()
 session().setParameter('CaloVisStructure','TRUE')
 session().setParameter('CaloVisPrism','TRUE')
 DetView_module.Calo()
 DetView_module.Muon()

 Style().setClosed()
 Style().setSolid()
 Style().setTransparency(1.0)


