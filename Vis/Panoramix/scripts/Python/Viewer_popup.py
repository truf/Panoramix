from Panoramix import *

# The selected popup option
item = ui().callbackValue();
ok = False
# special support for rz view
if ui().currentWidget().name() == 'Viewer_2d' : 
 session().setParameter('modeling.projection','-ZR')


def clusters2mcparticles() :
    session().setParameter('modeling.what','MCParticle')
    data_collect(da(),'SceneGraph(@current@)','highlight==true')
    data_filter(da(),'name','FTCluster*')
    data_visualize(da())
    session().setParameter('modeling.what','MCParticle')
    data_collect(da(),'SceneGraph(@current@)','highlight==true')
    data_filter(da(),'name','STCluster*')
    data_visualize(da())
    data_collect(da(),'SceneGraph(@current@)','highlight==true')
    data_filter(da(),'name','VeloCluster*')
    data_visualize(da())
    data_collect(da(),'SceneGraph(@current@)','highlight==true')
    data_filter(da(),'name','VLCluster*')
    data_visualize(da())
    data_collect(da(),'SceneGraph(@current@)','highlight==true')
    data_filter(da(),'name','VPCluster*')
    data_visualize(da())
    data_collect(da(),'SceneGraph(@current@)','highlight==true')
    data_filter(da(),'name','OTTime*')
    data_visualize(da())
    data_collect(da(),'SceneGraph(@current@)','highlight==true')
    data_filter(da(),'name','MuonCoord*')
    data_visualize(da())
    #data_collect(da(),'SceneGraph(@current@)','highlight==true')
    #data_filter(da(),'name','CaloCluster*')
    #data_visualize(da())

def tracks2clusters() :
    data_collect(da(),'SceneGraph(@current@)','highlight==true')
    data_filter(da(),'name','Track*')
    session().setParameter('modeling.what','clusters')
    data_visualize(da())


if item == 'MCParticle_Track':
    ok = True
    ui().echo('Viewing Tracks associated to selected MCParticles')
    data_collect(da(),'SceneGraph(@current@)','highlight==true')
    data_filter(da(),'name','MCParticle*')
    session().setParameter('modeling.what','Track')
    data_visualize(da())

if item == 'Track_MCParticle':
    ok = True
    ui().echo('Viewing MCParticles associated to selected Tracks')
    data_collect(da(),'SceneGraph(@current@)','highlight==true')
    data_filter(da(),'name','Track*')
    session().setParameter('modeling.what','MCParticle')
    data_visualize(da())
    
if item == 'Track_Cluster_MCParticle':
    ok = True
    ui().echo('Viewing all MCParticles associated to clusters of selected Tracks')
    tracks2clusters() 
## assume that only clusters are displayed which come from the previous association    
    session().setParameter('modeling.what','MCParticle')
    data_collect(da(),'SceneGraph(@current@)','')
    data_filter(da(),'name','FTCluster*')
    data_visualize(da())
    session().setParameter('modeling.what','MCParticle')
    data_collect(da(),'SceneGraph(@current@)','')
    data_filter(da(),'name','STCluster*')
    data_visualize(da())
    data_collect(da(),'SceneGraph(@current@)','')
    data_filter(da(),'name','VeloCluster*')
    data_visualize(da())
    data_collect(da(),'SceneGraph(@current@)','')
    data_filter(da(),'name','VLCluster*')
    data_visualize(da())
    data_collect(da(),'SceneGraph(@current@)','')
    data_filter(da(),'name','VPCluster*')
    data_visualize(da())
    data_collect(da(),'SceneGraph(@current@)','')
    data_filter(da(),'name','OTTime*')
    data_visualize(da())
    data_collect(da(),'SceneGraph(@current@)','')
    data_filter(da(),'name','MuonCoord*')
    data_visualize(da())
    #data_collect(da(),'SceneGraph(@current@)','')
    #data_filter(da(),'name','CaloCluster*')
    #data_visualize(da())

if item == 'Particle_MCParticle':
    ok = True
    ui().echo('Viewing MCParticles associated to selected Particles')
    data_collect(da(),'SceneGraph(@current@)','highlight==true')
    data_filter(da(),'name','Particle*')
    session().setParameter('modeling.what','MCParticle')
    data_visualize(da())

if item == 'Particle_Track':
    ok = True
    ui().echo('Viewing Tracks associated to selected Particles')
    data_collect(da(),'SceneGraph(@current@)','highlight==true')
    data_filter(da(),'name','Particle*')
    session().setParameter('modeling.what','Track')
    data_visualize(da())

if item == 'Particle_Daughters':
    ok = True
    ui().echo('Viewing daughter Particles associated to selected Particles')
    data_collect(da(),'SceneGraph(@current@)','highlight==true')
    data_filter(da(),'name','Particle*')
    session().setParameter('modeling.what','Particle')
    data_visualize(da())

if item == 'MCParticle_Clusters':
    ok = True
    ui().echo('Viewing clusters associated to selected MCParticles')
    data_collect(da(),'SceneGraph(@current@)','highlight==true')
    data_filter(da(),'name','MCParticle*')
    session().setParameter('modeling.what','Clusters')
    data_visualize(da())

if item == 'Cluster_MCParticles':
    ok = True
    ui().echo('Viewing MCParticles associated to selected Clusters')
    clusters2mcparticles() 

if item == 'MCParticle_MCHits':
    ok = True
    ui().echo('Viewing MCHits associated to selected MCParticles')
    data_collect(da(),'SceneGraph(@current@)','highlight==true')
    data_filter(da(),'name','MCParticle*')
    session().setParameter('modeling.what','MCHits')
    data_visualize(da())

if item == 'Track_Measurements':
    ok = True
    ui().echo('Viewing Measurements associated to selected Tracks')
    data_collect(da(),'SceneGraph(@current@)','highlight==true')
    data_filter(da(),'name','Track*')
    session().setParameter('modeling.what','Measurements')
    data_visualize(da())

if item == 'Track_Clusters':
    ok = True
    ui().echo('Viewing Clusters associated to selected Tracks')
    tracks2clusters() 

if item == 'ChargeConjugate_Track':
    ok = True
    ui().echo('Plot trajectory of charge conjugate track')
    data_collect(da(),'SceneGraph(@current@)','highlight==true')
    data_filter(da(),'name','Track*')
    session().setParameter('modeling.what','ChargeConjugate')
    data_visualize(da())

if item == 'ReFit_Tracks':
    ok = True
    ui().echo('ReFitting selected Tracks')
    data_collect(da(),'SceneGraph(@current@)','highlight==true')
    data_filter(da(),'name','Track*')
    session().setParameter('modeling.what','ReFit')
    data_visualize(da())
    
if ok == False:
    ui().echo('Unknown popup option '+item)
    ui().executeScript('DLD','OnX viewer_popup')
    
if ui().currentWidget().name() == 'Viewer_2d' : 
 session().setParameter('modeling.projection','')
    
