from panoramixmodule import *

pixel_color = 'orange'

def Visualize_Rich_RecoPixels(curRegion=False):
    
    #ui().echo('Visualising all reconstructed RICH pixels')

    Style().dontUseVisSvc()

    # Save current color
    save_color = session().parameterValue('modeling.color')
    Style().setColor(pixel_color)
    
    if curRegion :     uiSvc().visualize('/Event/Rec/Rich/RecoEvent/Offline/Pixels')
    else : # Draw in all regions
     for region in range(Page().fPage.getNumberOfRegions()) :
        
        # Move to each region in turn
        Page().setCurrentRegion(region)
        # draw
        uiSvc().visualize('/Event/Rec/Rich/RecoEvent/Offline/Pixels')
    
    # reset back
    Style().useVisSvc()
    Style().setColor(save_color)
    
def Visualize_Selected_Rich_RecoPixels(da):
    
    #ui().echo('Visualising selected reconstructed RICH pixels')

    # Save current color
    save_color = session().parameterValue('modeling.color')
    Style().setColor(pixel_color)

    # draw things
    data_visualize(da)

    # reset back
    Style().setColor(save_color)
