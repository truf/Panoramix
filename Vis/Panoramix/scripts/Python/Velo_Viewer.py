from panoramixmodule import *
from PRplot import Velo_clusters as Velo_clusters
from PRplot import disp_velotracks as disp_velotracks
from PRplot import BeamPipe as BeamPipe
from PRplot import Velo_viewL as Velo_viewL
from PRplot import Velo_viewR as Velo_viewR
import PR_Viewer
PR_Viewer.defaults()

def createPage(title,nregions): 
 page = ui().currentPage()
 page.deleteRegions()
 Page().setTitle(title)
 Page().titleVisible(True)
 Page().createDisplayRegions(1,1,0)
 ui().currentWidget().cast_ISoViewer().setHeadlight(1)
 rc = session().setParameter('modeling.userTrackRange','true')
 rc = session().setParameter('modeling.trackEndz','1000.')
 rc = session().setParameter('modeling.trackStartz','-1000.')
def plotTracks():
  session().setParameter('modeling.showCurve','true')
  session().setParameter('modeling.useExtrapolator','true')
#  tracklocation = 'Rec/Track/Best'
#  session().setParameter('Track.location',tracklocation)
    
# velo veloBack veloTT unique  seed match forward isDownstream  
  Style().setLineWidth(1.5) 
  Style().setColor('green')
  data_collect(da(),'Track','velo==true&&unique==true')
  data_visualize(da())
  Style().setColor('greenyellow')
  data_collect(da(),'Track','upstream==true&&unique==true')
  data_visualize(da())
  Style().setColor('blue')
  data_collect(da(),'Track','long==true&&unique==true')
  data_visualize(da())
  Style().setColor('skyblue')
  data_collect(da(),'Track','downstream==true&&unique==true')
  data_visualize(da())
                                     
def execute(what,action):
 curtitle = ui().currentPage().title.getValues()[0]
 if what == 'rz' :
# setup rz projection
  title = 'VELO RZ'
  if curtitle != title or action == 'recreate':  
   createPage(title,1)
   onlineViews[title] = 'Velo_RZView'
   Page().setCurrentRegion(0)  
# Camera positioning for -ZR projection
   Camera().setPosition(300., 0., 100.)
   Camera().setHeight(1000.)
   Camera().setOrientation(0., 1., 0., 0.)
   Camera().setNearFar(1.,1000.)
   Region().setTransformScale(1.,8.4,1.)
  Viewer().removeAutoClipping()
  if action ==  'visualize' and evt['DAQ/ODIN'] : 
   ui().echo(' Visualize Velo clusters')
 # clear regions before showing new event
   Page().setCurrentRegion(0)      
   Page().currentRegion().clear("dynamicScene")
   session().setParameter('modeling.projection','-ZR')
   session().setParameter('modeling.what','no')  
   
   Style().setColor('violet')
   data_collect(da(),'VeloCluster','isR==false')
   data_visualize(da())

   Style().setColor('white')
   data_collect(da(),'VeloCluster','isR==true')
   data_visualize(da())
   
   if evt['Rec/Track'] : PR_Viewer.VeloRZ()
   
   session().setParameter('modeling.projection','')
# setup 3d view
 if what == '3d' :
  title = 'VELO 3D'
  if curtitle != title or action == 'recreate':  
   createPage(title,1)
   Velo_viewL()
   Camera().setPosition(-74.5703, 449.579, 556.028)                       
   Camera().setHeight(939.035)                                            
   Camera().setOrientation(0.611425, 0.558558, 0.56051, 4.95116)          
   Camera().setNearFar(-6655.25,3041.07)                                  
   # BeamPipe()    
   onlineViews[title] = 'Velo_3dView'
  if action ==  'visualize' and evt['DAQ/ODIN']  : 
     Velo_clusters('dark')
# only associated clusters 
     if evt['Rec/Track/Velo']  : 
      disp_velotracks('')
      Style().setColor('green')    
      session().setParameter("Track.location",'Rec/Track/Velo')
      session().setParameter("modeling.what","clusters")
      data_collect(da(),'Track','')
      data_visualize(da())
      session().setParameter("modeling.what","no")
 if evt['DAQ/ODIN'] and ui().parameterValue('Panoramix_PRViewer_input_addinfo.value') == 'True' :overlay_runeventnr()
