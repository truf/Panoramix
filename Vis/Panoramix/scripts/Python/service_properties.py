from panoramixmodule import *
def print_sproperties(x):
    try:    properties = gaudi.service(x).properties()
    except: properties = []
    print(10*'-')
    print('Properties of %s' %x)
    print(20*'-')
    print('')
    for key in properties :
            if type(properties[key].value()) != type('') :
                try: 
                  first = True
                  for x in properties[key].value() : 
                      if first: 
                        first = False
                        print('%-35s : %s ' % ( key, x ) )
                      else :   print('%-35s , %s ' % (' ', x ) )
                except:   
                  print('%-35s : %s ' % ( key, properties[key].value() ) )
            else :
                  print('%-35s : %s ' % ( key, properties[key].value() ) )
gaudi = gaudimodule.AppMgr()
a = ui().callbackValue()
print_sproperties(a)

