# try display of tracks with Turbo
from panoramixmodule import *
import PRplot
fitterTool = getTool('TrackMasterFitter/HltFitter','ITrackFitter')

gbl = GaudiPython.gbl
pair = gbl.std.pair('int', 'unsigned int')
vector = gbl.std.vector(pair)

dialog = 'SoEvent_Track_dialog'

def zoom(x):
 page   = OnX.session().ui().currentPage()
 for n in range(page.getNumberOfRegions() ):
      region = page.getRegion(n)
      typ = region.getTypeId()
      if typ.getName() == 'SoDisplayRegion': break
 tsf = region.cast_SoDisplayRegion().getTransform()
 tsf.scaleFactor.setValue(iv.SbVec3f(x,x,1.))  

def execute():
 selection = ui().parameterValue('mainTree.selection')
 temp = selection.split('\n')
 selection  = temp[len(temp)-1]
#
 velo = evt['/Event/Raw/Velo/Clusters']
 tt = evt['/Event/Raw/TT/Clusters']
 it = evt['/Event/Raw/IT/Clusters']
 ot = evt['/Event/Raw/OT/Times']
 velol = [evt['/Event/Raw/Velo/LiteClusters'],{}]
 ttl   = [evt['/Event/Raw/TT/LiteClusters'],{}]
 itl   = [evt['/Event/Raw/IT/LiteClusters'],{}]
#
# how to find the right configuration for the database, geometry, magnetic field, ...
#
 if ot.size()>0: ot.clear()
 for t in evt['Turbo/'+selection+'/Tracks']:
  for l in t.lhcbIDs():
   if l.isVelo():       
      chid = l.veloID()
      vlcl  = gbl.LHCb.VeloLiteCluster(chid,0.,1,False)
      theKey = vlcl.channelID().channelID()
      if velo[theKey]: continue
      velol[1][ theKey ] = vlcl
      strip = vlcl.channelID().strip()
      npair = pair(strip,99)
      adcValues = vector()
      adcValues.push_back(npair)
      vcl       = gbl.LHCb.VeloCluster(vlcl,adcValues)
      k = vcl.key()
      k.setChannelID(theKey)
      #print "debug 0",theKey,k.channelID(),vcl.key().channelID()
      rc = velo.add(vcl)
      #print "debug 1",theKey,k.channelID(),vcl.key().channelID()
      k.setChannelID(theKey)
      velo.update() 
      s=velo.size()
      #print "debug 2",theKey,k.channelID(),velo.containedObjects().at(s-1).key().channelID()
      #print velo.containedObject(theKey) 
   if l.isST(): 
      if l.isIT(): contl = itl
      else:        contl = ttl
      if l.isIT(): cont = it
      else:        cont = tt
      chid = l.stID()
      vlcl  = gbl.LHCb.STLiteCluster(chid,0.,1,False)
      theKey = vlcl.channelID().channelID()
      if cont[theKey]: continue
      contl[1][ theKey ] = vlcl
      strip = vlcl.channelID().strip()
      npair = pair(strip,99)
      adcValues = vector()
      adcValues.push_back(npair)
      vcl       = gbl.LHCb.STCluster(vlcl,adcValues,0.,0,0,gbl.LHCb.STCluster.Spill(0))
      k = vcl.key()
      k.setChannelID(theKey)  
      cont.add(vcl)
      k.setChannelID(theKey)
      cont.update()        
   if l.isOT(): 
      chid = l.otID()
      vlcl  = gbl.LHCb.OTTime(chid,0.)
      rc = ot.add(vlcl)  
# now make sorting and fill fastcontainers:
  for cont in [velol,ttl,itl]:  
          cont[0].clear()
          keys = cont[1].keys()
          keys.sort()
          for k in keys:
            rc = cont[0].push_back(cont[1][k])

  session().setParameter('modeling.what','no')
  Style().setColor('green')    
  session().setParameter("Track.location",'Turbo/'+selection+'/Tracks')
  data_collect(da(),'Track','')
  data_visualize(da())
  Style().setColor('blue')    
  data_collect(da(),'OTTime','')
  data_visualize(da())
  Style().setColor('red')    
  uiSvc().visualize('/Event/Raw/TT/LiteClusters')
  uiSvc().visualize('/Event/Raw/IT/LiteClusters')
  Style().setColor('violet')
  data_collect(da(),'VeloCluster','isR==false')
  data_visualize(da())
  Style().setColor('white')
  data_collect(da(),'VeloCluster','isR==true')
  data_visualize(da())

 for t in evt['Turbo/'+selection+'/Tracks']:
  print "momentum before fit",t.key(),t.momentum()
  Style().setColor('gold')
  Object_visualize(t)
  #print t
  rc = fitterTool.fit(t)
  print "momentum after fit",t.key(),t.momentum()
  #print t
 if not ui().findWidget(dialog) : PRplot.defaults()
 ui().setParameter('SoEvent_Track_input_associations.value','Measurements')  
 PRplot.disp_tracks(0,container='Turbo/'+selection+'/Tracks')

def test():
 #python $myPanoramix -f /eos/lhcb/lbdt3/user/truf/TURBO/Down/00047765_00006579_1.LamcTurbo.mdst
 tt  = det['/dd/Structure/LHCb/BeforeMagnetRegion/TT']
 pos = getTool('STOfflinePosition/ITClusterPosition','ISTClusterPosition')
 cl = evt['Raw/TT/Clusters'].containedObjects().at(0)
 meas = GaudiPython.gbl.LHCb.STMeasurement( cl, tt, pos)
 velopotool = appMgr.toolsvc().create('VeloClusterPosition',interface='IVeloClusterPosition')
 for l in t.lhcbIDs():
      if l.isVelo(): 
          chid = l.veloID()
          print l.channelID(),chid.channelID(),evt['/Event/Raw/Velo/LiteClusters'].object(chid)
 for x in evt['/Event/Raw/Velo/LiteClusters']:
   print x,  evt['/Event/Raw/Velo/LiteClusters'].object(x.channelID())
