from Onlinemodule import *
import panoramixmodule
XYZPoint = LHCbMath.XYZPoint

def createPage(title,nregions): 
 page = ui().currentPage()
 page.deleteRegions()
 Page().setTitle(title)
 Page().titleVisible(True)
 if nregions == 5 : Page().createDisplayRegions(2,3,0)
 if nregions == 1 : Page().createDisplayRegions(1,1,0)
 ui().currentWidget().cast_ISoViewer().setHeadlight(1)
 Style().setWireFrame()
 if nregions == 5 : 
    for i in range(0,5) :
     Page().setCurrentRegion(i)
     if geometry != 'contour' : 
       uiSvc().visualize('/dd/Structure/LHCb/DownstreamRegion/Muon/M'+str(i+1))
    Page().setCurrentRegion(5)
    Page().currentRegion().color.setValue(1.,1.,1.)
 Style().setSolid()  
                                     
def execute(what):
 curtitle = ui().currentPage().title.getValues()[0]
 if what == 'xy' :
# setup xy projections for all 4 calorimeters on one page
  title = 'Muon XY view'
  if curtitle != title or action == 'recreate':  
# setup xy projections for all 5 muon stations on one page
   createPage(title,5)
   onlineViews[title] = 'Muon_XYView'
  for i in range(Page().fPage.getNumberOfRegions()) :
   Page().setCurrentRegion(i)  
# Camera positioning :
   Camera().setPosition(0., 0., 20000.)
   Camera().setHeight(11000.)
   Camera().setOrientation(0.,0.,1.,0.)
   Camera().setNearFar(1000.,20000.)
 if what == '3d' or action ==  'recreate':
# setup 3d view for all 5 muon stations on one page
  title = 'Muon 3d view'
  if curtitle != title or action == 'recreate':  
   createPage(title,1)
   onlineViews[title] = 'Muon_3DView'
  for i in range(Page().fPage.getNumberOfRegions()) :
   Page().setCurrentRegion(i)  
# Camera positioning :
   Camera().setPosition(-10000., 0., 10000.)
   Camera().setHeight(19000.)
   Camera().setOrientation( 0, -1, 0, 1.57)
   Camera().setNearFar(8000.,12000.)

 if action ==  'visualize' and evt['DAQ/ODIN']  :  
  ui().echo(' Visualize Muon Coords')
 # clear regions before showing new event
  for i in range(Page().fPage.getNumberOfRegions()) : 
   Page().setCurrentRegion(i)      
   Page().currentRegion().clear("dynamicScene")
 
  Style().dontUseVisSvc()
  for i in range(5):    
   if what == 'xy' : 
    Page().setCurrentRegion(i)
    if geometry == 'contour' :  muonStationContours(i)
   g = 1.-0.5*i/6.
   b = 0.5*i/6. 
   Style().setRGB(0.,g,b)
   data_collect(da(),'MuonCoord','station=='+str(i))
   data_visualize(da())
  
  Page().setCurrentRegion(5)
  Style().useVisSvc()
  panoramixmodule.overlay_runeventnr()

# Dialog inputs : 
projection  = ui().parameterValue('Panoramix_Muon_input_projection.value')
action      = ui().parameterValue('Panoramix_Muon_input_action.value')
geometry    = ui().parameterValue('Panoramix_Muon_input_geometry.value')

if action == 'NextEvent' :
  appMgr.run(1)
  action = 'visualize' 

execute(projection) 


