#
# Original code from Pere.
#
# From the Utilities.py of previous Panoramix version,
# the dumpMCParticle() had been revisited to work
# on the event model of LHCb_v20r0. G.Barrand. 6/April/2006.
#
# The way to program Inventor from Python in plotPoints()
# is today awfull. At least it works. G.Barrand. 6/April/2006.
#

import pmx
import math

pdt = pmx.partSvc()

#-------------------------------------------------------------------------
def particleName(p) :
#-------------------------------------------------------------------------
  return pdt.findByStdHepID(p.particleID().pid()).particle()

#-------------------------------------------------------------------------
def dumpMCParticle(k) :
#-------------------------------------------------------------------------
  parts = pmx.data('/Event/MC/Particles')
  if not parts :
    print '/Event/MC/Particles not found'
    return
  else:
    print '%d /Event/MC/Particles' % (parts.numberOfObjects())  

  p = parts[k]

  p_id_name = particleName(p)
  print 'Dump of particle %d %s E = %f' % (k, p_id_name, p.momentum().e())
  ov = p.originVertex()
  point = ov.position()
  print '    Origin: (%6f, %6f, %6f) ' % (point.x(),point.y(),point.z())
  if ov.mother() :
    ovm_id_name = particleName(ov.mother())
    print '        Particle %d %s' % (ov.mother().key(), ovm_id_name)   

  evs = p.endVertices()
  for v in evs :
    point = v.target().position()
    print '    End:   (%6f, %6f, %6f) ' % (point.x(),point.y(),point.z())
    for d in v.target().products() : 
      dt = d.target()
      d_id_name = particleName(dt)
      print '        Product %d %s E = %f'% \
            (dt.key(),d_id_name,dt.momentum().e()) 

  e_ihits = pmx.data('/Event/MC/IT/Hits')
  if not e_ihits :
    print '/Event/MC/IT/Hits not found'
    return
  else:
    print '%d /Event/MC/IT/Hits' % (e_ihits.numberOfObjects())  
  ihits = []  # transfomr e_ihits as a list.
  for i in range(0,e_ihits.numberOfObjects()):ihits.append(e_ihits[i])

  e_ohits = pmx.data('/Event/MC/OT/Hits')
  if not e_ohits :
    print '/Event/MC/OT/Hits not found'
    return
  else:
    print '%d /Event/MC/OT/Hits' % (e_ohits.numberOfObjects())  
  ohits = []  # transfomr e_ohits as a list.
  for i in range(0,e_ohits.numberOfObjects()):ohits.append(e_ohits[i])

  allhits = ihits + ohits
  allhits = filter( (lambda h : h.mcParticle() == p) ,allhits)
  printHits(allhits)
  sorthits = []
  point = ov.position()
  nhits  = len(allhits)
  for i in range(nhits) :
    # locate closest hit
    d2 = map( lambda h : dist2(point,h), allhits )
    h = allhits[d2.index(min(d2))]
    allhits.remove(h)
    sorthits.append(h)
    point = h.exit()
  print 'List of hits after sorting: '
  printHits(sorthits)
  points = [(ov.position().x(), ov.position().y(), ov.position().z())]
  for h in sorthits :
    points.append((h.entry().x(), h.entry().y(),h.entry().z()))
  if evs : 
    ev = evs[0].target()
    points.append((ev.position().x(), ev.position().y(), ev.position().z()))
  plotPoints(points)

#-------------------------------------------------------------------------
def hitcosang(h1,h2) :
#-------------------------------------------------------------------------
  v1 = (h1.exit().x()-h1.entry().x(), \
        h1.exit().y()-h1.entry().y(), \
        h1.exit().z()-h1.entry().z() )
  v2 = (h2.entry().x()-h1.exit().x(), \
        h2.entry().y()-h1.exit().y(), \
        h2.entry().z()-h1.exit().z() )
  norm = math.sqrt(v1[0]*v1[0]+v1[1]*v1[1]+v1[2]*v1[2])*\
         math.sqrt(v2[0]*v2[0]+v2[1]*v2[1]+v2[2]*v2[2])
  if norm == 0:
    print 'hitcosang : div / 0. Continue anyway...'
    return 0
  prod = (v1[0]*v2[0] + v1[1]*v2[1] + v1[2]*v2[2])/norm
  return prod
    
#-------------------------------------------------------------------------
def dist2( p, h ) :
#-------------------------------------------------------------------------
  v = (h.entry().x()-p.x(), h.entry().y()-p.y(), h.entry().z()-p.z() )
  return (v[0]*v[0] + v[1]*v[1] + v[2]*v[2])
    
#-------------------------------------------------------------------------
def printHits(hits) :
#-------------------------------------------------------------------------
  if not hits : return
  if len(hits) == 0 :return
  lh = hits[0]
  for h in hits :
    entry = h.entry()
    exit  = h.exit()
    if exit.z() > entry.z() : sym = '--->'
    else : sym = '<---'
    print '(%f, %f, %f) %s (%f, %f, %f) cos = %f' % \
             ( entry.x(), entry.y(), entry.z(), sym ,  \
               exit.x(), exit.y(), exit.z(), hitcosang(lh, h) )
    lh = h

#-------------------------------------------------------------------------
def dumpVertices():
#-------------------------------------------------------------------------
  verts = pmx.data('/Event/MC/Vertices')
  for v in verts :
    point = v.position()
    if v.mother() :
      print v.key(), '(%f, %f, %f)' % \
            ( point.x(), point.y(), point.z() ), v.mother().key()
    else :
      print v.key(), '(%f, %f, %f)' % ( point.x(), point.y(), point.z() )
            
#-------------------------------------------------------------------------
def testPlotPoints() :
#-------------------------------------------------------------------------
  # G.Barrand : to test the drawing.
  points = [(0,0,0)]
  points.append((100,0,0))
  points.append((100,100,0))
  points.append((0,100,0))
  points.append((0,0,0))
  plotPoints(points)

#-------------------------------------------------------------------------
def dumpAll():
#-------------------------------------------------------------------------
  # G.Barrand : to shake dumpMCParticle().
  parts = pmx.data('/Event/MC/Particles')
  if not parts :
    print '/Event/MC/Particles not found'
    return
  else:
    print '%d /Event/MC/Particles' % (parts.numberOfObjects())  
  for i in range(0,parts.numberOfObjects()):dumpMCParticle(i)

#-------------------------------------------------------------------------
def plotPoints( points, aR = 1, aG = 0.5, aB = 0):
#-------------------------------------------------------------------------
  #FIXME : the below way to program Inventor from Python is awfull.
  #FIXME : But it is the only reliable way by using the LCG-Python
  #FIXME : wrapping for the moment.
  region = pmx.uiSvc().currentSoRegion()
  if not region : return
  dscene = region.getDynamicScene()
  if not dscene : return

  separator = pmx.iv_create("SoSeparator")
  separator.setName(pmx.iv_SbName("sceneGraph"));

  soMaterial = pmx.iv_create("SoMaterial")
  pmx.iv_set_color(soMaterial,"diffuseColor",aR,aG,aB)
  separator.addChild(soMaterial)

  soStyle = pmx.iv_create("SoDrawStyle")
  pmx.iv_set_float(soStyle,"lineWidth",2)
  separator.addChild(soStyle)

  soCoordinates = pmx.iv_create("SoCoordinate3")
  #for p in points : print p
  scoor = map(lambda p : '%f %f %f'%(p[0],p[1],p[2]),points)
  soCoordinates.set( 'point [' + ','.join(scoor) + ' ] 1')
  separator.addChild(soCoordinates)

  soLineSet = pmx.iv_create("SoLineSet")
  separator.addChild(soLineSet)

  dscene.addChild(separator)

#FIXME : the below would need a good LCG-Python wrapping of Inventor.
#-------------------------------------------------------------------------
#def plotPoints( points, aColor = '1 -.5 0'):
#-------------------------------------------------------------------------
#    from OnX_wrap import SoSeparator, SoMaterial
#    from OnX_wrap import SoDrawStyle, SoCoordinate3, SoLineSet
#    import OnX       
#    separator = SoSeparator()
#    separator.setName('points')
#    material = SoMaterial()
#    material.set('diffuseColor %s' % aColor)
#    separator.addChild(material)
#    style = SoDrawStyle()
#    style.set('lineWidth 2')
#    separator.addChild(style)
#    coordinates = SoCoordinate3()
#    for p in points : print p
#    scoor = map(lambda p : '%f %f %f'%(p[0],p[1],p[2]),points)
#    coordinates.set( 'point [' + ','.join(scoor) + ' ] 1')
#    lineset = SoLineSet()
#    separator.addChild(coordinates)
#    separator.addChild(lineset)
#    OnX.region_dynamicNode().addChild(separator)
