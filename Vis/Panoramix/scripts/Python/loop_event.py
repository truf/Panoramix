from panoramixmodule import *
import AfterMagnetIT_Viewer
from PanoramixSys.Configuration import *
import PR_Viewer, next_event
import time,os
ld = {}
dialog = 'Panoramix_Global_dialog'
ltbox  = os.environ['PANORAMIXROOT']+'/data/letterbox.txt'

if not ui().findWidget(dialog) :  ui().showDialog( '$PANORAMIXROOT/scripts/OnX/Online_global.onx' ,dialog )
wi = ui().findWidget(dialog)
if wi : 
    wi.hide()
ui().synchronize()

if wi:
 sleeptime    = int(ui().parameterValue('Panoramix_Global_input_sleeptime.value'))
 maxnev       = int(ui().parameterValue('Panoramix_Global_input_maxnev.value'))
else :
 sleeptime = 5
 maxnev = -1

curtitle = ui().currentPage().title.getValues()[0]

def executeLoop():
 found = False
 for t in onlineViews :
  if curtitle == t : 
   found = True
   break 

 if not found: 
  print "Loop.py: Don\'t know what to do with this view, stop."
 else: 
  session().setParameter('looping','run')
  n = maxnev
  while n != 0 : 
   n-=1
   uiSvc().nextEvent()
   if PanoramixSys().getProp('Sim') != True : print evt['DAQ/ODIN']
   curtitle = ui().currentPage().title.getValues()[0]

   EraseEventAllRegions()
   for t in onlineViews :
    if curtitle == t :
     cmd = onlineViews[t]
     if cmd.find('(') > 0 : 
      m = cmd[:cmd.find('.')]
      ld[m] = sys.modules[m]
      eval(cmd,ld)   
     else : 
      x(cmd)
     break 
   ui().synchronize()
   session().flush()   
  
   nsteps = int(sleeptime)
   for i in range(nsteps) : 
    ui().synchronize()
    if session().parameterValue('looping') == 'stop' : 
     print 'Event loop stopped'
     n = 0
     break
    time.sleep(sleeptime)

def automaticPicture():
 i = 1
 while i>0:
  print 'automaticPicture'
  session().setParameter('looping','run')
  ltbx  = open(ltbox)
  aline = ltbx.readline()
# remove ctrl characters
  aline = aline.replace('\n','')
  aline = aline.replace('\r','')
  aline = aline.replace('\t','')
  ltbx.close()  
  tmp = aline.split(' ')[0]
  if tmp.find('/')<0: tmp = os.environ['PANORAMIXROOT']+'/data/'+tmp
  sel.open(tmp)
  next_event.execute()
  curtitle = ui().currentPage().title.getValues()[0]
  if curtitle != 'LHCb Event Display': PR_Viewer.execute(version='single')
  
  oldmtime =  os.path.getmtime(ltbox)
  while 5>1:
    time.sleep(sleeptime)
    ui().synchronize()
    if session().parameterValue('looping') == 'stop' : 
     print 'Event loop stopped'
     i = -1
     break
    if os.path.getmtime(ltbox) != oldmtime: 
    # wait another few seconds
     time.sleep(sleeptime)
     break
