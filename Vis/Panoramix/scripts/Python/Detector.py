from panoramixmodule import *
def vis_vacTank():
 if det['/dd/Structure/LHCb/BeforeMagnetRegion/Velo']: uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/Velo')
 if det['/dd/Structure/LHCb/BeforeMagnetRegion/VP']:   uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/VP')
 if det['/dd/Structure/LHCb/BeforeMagnetRegion/VL']:   uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/VL')

def vis_Velo():
    if det['/dd/Structure/LHCb/BeforeMagnetRegion/Velo']:       
      uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/Velo/VeloLeft')
      uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/Velo/VeloRight')
    if det['/dd/Structure/LHCb/BeforeMagnetRegion/VP']:   
      uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/VP/VPLeft')
      uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/VP/VPRight')

def vis_TTorUT():
      if det['/dd/Structure/LHCb/BeforeMagnetRegion/TT']:uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/TT')
      if det['/dd/Structure/LHCb/BeforeMagnetRegion/UT']:uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/UT')
def vis_OTorFT():
      if det['/dd/Structure/LHCb/AfterMagnetRegion/T/OT']:uiSvc().visualize('/dd/Structure/LHCb/AfterMagnetRegion/T/OT')
      if det['/dd/Structure/LHCb/AfterMagnetRegion/T/FT']:uiSvc().visualize('/dd/Structure/LHCb/AfterMagnetRegion/T/FT')

def vis_beamPipe(flag=''):
      uiSvc().visualize('/dd/Structure/LHCb/AfterMagnetRegion/PipeAfterT')
      uiSvc().visualize('/dd/Structure/LHCb/AfterMagnetRegion/PipeSupportsAfterMagnet')
      # uiSvc().visualize('/dd/Structure/LHCb/AfterMagnetRegion/Rich2/Rich2BeamPipe')
      uiSvc().visualize('/dd/Structure/LHCb/AfterMagnetRegion/T/PipeInT')
      if det['/dd/Structure/LHCb/BeforeMagnetRegion/TT']:uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/TT/PipeInTT')
      if det['/dd/Structure/LHCb/BeforeMagnetRegion/UT']:uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/UT/PipeInUT')
      uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/PipeJunctionBeforeVelo')
      uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/BeforeVelo/PipeBeforeVelo')
      uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/Rich1/Rich1BeamPipe')
      uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/Rich1/PipeInRich1SubMaster')
      uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/Rich1/PipeInRich1BeforeSubM')
      uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/Rich1/PipeInRich1AfterSubM')
      if det['/dd/Structure/LHCb/BeforeMagnetRegion/Velo']: 
          uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/Velo/DownStreamWakeFieldCone')
          uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/Velo/UpStreamWakeFieldCone')
          uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/Velo/UpstreamPipeSections')
          uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/Velo/VeloLeft/RFFoilLeft')
          uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/Velo/VeloRight/RFFoilRight')
          if flag.find('noVelo')<0:
           uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/PipeJunctionBeforeVelo');
           uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/BeforeVelo/PipeBeforeVelo');
           uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/Velo/UpstreamPipeSections');
      if det['/dd/Structure/LHCb/BeforeMagnetRegion/VP']:
        uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/VP/DownStreamWakeFieldCone')
        uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/VP/UpStreamWakeFieldCone')
        uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/VP/UpstreamPipeSections')
      uiSvc().visualize('/dd/Structure/LHCb/DownstreamRegion/PipeDownstream')
      uiSvc().visualize('/dd/Structure/LHCb/DownstreamRegion/PipeSupportsDownstream')
      uiSvc().visualize('/dd/Structure/LHCb/MagnetRegion/PipeInMagnet')
      uiSvc().visualize('/dd/Structure/LHCb/MagnetRegion/PipeSupportsInMagnet')
      uiSvc().visualize('/dd/Structure/LHCb/DownstreamRegion/AfterMuon')
      uiSvc().visualize('/dd/Structure/LHCb/UpstreamRegion/MBXWHUp')
      uiSvc().visualize('/dd/Structure/LHCb/UpstreamRegion/PipeUpstream')
def vis_simpleBeamPipe():
      uiSvc().visualize('/dd/Structure/LHCb/AfterMagnetRegion/PipeAfterT')
      uiSvc().visualize('/dd/Structure/LHCb/AfterMagnetRegion/T/PipeInT')
      if det['/dd/Structure/LHCb/BeforeMagnetRegion/TT']:uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/TT/PipeInTT')
      if det['/dd/Structure/LHCb/BeforeMagnetRegion/UT']:uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/UT/PipeInUT')
      uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/PipeJunctionBeforeVelo')
      uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/BeforeVelo/PipeBeforeVelo')
      uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/Rich1/Rich1BeamPipe')
      uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/Rich1/PipeInRich1SubMaster')
      uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/Rich1/PipeInRich1BeforeSubM')
      uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/Rich1/PipeInRich1AfterSubM')
      if det['/dd/Structure/LHCb/BeforeMagnetRegion/Velo']: 
          uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/Velo/DownStreamWakeFieldCone')
          uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/Velo/UpStreamWakeFieldCone')
          uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/Velo/UpstreamPipeSections')
          uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/Velo/VeloLeft/RFFoilLeft')
          uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/Velo/VeloRight/RFFoilRight')
      if det['/dd/Structure/LHCb/BeforeMagnetRegion/VP']:
        uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/VP/DownStreamWakeFieldCone')
        uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/VP/UpStreamWakeFieldCone')
        uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/VP/UpstreamPipeSections')
      uiSvc().visualize('/dd/Structure/LHCb/DownstreamRegion/PipeDownstream')
      uiSvc().visualize('/dd/Structure/LHCb/MagnetRegion/PipeInMagnet')
def vis_herschel():
      Region().setTransformScale(1.,1.,0.05)
# Camera positioning 
      Camera().setPosition(-8545.39, 1418.3, -3118.83)
      Camera().setHeight(7832.1)
      Camera().setOrientation(0.0527779, 0.995558, 0.07797, 4.3581)
      Camera().setNearFar(1444.12,16984.5)
      uiSvc().visualize('/dd/Structure/LHCb/BeforeUpstreamRegion/HCB2')
      uiSvc().visualize('/dd/Structure/LHCb/BeforeUpstreamRegion/LSSLeft')
      uiSvc().visualize('/dd/Structure/LHCb/UpstreamRegion/PipeUpstream')
      uiSvc().visualize('/dd/Structure/LHCb/UpstreamRegion/HCB0')
      uiSvc().visualize('/dd/Structure/LHCb/UpstreamRegion/HCB1')
      uiSvc().visualize('/dd/Structure/LHCb/UpstreamRegion/MBXWHUp')
      uiSvc().visualize('/dd/Structure/LHCb/AfterDownstreamRegion/HCF2')
      uiSvc().visualize('/dd/Structure/LHCb/AfterDownstreamRegion/LSSRight')
      uiSvc().visualize('/dd/Structure/LHCb/MagnetRegion/Magnet')
      vis_beamPipe()
