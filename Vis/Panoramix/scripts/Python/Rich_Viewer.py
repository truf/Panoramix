from Panoramix import *
from panoramixmodule import *
from Rich_RecoRings_Imp import *
from Rich_RecoSegments_Imp import *
from Rich_RecoPixels_Imp import *
from Rich_RecoPhotons_Imp import *
from Rich_MCPixels_Imp import *
from Rich_MCSegments_Imp import *
from Rich_MCRings_Imp import *
from Rich_MCPhotons_Imp import *
from Rich_TracklessRings_Imp import *
# RichDigitsLocation = 'Raw/Rich/Digits'
RichDigitsLocations = ['/Event/Rec/Rich/RecoEvent/Offline/Pixels','/Event/Rec/Rich/RecoEvent/OfflineLong/Pixels',
                       '/Event/Rec/Rich/RecoEvent/OfflineVeloTT/Pixels','/Event/Rec/Rich/RecoEvent/OfflineKsTrack/Pixels']

def createPage(title,nregions):
    page = ui().currentPage()
    page.deleteRegions()
    Page().setTitle(title)
    Page().titleVisible(True)
    if nregions == 4 : 
        Page().createDisplayRegions(2,2,0)
    else : 
        Page().createDisplayRegions(1,1,0)
        ui().currentWidget().cast_ISoViewer().setHeadlight(1)
    Style().setWireFrame()
    if nregions == 4 : 
        Page().setCurrentRegion(0)
        uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/Rich1/HPDPanel0')
        Page().setCurrentRegion(1)
        uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/Rich1/HPDPanel1')
        Page().setCurrentRegion(2)
        uiSvc().visualize('/dd/Structure/LHCb/AfterMagnetRegion/Rich2/HPDPanel0')
        Page().setCurrentRegion(3)
        uiSvc().visualize('/dd/Structure/LHCb/AfterMagnetRegion/Rich2/HPDPanel1')
    Style().setSolid() 

def execute(what):
    curtitle = ui().currentPage().title.getValues()[0]
    if what == '2d' :
        # setup 2d projections for all 4 HPD panels on one page
        title = 'Top RICH1             Bottom RICH2'
        if curtitle != title:  
            createPage(title,4)
            onlineViews[title] = 'Rich_2dView'
        Style().setColor('orange')  
        Page().setCurrentRegion(0)
        Page().currentRegion().clear("dynamicScene")
        # Camera positioning :
        Camera().setPosition( -74.7999, 1211.17, 1507.66)
        Camera().setHeight(750.)
        Camera().setOrientation(-1., -0.00132081, -0.0039564, 4.2447)
        Camera().setNearFar(1.,4500.)
        for x in RichDigitsLocations:
           if evt[x]: uiSvc().visualize(x)
        ui().synchronize()
        Page().setCurrentRegion(1)
        Page().currentRegion().clear("dynamicScene")
        # Camera positioning :
        Camera().setPosition(-1101.24, -3958.24, 5111.69)
        Camera().setHeight(750.)
        Camera().setOrientation(-0.108323, -0.314397, 0.943091, 3.06624)
        Camera().setNearFar(1000.,20000.)
        for x in RichDigitsLocations:
          if evt[x]: uiSvc().visualize(x)
        Style().setColor('magenta')
        Page().setCurrentRegion(2)
        Page().currentRegion().clear("dynamicScene")
        # Camera positioning :
        Camera().setPosition(2900., 8., 10100.)
        Camera().setHeight(950.)
        Camera().setOrientation(0.659871, 0.64718, -0.381744, 3.89631)
        Camera().setNearFar(1.,1600.)
        for x in RichDigitsLocations:
          if evt[x]: uiSvc().visualize(x)
        Page().setCurrentRegion(3)
        Page().currentRegion().clear("dynamicScene")
        # Camera positioning :
        Camera().setPosition(-4250., 20., 11000.)
        Camera().setHeight(950.)
        Camera().setOrientation(0.4552, -0.464477, -0.759641, 1.83032)
        Camera().setNearFar(1.,13000.)
        for x in RichDigitsLocations:
          if evt[x]: uiSvc().visualize(x)
#
        overlay_runeventnr()    
item = ui().parameterValue('Panoramix_Rich_input_action.value')

if item == 'RichRecoCKRings':
    ok = True
    session().setParameter('modeling.what','RichRecoCKRings')
    collect_scene_graphs(da())
    data_filter(da(),'name','Track*')
    Visualize_Selected_Rich_RecoRings(da())
    collect_scene_graphs(da())
    data_filter(da(),'name','RichRecSegment*')
    Visualize_Selected_Rich_RecoRings(da())
    collect_scene_graphs(da())
    data_filter(da(),'name','RichRecPixel*')
    Visualize_Selected_Rich_RecoRings(da())
    collect_scene_graphs(da())
    data_filter(da(),'name','RichRecPhoton*')
    Visualize_Selected_Rich_RecoRings(da())
    collect_scene_graphs(da())
    data_filter(da(),'name','RichRecRing*')
    Visualize_Selected_Rich_RecoRings(da())
    collect_scene_graphs(da())
    data_filter(da(),'name','Particle*')
    Visualize_Selected_Rich_RecoRings(da())

if item == 'RichRecoSegments':
    ok = True
    session().setParameter('modeling.what','RichRecoSegments')
    collect_scene_graphs(da())
    data_filter(da(),'name','Track*')
    Visualize_Selected_Rich_RecoSegments(da())
    collect_scene_graphs(da())
    data_filter(da(),'name','Particle*')
    Visualize_Selected_Rich_RecoSegments(da())
    collect_scene_graphs(da())
    data_filter(da(),'name','RichRecRing*')
    Visualize_Selected_Rich_RecoSegments(da())
    collect_scene_graphs(da())
    data_filter(da(),'name','RichRecPixel*')
    Visualize_Selected_Rich_RecoSegments(da())
    collect_scene_graphs(da())
    data_filter(da(),'name','RichRecPhoton*')
    Visualize_Selected_Rich_RecoSegments(da())

if item == 'RichRecoPhotons':
    ok = True
    session().setParameter('modeling.what','RichRecoPhotons')
    collect_scene_graphs(da())
    data_filter(da(),'name','Track*')
    Visualize_Selected_Rich_RecoPhotons(da())
    collect_scene_graphs(da())
    data_filter(da(),'name','RichRecSegment*')
    Visualize_Selected_Rich_RecoPhotons(da())
    collect_scene_graphs(da())
    data_filter(da(),'name','RichRecRing*')
    Visualize_Selected_Rich_RecoPhotons(da())
    collect_scene_graphs(da())
    data_filter(da(),'name','RichRecPixel*')
    Visualize_Selected_Rich_RecoPhotons(da())
    collect_scene_graphs(da())
    data_filter(da(),'name','Particle*')
    Visualize_Selected_Rich_RecoPhotons(da())

if item == 'RichRecoPixels':
    ok = True
    session().setParameter('modeling.what','RichRecoPixels')
    collect_scene_graphs(da())
    data_filter(da(),'name','Track*')
    Visualize_Selected_Rich_RecoPixels(da())
    collect_scene_graphs(da())
    data_filter(da(),'name','Particle*')
    Visualize_Selected_Rich_RecoPixels(da())
    collect_scene_graphs(da())
    data_filter(da(),'name','RichRecRing*')
    Visualize_Selected_Rich_RecoPixels(da())
    collect_scene_graphs(da())
    data_filter(da(),'name','RichRecSegment*')
    Visualize_Selected_Rich_RecoPixels(da())
    collect_scene_graphs(da())
    data_filter(da(),'name','RichRecPhoton*')
    Visualize_Selected_Rich_RecoPixels(da())

# CRJ : Not yet implemented
#if item == 'RichRecoTracks':
#    ok = True
#    session().setParameter('modeling.what','RichRecoTracks')
#    collect_scene_graphs(da())
#    data_filter(da(),'name','RichRecSegment*')
#    data_visualize(da())
#    collect_scene_graphs(da())
#    data_filter(da(),'name','RichRecRing*')
#    data_visualize(da())
#    collect_scene_graphs(da())
#    data_filter(da(),'name','RichRecPhoton*')
#    data_visualize(da())
#    collect_scene_graphs(da())
#    data_filter(da(),'name','RichRecPixel*')
#    data_visualize(da())

if item == 'RichMCPixels':
    ok = True
    session().setParameter('modeling.what','RichMCPixels')
    collect_scene_graphs(da())
    data_filter(da(),'name','Track*')
    Visualize_Selected_Rich_MCPixels(da())
    collect_scene_graphs(da())
    data_filter(da(),'name','RichRecSegment*')
    Visualize_Selected_Rich_MCPixels(da())
    collect_scene_graphs(da())
    data_filter(da(),'name','RichRecPixel*')
    Visualize_Selected_Rich_MCPixels(da())
    collect_scene_graphs(da())
    data_filter(da(),'name','RichRecRing*')
    Visualize_Selected_Rich_MCPixels(da())
    collect_scene_graphs(da())
    data_filter(da(),'name','RichRecPhoton*')
    Visualize_Selected_Rich_MCPixels(da())
    collect_scene_graphs(da())
    data_filter(da(),'name','Particle*')
    Visualize_Selected_Rich_MCPixels(da())

if item == 'RichMCSegments':
    ok = True
    session().setParameter('modeling.what','RichMCSegments')
    collect_scene_graphs(da())
    data_filter(da(),'name','Track*')
    Visualize_Selected_Rich_MCSegments(da())
    collect_scene_graphs(da())
    data_filter(da(),'name','RichRecSegment*')
    Visualize_Selected_Rich_MCSegments(da())
    collect_scene_graphs(da())
    data_filter(da(),'name','RichRecRing*')
    Visualize_Selected_Rich_MCSegments(da())
    collect_scene_graphs(da())
    data_filter(da(),'name','RichRecPhoton*')
    Visualize_Selected_Rich_MCSegments(da())
    collect_scene_graphs(da())
    data_filter(da(),'name','RichRecPixel*')
    Visualize_Selected_Rich_MCSegments(da())
    collect_scene_graphs(da())
    data_filter(da(),'name','Particle*')
    Visualize_Selected_Rich_MCSegments(da())

if item == 'RichMCCKRings':
    ok = True
    session().setParameter('modeling.what','RichMCCKRings')
    collect_scene_graphs(da())
    data_filter(da(),'name','Track*')
    Visualize_Selected_Rich_MCRings(da())
    collect_scene_graphs(da())
    data_filter(da(),'name','RichRecSegment*')
    Visualize_Selected_Rich_MCRings(da())
    collect_scene_graphs(da())
    data_filter(da(),'name','RichRecPhoton*')
    Visualize_Selected_Rich_MCRings(da())
    collect_scene_graphs(da())
    data_filter(da(),'name','RichRecRing*')
    Visualize_Selected_Rich_MCRings(da())
    collect_scene_graphs(da())
    data_filter(da(),'name','Particle*')
    Visualize_Selected_Rich_MCRings(da())
    
if item == 'RichMCPhotons':
    ok = True
    session().setParameter('modeling.what','RichMCPhotons')
    collect_scene_graphs(da())
    data_filter(da(),'name','Track*')
    Visualize_Selected_Rich_MCPhotons(da())
    collect_scene_graphs(da())
    data_filter(da(),'name','RichRecSegment*')
    Visualize_Selected_Rich_MCPhotons(da())
    collect_scene_graphs(da())
    data_filter(da(),'name','RichRecPhoton*')
    Visualize_Selected_Rich_MCPhotons(da())
    collect_scene_graphs(da())
    data_filter(da(),'name','RichRecRing*')
    Visualize_Selected_Rich_MCPhotons(da())
    collect_scene_graphs(da())
    data_filter(da(),'name','Particle*')
    Visualize_Selected_Rich_MCPhotons(da())
    

