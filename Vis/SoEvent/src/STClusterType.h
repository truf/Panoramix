#ifndef SoEvent_STClusterType_h
#define SoEvent_STClusterType_h

// Inheritance :
#include "Type.h"

#include "Event/STCluster.h" //The data class.

class IUserInterfaceSvc;
class ISoConversionSvc;
class IDataProviderSvc;

class STClusterType : public SoEvent::Type<LHCb::STCluster> {
public: //Lib::IType
  virtual Lib::Variable value(Lib::Identifier,const std::string&,void*);
public: //OnX::IType
  virtual void visualize(Lib::Identifier,void*);
public:
  STClusterType(IUserInterfaceSvc*,ISoConversionSvc*,IDataProviderSvc*);
private:
  void visualizeMCParticle(LHCb::STCluster&);
};

#endif
