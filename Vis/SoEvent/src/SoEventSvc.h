#ifndef SoEvent_SoEventSvc_h
#define SoEvent_SoEventSvc_h

// Gaudi :
#include "GaudiKernel/Service.h"
#include "GaudiKernel/MsgStream.h"
#include "Kernel/IParticlePropertySvc.h"

template <typename T> class SvcFactory;

class IUserInterfaceSvc;
class ISoConversionSvc;
class IDataProviderSvc;
class IParticlePropertySvc;
class IToolSvc;

class SoEventSvc : public Service 
{
public: //IService
  virtual StatusCode initialize();
  virtual StatusCode finalize();
  SoEventSvc(const std::string&,ISvcLocator*);
  virtual ~SoEventSvc();
private:
  inline MsgStream & msgStream()
  {
    if ( !m_msgStream ) { m_msgStream = new MsgStream(msgSvc(), Service::name()); }
    return *m_msgStream;
  }
  MsgStream & debug()       { return msgStream() << MSG::DEBUG;   }
  MsgStream & info()        { return msgStream() << MSG::INFO;    }
  MsgStream & error()       { return msgStream() << MSG::WARNING; }
  MsgStream & warning()     { return msgStream() << MSG::ERROR;   }
private:
  template < class SVC >
  void releaseSvc( SVC *& svc ) { if (svc) { svc->release(); svc=NULL; } }
  template < class SVC >
  StatusCode getService( const std::string & name, SVC *& svc )
  {
    releaseSvc(svc);
    const StatusCode sc = service(name,svc,true);
    if ( sc.isFailure() || !svc ) 
    {
      error() << name << " not found" << endmsg;
    } else { svc->addRef(); }
    return sc;
  }
private:
  IUserInterfaceSvc* m_uiSvc;
  ISoConversionSvc* m_soConSvc;
  IDataProviderSvc* m_evtDataSvc;
  IDataProviderSvc* m_detDataSvc;
  LHCb::IParticlePropertySvc* m_pPropSvc;   
  IToolSvc* m_toolSvc;
  MsgStream* m_msgStream;
};

#endif
