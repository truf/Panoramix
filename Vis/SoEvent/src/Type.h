#ifndef OnXSvc_Type_h
#define OnXSvc_Type_h

class IDataProviderSvc;

#include "OnXSvc/ISoConversionSvc.h"
#include "OnXSvc/IUserInterfaceSvc.h"

#include "Lib/Interfaces/IIterator.h"
#include "Lib/Interfaces/ISession.h"
#include "Lib/Out.h"
#include "OnX/Core/BaseType.h"

#include "GaudiKernel/IDataProviderSvc.h"
#include "GaudiKernel/SmartDataPtr.h"
#include "GaudiKernel/KeyedContainer.h"
#include "GaudiKernel/System.h"

namespace SoEvent {

template <class Object> 
class Iterator : public Lib::IIterator {
 public: //Lib::IIterator
  virtual Lib::Identifier object() {
    if(!fVector) return 0;
    if(fIterator==fVector->end()) return 0;
    return *fIterator;
  }
  virtual void next() { 
    if(!fVector) return;
    // Skip null objects.
    while(true) {
      ++fIterator;        
      if(fIterator==fVector->end()) return;
      if(*fIterator) return;
    }
  }
  virtual void* tag() { return 0;}
public:
  typedef KeyedContainer< Object, Containers::HashMap> Collection;
  Iterator(Collection* aVector = 0):fVector(aVector) {
    if(fVector) fIterator = fVector->begin();
  }
private:
  Collection* fVector;
  typename Collection::iterator fIterator;
};

template <class Object> 
class Type : public OnX::BaseType {
private:
  typedef KeyedContainer< Object, Containers::HashMap> Collection;
public: //Lib::IType
  virtual void* cast(const std::string& aClass) const {
    //FIXME : needed with g++-3.23 so that 
    //        the virtuality over the safe cast works !
    if(aClass=="Slash::Data::IVisualizer") {
      return (void*)static_cast<const Slash::Data::IVisualizer*>(this);
    } else {
      return OnX::BaseType::cast(aClass);
    }
  }
  virtual std::string name() const {  return fType;}
  virtual Lib::IIterator* iterator() {
    if(fIterator) return fIterator;
    if(!fDataProviderSvc) {
      Lib::Out out(printer());
      out << "SoEvent::Type<>::iterator :"
	  << " no data provider found."
	  << Lib::endl;
      return new Iterator< Object >();
    }
    SmartDataPtr<DataObject> smartDataObject(fDataProviderSvc,fLocation);
    if(smartDataObject) {}
    DataObject* dataObject = 0;
    StatusCode sc = fDataProviderSvc->retrieveObject(fLocation, dataObject);
    if(!sc.isSuccess()) {
      Lib::Out out(printer());
      out << "SoEvent::Type<>::iterator :"
	  << " retreiveObject failed."
	  << Lib::endl;
      return new Iterator< Object >();
    }
    Collection* collection = dynamic_cast<Collection*>(dataObject);
    if(!collection) {
      Lib::Out out(printer());
      out << "SoEvent::Type<>::iterator :"
	  << " dynamic_cast failed."
	  << Lib::endl;
      return new Iterator< Object >();
    }
    return new Iterator< Object >(collection);
  }
  virtual void setIterator(Lib::IIterator* aIterator) { fIterator = aIterator;}
public: //OnX::IType
  virtual void beginVisualize() {
    if(!fUISvc) {
      Lib::Out out(printer());
      out << "SoEvent::Type<>::beginVisualize :"
	  << " can't get a IUserInterfaceSvc."
	  << Lib::endl;
      fSoRegion = 0;
      return;
    }
    ISession* session = fUISvc->session();
    if(!session) {
      Lib::Out out(printer());
      out << "SoEvent::Type<>::beginVisualize :"
	  << " can't get a ISession."
	  << Lib::endl;
      fSoRegion = 0;
      return;
    }
    fSoRegion = fUISvc->currentSoRegion();
  }
  virtual void endVisualize() {
    fSoRegion = 0;
  }
  virtual void visualize(Lib::Identifier aIdentifier,void*) {
    if(!aIdentifier) return;
    if(!fSoCnvSvc) return;
    if(!fCollection) return;
    Object* object = (Object*)aIdentifier;
    // Convert it :
    if(!fConverter) {
      // Optimisation : get the converter once.
      fConverter = fSoCnvSvc->converter(fCLID);
      if(!fConverter) {
        Lib::Out out(printer());
        out << "SoEvent::Type<>::visualize :"
            << " converter not found for Object/CLID='"
            << System::typeinfoName( typeid( *fCollection ) ) << "'/'"
            << fCLID << "'"
	    << Lib::endl;
      }
    }
    // Have a Collection with one entry.
    fCollection->add(object);
    if(fConverter) {
      IOpaqueAddress* addr = (IOpaqueAddress*)fSoRegion;
      fConverter->createRep(fCollection,addr) ;
    } else { // Try anyway with the generic mechanism :
      IOpaqueAddress* addr = 0;
      StatusCode sc = fSoCnvSvc->createRep(fCollection, addr);
      if (sc.isSuccess()) sc = fSoCnvSvc->fillRepRefs(addr,fCollection);
    }  
    fCollection->remove(object);
  }
public:
  Type(const CLID& aCLID,
       const std::string& aType,
       const std::string& aLocation,
       IUserInterfaceSvc* aUISvc,
       ISoConversionSvc* aSoCnvSvc,
       IDataProviderSvc* aDataProviderSvc)
    :OnX::BaseType(aUISvc->printer())
    ,fCLID(aCLID)
    ,fType(aType)
    ,fLocation(aLocation)
    ,fIterator(0)
    ,fConverter(0)
    ,fCollection(0)
    ,fSoRegion(0)
    ,fUISvc(aUISvc)
    ,fSoCnvSvc(aSoCnvSvc)
    ,fDataProviderSvc(aDataProviderSvc){

    fCollection = new Collection();
  }
  virtual ~Type() {
    delete fCollection;
  }
  void setLocation(const std::string& aLocation) { fLocation = aLocation;}
  const std::string& location() const { return fLocation;}
protected:
  bool setLocationFromSession(bool verbose = true) {
    if(!fUISvc) return false;
    ISession* session = fUISvc->session();
    if(!session) return false;
    std::string value;
    if(!session->parameterValue(fType+".location",value)) {
      if(verbose) {
        Lib::Out out(printer());
        out << "SoEvent::Type<" << fType << ">::setLocationFromSession :"
            << " Session parameter " << fType << ".location not found."
            << Lib::endl;
      }
      return false;
    }
    if(value=="") {
      if(verbose) {
        Lib::Out out(printer());
        out << "SoEvent::Type<" << fType << ">::setLocationFromSession :"
            << " Session parameter " << fType << ".location empty."
            << Lib::endl;
      }
      return false;
    }
    if(verbose) {
      Lib::Out out(printer());
      out << "SoEvent::Type<" << fType << ">::setLocationFromSession :"
          << " set location \"" << value << "\""
          << Lib::endl;
    }
    setLocation(value);
    return true;
  }
private:
  const CLID& fCLID;
  std::string fType;
  std::string fLocation;
  Lib::IIterator* fIterator;
  IConverter* fConverter;
  Collection* fCollection;
  SoRegion* fSoRegion;
protected:  
  IUserInterfaceSvc* fUISvc;
  ISoConversionSvc* fSoCnvSvc;
  IDataProviderSvc* fDataProviderSvc;
};

}

#endif
