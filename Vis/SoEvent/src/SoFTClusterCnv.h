#ifndef SoEvent_SoFTClusterCnv_h
#define SoEvent_SoFTClusterCnv_h

#include "SoEventConverter.h"

template <class T> class CnvFactory;

class SoFTClusterCnv : public SoEventConverter {
  //friend class CnvFactory<SoFTRawClusterCnv>;
public:
  SoFTClusterCnv(ISvcLocator*);
  virtual StatusCode createRep(DataObject*,IOpaqueAddress*&);
public:
  static const CLID& classID();
  static unsigned char storageType();
};

#endif
