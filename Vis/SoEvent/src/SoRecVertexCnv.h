#ifndef SoEvent_SoRecVertexCnv_h
#define SoEvent_SoRecVertexCnv_h

#include "SoEventConverter.h"
#include "Event/RecVertex.h"

template <class T> class CnvFactory;

class SoRecVertexCnv : public SoEventConverter {
  //friend class CnvFactory<SoRecVertexCnv>;
public:
  SoRecVertexCnv(ISvcLocator*);
  virtual StatusCode createRep(DataObject*,IOpaqueAddress*&);
public:
  static const CLID& classID();
  static unsigned char storageType();
};

#endif
