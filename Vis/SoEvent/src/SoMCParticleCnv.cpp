// To fix clashes between Gaudi and Windows :
#include "OnXSvc/Win32.h"

// this :
#include "SoMCParticleCnv.h"

// Inventor :
#include "Inventor/nodes/SoSeparator.h"
#include "Inventor/nodes/SoLightModel.h"
#include "Inventor/nodes/SoDrawStyle.h"
#include "Inventor/nodes/SoCoordinate3.h"
#include "Inventor/nodes/SoLineSet.h"
#include "Inventor/nodes/SoTransform.h"
#include "HEPVis/nodes/SoTextTTF.h"

// HEPVis :
#include "HEPVis/nodes/SoSceneGraph.h"
#include "HEPVis/nodes/SoHighlightMaterial.h"
#include "HEPVis/nodes/SoMarkerSet.h"
typedef HEPVis_SoMarkerSet SoMarkerSet;
#include "HEPVis/misc/SoStyleCache.h"

// Lib :
#include "Lib/smanip.h"
#include "Lib/Interfaces/ISession.h"

// Gaudi :
#include "GaudiKernel/MsgStream.h"
#include "Kernel/IParticlePropertySvc.h"
#include "GaudiKernel/CnvFactory.h"
#include "Kernel/ParticleProperty.h"

// LHCb :
#include "Event/MCParticle.h"
#include "Event/MCVertex.h"
#include "GaudiKernel/Point3DTypes.h"
#include "GaudiKernel/Vector3DTypes.h"

// SoUtils :
#include "SoUtils/SbProjector.h"

// OnXSvc :
#include "OnXSvc/Filter.h"
#include "OnXSvc/IUserInterfaceSvc.h"
#include "OnXSvc/ClassID.h"
#include "OnXSvc/Helpers.h"

DECLARE_CONVERTER_FACTORY(SoMCParticleCnv)

//////////////////////////////////////////////////////////////////////////////
  SoMCParticleCnv::SoMCParticleCnv(
                                   ISvcLocator* aSvcLoc
                                   )
    :SoEventConverter(aSvcLoc,SoMCParticleCnv::classID())
                                  //////////////////////////////////////////////////////////////////////////////
                                  //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
}
//////////////////////////////////////////////////////////////////////////////
StatusCode SoMCParticleCnv::createRep(
                                      DataObject* aObject
                                      ,IOpaqueAddress*& aAddr
                                      )
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  MsgStream log(msgSvc(), "SoMCParticleCnv");
  log << MSG::DEBUG << "MC createReps" << endmsg;

  if(!fUISvc) {
    log << MSG::INFO << " UI service not found" << endmsg;
    return StatusCode::SUCCESS;
  }

  if(!fParticlePropertySvc) {
    log << MSG::INFO << " ParticleProperty service not found" << endmsg;
    return StatusCode::SUCCESS;
  }

  ISession* session = fUISvc->session();
  if(!session) {
    log << MSG::INFO << " can't get ISession." << endmsg;
    return StatusCode::FAILURE;
  }

  SoRegion* region = 0;
  if(aAddr) {
    // Optimization.
    // If having a not null aAddr, we expect a SoRegion.
    // See SoEvent/Type.h/SoEvent::Type<>::beginVisualize.
    region = (SoRegion*)aAddr;
  } else {
    region = fUISvc->currentSoRegion();
  }
  if(!region) {
    log << MSG::INFO << " can't get viewing region." << endmsg;
    return StatusCode::FAILURE;
  }

  if(!aObject) {
    log << MSG::INFO << " NULL object." << endmsg;
    return StatusCode::FAILURE;
  }

  LHCb::MCParticles* mcParticles = dynamic_cast<LHCb::MCParticles*>(aObject);
  if(!mcParticles) {
    log << MSG::INFO << " bad object type." << endmsg;
    return StatusCode::FAILURE;
  }

  bool deleteVector = false;
  // Filter :
  Filter<LHCb::MCParticle> filter(*fUISvc,log);
  const std::string& cuts = fUISvc->cuts();
  if(cuts!="") {
    log << MSG::INFO << " cuts \"" << cuts << "\"" << endmsg;
    mcParticles = filter.collect(*mcParticles,"MCParticle",cuts);
    if(!mcParticles) return StatusCode::SUCCESS;
    //filter.dump(*mcParticles,"MCParticle");
    deleteVector = true;;
  }

  if(!mcParticles->size()) {
    log << MSG::INFO << " collection is empty." << endmsg;
    return StatusCode::SUCCESS;
  }

  log << MSG::DEBUG << "MC number " << mcParticles->size() << endmsg;

  // Representation attributes :
  // Get color (default is grey (valid on black or white background) ):
  double r = 0.5, g = 0.5, b = 0.5;
  double hr = 1.0, hg = 1.0, hb = 0.0;
  std::string value;
  if(session->parameterValue("modeling.color",value))
    Lib::smanip::torgb(value,r,g,b);
  if(session->parameterValue("modeling.highlightColor",value))
    Lib::smanip::torgb(value,hr,hg,hb);
  double lineWidth = 0;
  if(session->parameterValue("modeling.lineWidth",value))
    if(!Lib::smanip::todouble(value,lineWidth)) lineWidth = 0;
  // Non linear projections :
  session->parameterValue("modeling.projection",value);
  SoUtils::SbProjector projector(value.c_str());

  SoStyleCache* styleCache = region->styleCache();
  SoLightModel* lightModel = styleCache->getLightModelBaseColor();
  SoDrawStyle* drawStyle =
    styleCache->getLineStyle(SbLinePattern_solid,float(lineWidth));
  SoMaterial* highlightMaterial =
    styleCache->getHighlightMaterial(float(r),float(g),float(b),
                                     float(hr),float(hg),float(hb),0,TRUE);

  int npoints = 100;

  // One scene graph per object ?
  int multiNodeLimit = 1000;
  if(session->parameterValue("modeling.multiNodeLimit",value))
    if(!Lib::smanip::toint(value,multiNodeLimit)) multiNodeLimit = 1000;

  if(int(mcParticles->size())>=multiNodeLimit) {
    // One line set for all MCParticles.
    LHCb::MCParticles::iterator it;

    // Count number of segments :
    int segmentn = 0;
    for(it = mcParticles->begin(); it != mcParticles->end(); it++) {
      if(!(*it)->originVertex()) continue;
      segmentn++;
    }

    SbVec3f* points = new SbVec3f[2 * segmentn];
    int32_t pointn = 0;
    bool abnormalParticle = false;
    for(it = mcParticles->begin(); it != mcParticles->end(); it++) {
      LHCb::MCParticle* mcParticle  = (*it);
      const LHCb::MCVertex* mcVertex = mcParticle->originVertex();
      if(!mcVertex) {
        log << MSG::INFO << "MC particle without origin vertex." << endmsg;
        continue;
      }

      const Gaudi::XYZPoint& pos = mcVertex->position();
      points[pointn].setValue((float)pos.x(),(float)pos.y(),(float)pos.z());
      pointn++;

      const SmartRefVector<LHCb::MCVertex>& decayVertices =
        mcParticle->endVertices();
      int number = decayVertices.size();
      if(number>=1) {
        const LHCb::MCVertex* vertex = decayVertices[0];
        if(vertex) {
          const Gaudi::XYZPoint& _pos = vertex->position();
          points[pointn].setValue
            ((float)_pos.x(),(float)_pos.y(),(float)_pos.z());
          pointn++;
        } else {
          abnormalParticle = true;
          double factor = 1000;
          Gaudi::XYZVector vec = mcParticle->momentum().Vect().Unit();
          points[pointn].setValue((float)(pos.x() + factor * vec.x()),
                                  (float)(pos.y() + factor * vec.y()),
                                  (float)(pos.z() + factor * vec.z()));
          pointn++;
        }
      } else { // No decay vertices, set end point far away :
        double factor = 1000;
        Gaudi::XYZVector vec = mcParticle->momentum().Vect().Unit();
        points[pointn].setValue((float)(pos.x() + factor * vec.x()),
                                (float)(pos.y() + factor * vec.y()),
                                (float)(pos.z() + factor * vec.z()));
        pointn++;
      }
    }

    if(abnormalParticle) {
      log << MSG::INFO
          << "some MCParticle has null vertices in endVertices."
          << endmsg;
    }

    if(pointn) {

      SoSceneGraph* separator = new SoSceneGraph;
      separator->setString("MCParticles");

      separator->addChild(highlightMaterial);

      separator->addChild(lightModel);

      separator->addChild(drawStyle);

      SoCoordinate3* coordinate3 = new SoCoordinate3;
      projector.project(pointn,points);
      coordinate3->point.setValues(0,pointn,points);
      separator->addChild(coordinate3);

      int32_t* vertices = new int32_t [segmentn];
      for (int count=0;count<segmentn;count++) {
        vertices[count] = 2;
      }

      //log << MSG::INFO << "MC nseg " << segmentn << endmsg;
      SoLineSet* lineSet = new SoLineSet;
      lineSet->numVertices.setValues(0,segmentn,vertices);
      delete [] vertices;
      separator->addChild(lineSet);

      // Send scene graph to the page (in the "dynamic" area) :
      region_addToDynamicScene(*region,separator);

    }
    delete [] points;

  } else {
    // One scene graph per MCParticle :

    // Representation attributes :
    bool showText = false;
    session->parameterValue("modeling.showText",value);
    Lib::smanip::tobool(value,showText);
    int sizeText = 10; //SoTextTTF default.
    session->parameterValue("modeling.sizeText",value);
    if(!Lib::smanip::toint(value,sizeText)) sizeText = 10;
    session->parameterValue("modeling.posText",value);
    std::string posTextAtb = value=="" ? "medium" : value;

    bool abnormalParticle = false;
    LHCb::MCParticles::iterator it;
    for(it = mcParticles->begin(); it != mcParticles->end(); it++) {
      LHCb::MCParticle* mcParticle  = (*it);
      const LHCb::MCVertex* mcVertex = mcParticle->originVertex();
      if(!mcVertex) {
        log << MSG::INFO << "MC particle without origin vertex." << endmsg;
        continue;
      }

      // Build picking string id :
      char sid[64];
      ::sprintf(sid,"MCParticle/0x%lx",(unsigned long)mcParticle);

      SoSceneGraph* separator = new SoSceneGraph;
      separator->setString(sid);

      // Material :
      separator->addChild(highlightMaterial);

      separator->addChild(lightModel);
      separator->addChild(drawStyle);

      const SmartRefVector<LHCb::MCVertex>& decayVertices = mcParticle->endVertices();

      SbVec3f textPoint;

      int number = decayVertices.size();
      if(number>=1) {

        SbVec3f* points = new SbVec3f[npoints*number+number+1];
        Gaudi::XYZPoint pos = mcVertex->position();
        Gaudi::XYZPoint curPos = pos;

        int count = 0;
        points[count].setValue((float)pos.x(),(float)pos.y(),(float)pos.z() );
        count++;

        // star for each prod vertex:
        SbVec3f star((float)pos.x(),(float)pos.y(),(float)pos.z());
        SoCoordinate3* coord3 = new SoCoordinate3;
        projector.project(1,&star);
        coord3->point.setValues(0,1,&star);
        separator->addChild(coord3);

        SoMarkerSet* markerSetA = new SoMarkerSet;
        markerSetA->numPoints = 1;
        markerSetA->markerIndex = SoMarkerSet::STAR_9_9;
        separator->addChild(markerSetA);

        SmartRefVector<LHCb::MCVertex>::const_iterator itv;
        bool goodParticle = true;

        for(itv=decayVertices.begin();itv!=decayVertices.end();++itv) {
          const LHCb::MCVertex* vertex = (*itv);
          if(!vertex) {
            goodParticle = false;
            abnormalParticle = true;
            break;
          }
          Gaudi::XYZPoint newpos = vertex->position();

          // triangle for each decay vertex:
          SbVec3f trian((float)newpos.x(),(float)newpos.y(),(float)newpos.z());
          SoCoordinate3* coordinate3 = new SoCoordinate3;
          projector.project(1,&trian);
          coordinate3->point.setValues(0,1,&trian);
          separator->addChild(coordinate3);

          SoMarkerSet* markerSetB = new SoMarkerSet;
          markerSetB->numPoints = 1;
          markerSetB->markerIndex = SoMarkerSet::TRIANGLE_UP_LINE_9_9;
          separator->addChild(markerSetB);

          // extrapolate as straight line between origin and end vertices
          // except if end vertex is before magnet
          double zvelo   = 900.;
          if ( curPos.z() < zvelo && newpos.z() > zvelo ) {
            // inside velo, use momentum vector
            Gaudi::XYZVector vec = mcParticle->momentum().Vect().Unit();
            double step   = ( zvelo - curPos.z() )/ vec.z();
            points[count].setValue(
                                   (float)(curPos.x() + step * vec.x()),
                                   (float)(curPos.y() + step * vec.y()),
                                   (float)(curPos.z() + step * vec.z()));
            count++;
          }
          // if end vertex is before magnet
          double zex   = 5300.;
          if ( points[count-1][2] < zex && newpos.z() > zex ) {
            // through magnet, use momentum vector
            Gaudi::XYZVector vec = mcParticle->momentum().Vect().Unit();
            double step   = ( zex - points[count-1][2] )/ vec.z();
            points[count].setValue(
                                   (float)(points[count-1][0] + step * vec.x()),
                                   (float)(points[count-1][1] + step * vec.y()),
                                   (float)(points[count-1][2] + step * vec.z()));
            count++;
          }
          // last point is decay vertex
          points[count].setValue((float)newpos.x(),
                                 (float)newpos.y(),
                                 (float)newpos.z());
          count++;

          curPos = newpos;
        }
        if(goodParticle) {
          SoCoordinate3* coordinate3 = new SoCoordinate3;
          projector.project(count,points);
          coordinate3->point.setValues(0,count,points);
          separator->addChild(coordinate3);

          SoLineSet* lineSet = new SoLineSet;
          int32_t vertices = count;
          lineSet->numVertices.setValues(0,1,&vertices);
          separator->addChild(lineSet);
        }

        if(posTextAtb=="medium") {
          textPoint = (points[0] + points[1])/2;
        } else {
          double posText;
          if(Lib::smanip::todouble(posTextAtb,posText)) {
            SbVec3f direction = points[1]-points[0];
            direction.normalize();
            SbVec3f offset ;
            if ( mcParticle->particleID().threeCharge() > 0 ) {
              offset.setValue(0.1,0.,0.);
            }else if ( mcParticle->particleID().threeCharge() < 0 ) {
              offset.setValue(-0.1,0.,0.);
            }else{
              offset.setValue(0.,0.,0.);
            }
            textPoint = points[0] + offset + float(posText) * direction;
          } else {
            textPoint = (points[0] + points[1])/2;
          }
        }

        delete [] points;

      } else { // No decay vertices, set end point at magnet :
        SbVec3f* points = new SbVec3f[npoints];
        Gaudi::XYZPoint pos = mcVertex->position();

        // star for each prod vertex:
        SbVec3f star((float)pos.x(),(float)pos.y(),(float)pos.z());
        SoCoordinate3* coord3 = new SoCoordinate3;
        projector.project(1,&star);
        coord3->point.setValues(0,1,&star);
        separator->addChild(coord3);

        SoMarkerSet* markerSetC = new SoMarkerSet;
        markerSetC->numPoints = 1;
        markerSetC->markerIndex = SoMarkerSet::STAR_9_9;
        separator->addChild(markerSetC);

        Gaudi::XYZVector vec = mcParticle->momentum().Vect().Unit();
        double step   = ( 5300.) / fabs(vec.z());
        points[0].setValue((float)(pos.x()),(float)(pos.y()),(float)(pos.z()));
        points[1].setValue( (float)(pos.x() + step * vec.x()),
                            (float)(pos.y() + step * vec.y()),
                            (float)(pos.z() + step * vec.z()));

        double posText;
        Lib::smanip::todouble(posTextAtb,posText);
        SbVec3f direction = points[1]-points[0];
        direction.normalize();
        textPoint = points[0] + float(posText) * direction;

        SoCoordinate3* coordinate3 = new SoCoordinate3;
        projector.project(2,points);
        coordinate3->point.setValues(0,2,points);
        separator->addChild(coordinate3);

        SoLineSet* lineSet = new SoLineSet;
        int32_t vertices = 2;
        lineSet->numVertices.setValues(0,1,&vertices);
        separator->addChild(lineSet);

        delete [] points;
      }

      // Text :
      if(showText) {
        SoSeparator* sepText = new SoSeparator;
        SoTransform* tsf = new SoTransform;
        tsf->translation.setValue(textPoint);

        SoTextTTF* text = new SoTextTTF();
        text->size.setValue(sizeText);

        const LHCb::ParticleProperty* pp =
          fParticlePropertySvc->find(mcParticle->particleID());
        std::string mcname = pp ? pp->particle() : "nil";
        text->string.set1Value(0,mcname.c_str());
        sepText->addChild(tsf);
        sepText->addChild(text);

        separator->addChild(sepText);
      }

      //  Send scene graph to the viewing region
      // (in the "dynamic" sub-scene graph) :
      region_addToDynamicScene(*region,separator);

    }

    if(abnormalParticle) {
      log << MSG::INFO
          << "some MCParticle has null vertices in endVertices."
          << endmsg;
    }

  }

  if(deleteVector) {
    // We have first to empty the vector :
    while(mcParticles->size()) {
      mcParticles->remove(*(mcParticles->begin()));
    }
    // Then we can delete it :
    delete mcParticles;
  }

  return StatusCode::SUCCESS;
}
//////////////////////////////////////////////////////////////////////////////
const CLID& SoMCParticleCnv::classID(
)
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  return LHCb::MCParticles::classID();
}
//////////////////////////////////////////////////////////////////////////////
unsigned char SoMCParticleCnv::storageType(
)
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  return So_TechnologyType;
}
