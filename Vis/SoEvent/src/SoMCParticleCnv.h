#ifndef SoEvent_SoMCParticleCnv_h
#define SoEvent_SoMCParticleCnv_h

#include "SoEventConverter.h"

template <class T> class CnvFactory;

class SoMCParticleCnv : public SoEventConverter {
  //friend class CnvFactory<SoMCParticleCnv>;
public:
  SoMCParticleCnv(ISvcLocator*);
  virtual StatusCode createRep(DataObject*,IOpaqueAddress*&);
public:
  static const CLID& classID();
  static unsigned char storageType();
};

#endif
