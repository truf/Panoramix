#ifndef SoEvent_ParticleType_h
#define SoEvent_ParticleType_h

// Inheritance :
#include "Type.h"

#include "TrackType.h"

#include "Event/Particle.h"
#include "Kernel/IParticlePropertySvc.h"

class IUserInterfaceSvc;
class ISoConversionSvc;
class IDataProviderSvc;
class IParticlePropertySvc;
class IToolSvc;
class MsgStream;

#include "Event/MCParticle.h"

#include "GaudiKernel/DeclareFactoryEntries.h"

class ParticleType : public SoEvent::Type<LHCb::Particle> 
{
public: //Lib::IType
  virtual Lib::Variable value(Lib::Identifier,const std::string&,void*);
public: //OnX::IType
  virtual void visualize(Lib::Identifier,void*);
public:
  ParticleType(IUserInterfaceSvc*,
               ISoConversionSvc*,
               IDataProviderSvc*,
               LHCb::IParticlePropertySvc*,
               IToolSvc*,
               MsgStream&);
  Lib::IIterator* iterator();
  ~ParticleType();
private:
  /// Draw the associated MCParticles
  void visualizeMCParticle(LHCb::Particle&);
  /// Draw the daughter Particles
  void visualizeDaughters(LHCb::Particle&);
  /// Draw the data container
  template < typename TYPE >
  inline void drawContainer( TYPE & objs ) const
  {
    if ( !objs.empty() )
    {
      IOpaqueAddress * addr = 0;
      StatusCode sc = fSoCnvSvc->createRep(&objs,addr);
      if (sc.isSuccess()) sc = fSoCnvSvc->fillRepRefs(addr,&objs);
      objs.clear();
    }
  }
private:
  LHCb::IParticlePropertySvc* fParticlePropertySvc;
  TrackType * m_trackType;
};

#endif
