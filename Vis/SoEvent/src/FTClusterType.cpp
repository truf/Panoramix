// this :
#include "FTClusterType.h"
#include "SoEventSvc.h"

// Lib :
#include "Lib/Interfaces/IIterator.h"
#include "Lib/Interfaces/ISession.h"
#include "Lib/Variable.h"
#include "Lib/Out.h"

// Gaudi :
#include "GaudiKernel/IDataProviderSvc.h"       
#include "GaudiKernel/SmartDataPtr.h"           

#include "OnXSvc/IUserInterfaceSvc.h"
#include "OnXSvc/ISoConversionSvc.h"

#include <Linker/LinkedTo.h>
// Event model :
#include <Event/MCParticle.h>
#include "FTDet/DeFTDetector.h"


//////////////////////////////////////////////////////////////////////////////
FTClusterType::FTClusterType(
                                 IUserInterfaceSvc* aUISvc
                                 ,ISoConversionSvc* aSoCnvSvc
                                 ,IDataProviderSvc* aDataProviderSvc
                                 ,IDataProviderSvc* aDetectorDataSvc
                                 )
  :SoEvent::Type<LHCb::FTCluster>(
                                    LHCb::FTClusters::classID(),
                                    "FTCluster",
                                    LHCb::FTClusterLocation::Default,
                                    aUISvc,aSoCnvSvc,aDataProviderSvc)
  ,fDetectorDataSvc(aDetectorDataSvc)
{
  addProperty("layer", Lib::Property::INTEGER);
  addProperty("quarter", Lib::Property::INTEGER);
  addProperty("sipmId", Lib::Property::INTEGER);
  addProperty("sipmCell",   Lib::Property::INTEGER);
  addProperty("address", Lib::Property::POINTER);
}
//////////////////////////////////////////////////////////////////////////////
DeFTDetector * FTClusterType::deFTDetector() const
{
  static bool alreadyTried = false;
  static DeFTDetector * fFT(NULL);
  if ( !fFT && fDetectorDataSvc && !alreadyTried )
  {
    alreadyTried = true;
    fFT = (DeFTDetector*)SmartDataPtr<DeFTDetector>(fDetectorDataSvc,
                                          DeFTDetectorLocation::Default );
    if( fFT==0 )
    {
      Lib::Out out(printer());
      out << "ERROR : FTClusterType : "
          << "Unable to retrieve FT detector element at " << DeFTDetectorLocation::Default
          << Lib::endl;
    }
  }
  return fFT;
}
//////////////////////////////////////////////////////////////////////////////
Lib::Variable FTClusterType::value(
 Lib::Identifier aIdentifier
,const std::string& aName
,void*
) 
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  LHCb::FTCluster* obj = (LHCb::FTCluster*)aIdentifier;
  LHCb::FTChannelID chid = obj->channelID();
  if(aName=="address") {
    return Lib::Variable(printer(),(void*)obj);
  } else if(aName=="layer") {
    return Lib::Variable(printer(),int(chid.layer()) );
  } else if(aName=="quarter") {
    return Lib::Variable(printer(),int(chid.quarter()) );
  } else if(aName=="sipmId") {
    return Lib::Variable(printer(),int(chid.sipmId()) );
  } else if(aName=="sipmCell") {
    return Lib::Variable(printer(),int(chid.sipmCell()) );
  } else {
    return Lib::Variable(printer());
  }
}
//////////////////////////////////////////////////////////////////////////////
void FTClusterType::visualize(
 Lib::Identifier aIdentifier
,void* aData
)
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{

  if(!aIdentifier) return;
  if(!fUISvc) return;
  if(!fUISvc->session()) return;
  std::string value;
  fUISvc->session()->parameterValue("modeling.what",value);
  if(value=="MCParticle") {

      LHCb::FTCluster* object = (LHCb::FTCluster*)aIdentifier;
      visualizeMCParticle(*object);

  } else {

      this->SoEvent::Type<LHCb::FTCluster>::visualize(aIdentifier,aData);

  }
}
//////////////////////////////////////////////////////////////////////////////
void FTClusterType::visualizeMCParticle(
    LHCb::FTCluster& aFTCluster
)
//////////////////////////////////////////////////////////////////////////////

//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
     std::string value;
     fUISvc->session()->parameterValue("DEBUG",value);
     LinkedTo<LHCb::MCParticle,LHCb::FTCluster> FTClusterLink (fDataProviderSvc,0,LHCb::FTClusterLocation::Default);
     LHCb::MCParticle * part  ;  
     part = FTClusterLink.first(&aFTCluster);
     while ( NULL != part) {
        LHCb::MCParticles* objs = new LHCb::MCParticles;
        objs->add(part);
        IOpaqueAddress* addr = 0;
        StatusCode sc = fSoCnvSvc->createRep(objs, addr);
        if (sc.isSuccess()) sc = fSoCnvSvc->fillRepRefs(addr,objs);
        if (value=="True"){
         Lib::Out out(printer());
         out <<  "INFO : FTCluster to MCParticle: "
             <<  &aFTCluster   << "  " <<  part    << Lib::endl;
        }
        objs->remove(part);
        delete objs;
        part = FTClusterLink.next();
    }
}

