#ifndef SoEventConverter_h
#define SoEventConverter_h

// Inheritance :
#include "GaudiKernel/Converter.h"
#include "Kernel/IParticlePropertySvc.h"

class ISoConversionSvc;
class IUserInterfaceSvc;
class IParticlePropertySvc;
class IDataProviderSvc;

class SoEventConverter : public Converter {
public:
  SoEventConverter(ISvcLocator*,CLID);
  virtual StatusCode initialize();
  virtual StatusCode finalize();
  virtual long repSvcType() const;
protected:
  ISoConversionSvc* fSoCnvSvc;
  IUserInterfaceSvc* fUISvc;
  LHCb::IParticlePropertySvc* fParticlePropertySvc;
  IDataProviderSvc* fDetectorDataSvc;
};

#endif
