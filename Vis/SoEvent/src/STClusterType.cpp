// this :
#include "STClusterType.h"

// Lib :
#include "Lib/Interfaces/IIterator.h"
#include "Lib/Interfaces/ISession.h"
#include "Lib/Variable.h"
#include "Lib/Out.h"

// Gaudi :
#include "GaudiKernel/IDataProviderSvc.h"       
#include "GaudiKernel/SmartDataPtr.h"           

#include "OnXSvc/IUserInterfaceSvc.h"
#include "OnXSvc/ISoConversionSvc.h"

#include <Linker/LinkedTo.h>
// Event model :
#include <Event/MCParticle.h>

//////////////////////////////////////////////////////////////////////////////
STClusterType::STClusterType(
 IUserInterfaceSvc* aUISvc
,ISoConversionSvc* aSoCnvSvc
,IDataProviderSvc* aDataProviderSvc
)
:SoEvent::Type<LHCb::STCluster>(
  LHCb::STClusters::classID(),
  "STCluster",
  LHCb::STClusterLocation::ITClusters,
  aUISvc,aSoCnvSvc,aDataProviderSvc)
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  addProperty("key",Lib::Property::INTEGER);
  addProperty("station", Lib::Property::INTEGER);
  addProperty("layer",   Lib::Property::INTEGER);
  addProperty("strip",   Lib::Property::INTEGER);
  addProperty("ITorTT",  Lib::Property::STRING);
  addProperty("address", Lib::Property::POINTER);
}
//////////////////////////////////////////////////////////////////////////////
Lib::Variable STClusterType::value(
 Lib::Identifier aIdentifier
,const std::string& aName
,void*
) 
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  LHCb::STCluster* obj = (LHCb::STCluster*)aIdentifier;
  LHCb::STChannelID chid = obj->firstChannel();
  if(aName=="address") {
    return Lib::Variable(printer(),(void*)obj);
  } else if(aName=="key") {
    return Lib::Variable(printer(),(int) (obj->key() ) );
  } else if(aName=="station") {
    return Lib::Variable(printer(),int(chid.station()) );
  } else if(aName=="layer") {
    return Lib::Variable(printer(),int(chid.layer()) );
  } else if(aName=="strip") {
    return Lib::Variable(printer(),int(chid.strip()) );
  } else if(aName=="ITorTT") {
	  if (chid.isTT()) return Lib::Variable(printer(),"TT" );
	  else  return Lib::Variable(printer(),"IT" );
  } else {
    return Lib::Variable(printer());
  }
}
//////////////////////////////////////////////////////////////////////////////
void STClusterType::visualize(
 Lib::Identifier aIdentifier
,void* aData
)
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{

  if(!aIdentifier) return;
  if(!fUISvc) return;
  if(!fUISvc->session()) return;
  std::string value;
  fUISvc->session()->parameterValue("modeling.what",value);
  if(value=="MCParticle") {

      LHCb::STCluster* object = (LHCb::STCluster*)aIdentifier;
      visualizeMCParticle(*object);

  } else {

      this->SoEvent::Type<LHCb::STCluster>::visualize(aIdentifier,aData);

  }
}
//////////////////////////////////////////////////////////////////////////////
void STClusterType::visualizeMCParticle(
    LHCb::STCluster& aSTCluster
)
//////////////////////////////////////////////////////////////////////////////

//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
     LinkedTo<LHCb::MCParticle,LHCb::STCluster> ITClusterLink (fDataProviderSvc,0,LHCb::STClusterLocation::ITClusters);
     LinkedTo<LHCb::MCParticle,LHCb::STCluster> TTClusterLink (fDataProviderSvc,0,LHCb::STClusterLocation::TTClusters);
     LHCb::MCParticle * part  ;  
     if (aSTCluster.isIT()) {
        part = ITClusterLink.first(&aSTCluster);
     }else {
        part = TTClusterLink.first(&aSTCluster);
     }
   while ( NULL != part) {
        LHCb::MCParticles* objs = new LHCb::MCParticles;
        objs->add(part);
        // Convert it :
        IOpaqueAddress* addr = 0;
        StatusCode sc = fSoCnvSvc->createRep(objs, addr);
        if (sc.isSuccess()) sc = fSoCnvSvc->fillRepRefs(addr,objs);
        objs->remove(part);
        delete objs;
        if (aSTCluster.isIT()) {
          part = ITClusterLink.next();
        }else {
          part = TTClusterLink.next();
     }
    }
}

