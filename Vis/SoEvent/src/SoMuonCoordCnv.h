#ifndef SoEvent_SoMuonCoordCnv_h
#define SoEvent_SoMuonCoordCnv_h

#include "SoEventConverter.h"

template <class T> class CnvFactory;

class SoMuonCoordCnv : public SoEventConverter {
  //friend class CnvFactory<SoMuonCoordCnv>;
public:
  SoMuonCoordCnv(ISvcLocator*);
  virtual StatusCode createRep(DataObject*,IOpaqueAddress*&);
public:
  static const CLID& classID();
  static unsigned char storageType();
};

#endif
