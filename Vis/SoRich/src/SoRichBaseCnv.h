
/** @file SoRichBaseCnv.h
 *
 *  Header file for RICH "So" visualisation converter base class : SoRichBaseCnv
 *
 *  @author  Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date    09/05/2004
 */

#ifndef  SORICH_SORICHBASECNV_H
#define  SORICH_SORICHBASECNV_H  1

// base class
#include "RichKernel/RichConverter.h"

// forward declaration
class ISoConversionSvc;
class IUserInterfaceSvc;

/**  @class SoRichBaseCnv  SoRichBaseCnv.h
 *
 *   Common base class for "So" converters for visualisation of
 *   RICH objects.
 *
 *   @author  Chris Jones   Christopher.Rob.Jones@cern.ch
 *   @date    09/05/2004
 */

class SoRichBaseCnv : public Rich::Converter
{

public:

  /// standard constructor
  SoRichBaseCnv ( const CLID& clid , ISvcLocator* svcLoc );

  /// virtual destructor
  virtual ~SoRichBaseCnv();

  /// standard finalization  method
  virtual StatusCode finalize();

  /// Creates the visual representations
  virtual StatusCode createRep ( DataObject* Object ,
                                 IOpaqueAddress*& Address );

protected:

  /// accessor to "So" conversion service
  ISoConversionSvc* soSvc() const;

  /// accessor to "UI" conversion service
  IUserInterfaceSvc* uiSvc() const;

private:

  /// So conversion service
  mutable ISoConversionSvc*  m_soSvc;

  /// UI service
  mutable IUserInterfaceSvc* m_uiSvc;

};

// ============================================================================
#endif  // SORICH_SORICHBASECNV_H
// ============================================================================
