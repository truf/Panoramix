// this :
#include "MCRichSegmentType.h"

#include "MCRichSegmentRep.h"

#include <OnXSvc/Helpers.h>

#include <Inventor/nodes/SoSeparator.h>

//////////////////////////////////////////////////////////////////////////////
MCRichSegmentType::MCRichSegmentType(
                                     IUserInterfaceSvc* aUISvc
                                     ,ISoConversionSvc* aSoCnvSvc
                                     ,IDataProviderSvc* aDataProviderSvc
                                     )
  :OnXSvc::KeyedType<LHCb::MCRichSegment>(
                                          LHCb::MCRichSegments::classID(),
                                          "MCRichSegment",
                                          LHCb::MCRichSegmentLocation::Default,
                                          aUISvc,aSoCnvSvc,aDataProviderSvc)
  ,m_separator(0)
  ,m_rep(0)
  ,m_empty(true)
  //////////////////////////////////////////////////////////////////////////////
  //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  addProperty("segmentn",Lib::Property::INTEGER);
}
//////////////////////////////////////////////////////////////////////////////
Lib::Variable MCRichSegmentType::value(
                                       Lib::Identifier aIdentifier
                                       ,const std::string& aName
                                       ,void*
                                       )
  //////////////////////////////////////////////////////////////////////////////
  //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  LHCb::MCRichSegment* obj = (LHCb::MCRichSegment*)aIdentifier;

  if(aName=="segmentn") {
    int segmentn = obj->trajectoryPoints().size()-1;
    return Lib::Variable(printer(),segmentn);
  } else {
    return Lib::Variable(printer());
  }
}
//////////////////////////////////////////////////////////////////////////////
void MCRichSegmentType::beginVisualize(
)
  //////////////////////////////////////////////////////////////////////////////
  //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  OnXSvc::KeyedType<LHCb::MCRichSegment>::beginVisualize();

  // Representation attributes :
  float r = 0.5, g = 0.5, b = 0.5;
  attribute("modeling.color",r,g,b);
  SbColor color(r,g,b);
  float hr = 1.0, hg = 1.0, hb = 1.0;
  attribute("modeling.highlightColor",hr,hg,hb);
  SbColor hcolor(hr,hg,hb);
  std::string shape;
  if(!attribute("modeling.MCRichSegmentMode",shape)) shape = "line";

  m_rep = new MCRichSegmentRep(shape,
                               MCRichSegmentRep::Qualities
                               (color,hcolor,SoMarkerSet::CIRCLE_FILLED_5_5),
                               fSoRegion->styleCache());

  m_separator = m_rep->begin();

  m_empty = true;
}
//////////////////////////////////////////////////////////////////////////////
void MCRichSegmentType::visualize(
                                  Lib::Identifier aIdentifier
                                  ,void*
                                  )
  //////////////////////////////////////////////////////////////////////////////
  //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  LHCb::MCRichSegment* obj = (LHCb::MCRichSegment*)aIdentifier;
  m_rep->represent(obj,m_separator);
  m_empty = false;
}
//////////////////////////////////////////////////////////////////////////////
void MCRichSegmentType::endVisualize(
)
  //////////////////////////////////////////////////////////////////////////////
  //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  if(m_empty) {
    m_separator->unref();
  } else {
    fSoRegion->resetUndo();
    region_addToDynamicScene(*fSoRegion,m_separator);
  }
  delete m_rep;
  OnXSvc::KeyedType<LHCb::MCRichSegment>::endVisualize();
}



