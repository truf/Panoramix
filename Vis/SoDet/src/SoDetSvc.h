#ifndef SoDet_SoDetSvc_h
#define SoDet_SoDetSvc_h

// Gaudi :
#include <GaudiKernel/Service.h>

template <typename T> class SvcFactory;

class IUserInterfaceSvc;
class IMagneticFieldSvc;

class SoDetSvc : public Service { 
public: //IService
  virtual StatusCode initialize();
  virtual StatusCode finalize();
protected:
  // No instantiation via new, only via the factory.
  SoDetSvc(const std::string&,ISvcLocator*);
  virtual ~SoDetSvc();
  friend class SvcFactory<SoDetSvc>;
private:
  IUserInterfaceSvc* m_uiSvc;
  IMagneticFieldSvc* m_magneticFieldSvc;
};

#endif
