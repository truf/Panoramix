// To fix clashes between Gaudi and Windows :
#include <OnXSvc/Win32.h>

// this :
#include "SoDetElemCnv.h"

// Inventor :
#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoTranslation.h>
#include <Inventor/nodes/SoRotation.h>

#include <Lib/Interfaces/ISession.h>
#include <Lib/smanip.h>

#include <GaudiKernel/Converter.h>
#include <GaudiKernel/CnvFactory.h>
#include <GaudiKernel/ISvcLocator.h>

#include <DetDesc/CLIDDetectorElement.h>
#include <DetDesc/DetectorElement.h>
#include <DetDesc/Solids.h>
#include <DetDesc/IGeometryInfo.h>
#include <DetDesc/ILVolume.h>
#include <DetDesc/ISolid.h>

#include <OnXSvc/IUserInterfaceSvc.h>
#include <OnXSvc/ClassID.h>
#include <OnXSvc/Helpers.h>

#include "SoDetConverter.h"

DECLARE_CONVERTER_FACTORY(SoDetElemCnv)

/*
  class Transform3D : public HepTransform3D {
  public:
  Transform3D(const HepTransform3D& aHT3D) : HepTransform3D(aHT3D) {
  m[0]  = xx;     m[1]  = yx;    m[2]  = zx;     m[3]  = 0;
  m[4]  = xy;     m[5]  = yy;    m[6]  = zy;     m[7]  = 0;
  m[8]  = xz;     m[9]  = yz;    m[10] = zz;     m[11] = 0;
  m[12] = dx;     m[13] = dy;    m[14] = dz;     m[15] = 1;
  }
  SbMatrix* getMatrix () const {
  return new SbMatrix(m[0],m[1],m[2],m[3],
  m[4],m[5],m[6],m[7],
  m[8],m[9],m[10],m[11],
  m[12],m[13],m[14],m[15]);
  }
  private:
  float m[16];
  };
*/

//////////////////////////////////////////////////////////////////////////////
  SoDetElemCnv::SoDetElemCnv( ISvcLocator* aSvcLoc )
    :SoDetConverter(aSvcLoc, CLID_DetectorElement)
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
}
//////////////////////////////////////////////////////////////////////////////
StatusCode SoDetElemCnv::createRep(
                                   DataObject* aObject
                                   ,IOpaqueAddress*&
                                   )
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  if(!fUISvc) {
    msgStream() << MSG::WARNING << " UI service not found" << endmsg;
    return StatusCode::SUCCESS;
  }

  ISession* session = fUISvc->session();
  if(!session) {
    msgStream() << MSG::ERROR << " can't get OnX session." << endmsg;
    return StatusCode::FAILURE;
  }

  SoRegion* region = fUISvc->currentSoRegion();
  if(!region) {
    msgStream() << MSG::ERROR << " can't get viewing region." << endmsg;
    return StatusCode::FAILURE;
  }

  std::string value;
  bool opened = false;
  if(session->parameterValue("modeling.opened",value))
    Lib::smanip::tobool(value,opened);

  msgStream() << MSG::DEBUG << "SoDetElemCnv::createReps() called" << endmsg;

  if(!aObject) {
    msgStream() << MSG::ERROR << " NULL object." << endmsg;
    return StatusCode::FAILURE;
  }

  DetectorElement* de  = dynamic_cast<DetectorElement*>(aObject);
  if(!de) {
    msgStream() << MSG::ERROR << " bad object type." << endmsg;
    return StatusCode::FAILURE;
  }

  if(!de->geometry()) {
    msgStream() << MSG::WARNING << " no geometry associated" << endmsg;
    return StatusCode::SUCCESS;
  }

  const ILVolume* lv  = de->geometry()->lvolume();
  // check if the detector element has a geometry associated
  if( 0 == lv ) {
    msgStream() << MSG::WARNING << " no geometry associated" << endmsg;
    msgStream() << MSG::WARNING << " lvolume name :" << de->geometry()->lvolumeName()
                << "|" << endmsg;
    return StatusCode::SUCCESS;
  }

  msgStream() << MSG::DEBUG << " lvolume : "
              << de->geometry()->lvolumeName() << endmsg;

  SoNode* node = volumeToSoDetectorTreeKit(*lv,Gaudi::Transform3D(),opened);
  if(!node) {
    msgStream() << MSG::DEBUG << " no representation" << endmsg;
    return StatusCode::SUCCESS;
  }

  SoSeparator* separator = new SoSeparator;

  // Deal with the Transformation
  Gaudi::Rotation3D rot;
  Gaudi::XYZVector c;
  de->geometry()->toLocalMatrix().GetDecomposition(rot,c);
  c = c * -1.0;
  rot.Invert();
  Gaudi::AxisAngle axisAngle(rot);
  double angle;
  Gaudi::XYZVector v;
  axisAngle.GetComponents(v,angle);

  msgStream() << MSG::DEBUG << " translation "
              << c.x() << " " << c.y() << " " << c.z() << endmsg;
  msgStream() << MSG::DEBUG << " rotation "
              << v.x() << " " << v.y() << " " << v.z() << " " << angle << endmsg;

  SoTranslation* translation = new SoTranslation;
  translation->translation = SbVec3f((float)c.x(),(float)c.y(),(float)c.z());
  SoRotation* rotation = new SoRotation;
  rotation->rotation =
    SbRotation(SbVec3f((float)v.x(),(float)v.y(),(float)v.z()),(float)angle);

  // Add transformation and logical volume representation :
  // The volume is placed and rotated :
  separator->addChild(rotation);
  separator->addChild(translation);
  separator->addChild(node);

  // Send scene graph to the viewing region (in the "static" sub-scene graph) :
  region_addToStaticScene(*region,separator);

  return StatusCode::SUCCESS;
}
//////////////////////////////////////////////////////////////////////////////
const CLID& SoDetElemCnv::classID( )
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  return CLID_DetectorElement;
}
//////////////////////////////////////////////////////////////////////////////
unsigned char SoDetElemCnv::storageType( )
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  return So_TechnologyType;
}
