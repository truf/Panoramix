
// this :
#include "Types.h"

/*
// Inventor :
#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoLightModel.h>
#include <Inventor/nodes/SoDrawStyle.h>
#include <Inventor/nodes/SoCoordinate3.h>
#include <Inventor/nodes/SoLineSet.h>
#include <Inventor/nodes/SoTransform.h>
#include <Inventor/nodes/SoTranslation.h>

#include <Inventor/nodes/SoFont.h>
#include <Inventor/nodes/SoText2.h>

// HEPVis :
#include <HEPVis/SbPolyhedron.h>
#include <HEPVis/nodes/SoSceneGraph.h>
#include <HEPVis/nodes/SoPolyhedron.h>
#include <HEPVis/nodes/SoHighlightMaterial.h>

// Lib :
#include <Lib/smanip.h>

#include <OnXSvc/Helpers.h>
*/

// Lib :
#include "Lib/Interfaces/IIterator.h"
#include "Lib/Interfaces/ISession.h"
#include "Lib/Variable.h"
#include "Lib/Out.h"

// Gaudi
#include "Kernel/IParticlePropertySvc.h"
#include "GaudiKernel/ISvcLocator.h"
#include "GaudiKernel/IDataProviderSvc.h"
#include "Kernel/ParticleProperty.h"
#include "GaudiKernel/MsgStream.h"
#include "GaudiKernel/SmartDataPtr.h"
#include "GaudiKernel/IToolSvc.h"
#include "GaudiKernel/IRegistry.h"

// MC relations:
#include "Linker/LinkedTo.h"

#include "OnXSvc/IUserInterfaceSvc.h"
#include "OnXSvc/ISoConversionSvc.h"

// HepMC :
#include <Event/HepMCEvent.h>
#include <HepMC/GenEvent.h>

//////////////////////////////////////////////////////////////////////////////
GenParticleType::GenParticleType(
 LHCb::IParticlePropertySvc* aParticlePropertySvc
,IDataProviderSvc* aDataProviderSvc
,IUserInterfaceSvc* aUISvc
)
:OnX::BaseType(aUISvc->printer())
,fType("GenParticle")
,fParticlePropertySvc(aParticlePropertySvc)
,fDataProviderSvc(aDataProviderSvc)
,fUISvc(aUISvc)
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  addProperty("particle",Lib::Property::STRING);
  addProperty("barcode",Lib::Property::INTEGER);
  addProperty("pdg_id",Lib::Property::INTEGER);
  addProperty("energy",Lib::Property::DOUBLE);
  addProperty("pt",Lib::Property::DOUBLE);
  addProperty("charge",Lib::Property::DOUBLE,6);
  addProperty("beg_vtx",Lib::Property::BOOLEAN);
  addProperty("end_vtx",Lib::Property::BOOLEAN);
  addProperty("beg_vtx_d",Lib::Property::DOUBLE);
  addProperty("end_vtx_d",Lib::Property::DOUBLE);
  addProperty("beg_vtx_barcode",Lib::Property::INTEGER);
  addProperty("end_vtx_barcode",Lib::Property::INTEGER);
}
//////////////////////////////////////////////////////////////////////////////
std::string GenParticleType::name() const
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  return fType;
}
//////////////////////////////////////////////////////////////////////////////
Lib::IIterator* GenParticleType::iterator(
) 
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  if(!fDataProviderSvc) return 0;
  SmartDataPtr<DataObject> 
    smartDataObject(fDataProviderSvc,"/Event/McEventCollection");
  if(smartDataObject) {}
  DataObject* dataObject = 0;
  StatusCode sc = 
    fDataProviderSvc->retrieveObject("/Event/Gen/HepMCEvents", dataObject);
  if(!sc.isSuccess()) return 0;
  LHCb::HepMCEvents* mcEvents = dynamic_cast<LHCb::HepMCEvents*>(dataObject);
  if(!mcEvents) return 0;

  class Iterator : public Lib::IIterator {
  public: //Lib::IIterator
    virtual Lib::Identifier object() {
      if(fIterator==fEvent.particles_end()) return 0;
      return *fIterator;
    }
    virtual void next() { ++fIterator;}
    virtual void* tag() { return 0;}
  public:
    Iterator(HepMC::GenEvent& aEvent):fEvent(aEvent) {
      fIterator = fEvent.particles_begin();
    }
  private:
    HepMC::GenEvent& fEvent;
    HepMC::GenEvent::particle_const_iterator fIterator;
  };

  LHCb::HepMCEvents::iterator it;
  for(it=mcEvents->begin();it!=mcEvents->end();it++) {
    // In principle we should have only one McEvent :
    HepMC::GenEvent* genEvent = (*it)->pGenEvt();
    return new Iterator(*genEvent);
  }
  return 0;
}
//////////////////////////////////////////////////////////////////////////////
Lib::Variable GenParticleType::value(
 Lib::Identifier aIdentifier
,const std::string& aName
,void*
) 
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  HepMC::GenParticle* obj = (HepMC::GenParticle*)aIdentifier;
  if(aName=="id") {
    return Lib::Variable(printer(),(void*)obj);
  } else if(aName=="particle") {
    if(fParticlePropertySvc) {
      const LHCb::ParticleProperty* pp = 
	fParticlePropertySvc->find(LHCb::ParticleID(obj->pdg_id()));
      if(pp) return Lib::Variable(printer(),pp->particle());
    }
    return Lib::Variable(printer(),std::string("nil"));
  } else if(aName=="barcode") {
    return Lib::Variable(printer(),obj->barcode());
  } else if(aName=="pdg_id") {
    return Lib::Variable(printer(),(int)(obj->pdg_id()));
  } else if(aName=="energy") {
    return Lib::Variable(printer(),obj->momentum().e());
  } else if(aName=="pt") {
    double px = obj->momentum().px();
    double py = obj->momentum().py();
    return Lib::Variable(printer(),sqrt(px*px+py*py));
  } else if(aName=="charge") {
    if(fParticlePropertySvc) {
      const LHCb::ParticleProperty* pp = 
	fParticlePropertySvc->find(LHCb::ParticleID(obj->pdg_id()));
      if(pp) return Lib::Variable(printer(),pp->charge());
    }
    return Lib::Variable(printer(),0.);

  } else if(aName=="beg_vtx_barcode") {
    HepMC::GenVertex* v = obj->production_vertex();
    return Lib::Variable(printer(),v? v->barcode() : 0);
  } else if(aName=="end_vtx_barcode") {
    HepMC::GenVertex* v = obj->end_vertex();
    return Lib::Variable(printer(),v? v->barcode() : 0);
  } else if(aName=="beg_vtx") {
    HepMC::GenVertex* v = obj->production_vertex();
    return Lib::Variable(printer(),v?true:false);
  } else if(aName=="beg_vtx_d") {
    HepMC::GenVertex* v = obj->production_vertex();
    if(v) return Lib::Variable(printer(),v->point3d().r());
    return Lib::Variable(printer(),0.);
  } else if(aName=="end_vtx") {
    HepMC::GenVertex* v = obj->end_vertex();
    return Lib::Variable(printer(),v?true:false);
  } else if(aName=="end_vtx_d") {
    HepMC::GenVertex* v = obj->end_vertex();
    if(v) return Lib::Variable(printer(),v->point3d().r());
    return Lib::Variable(printer(),0.);

    /*
  } else if(aName=="bx") {
    HepMC::GenVertex* v = obj->production_vertex();
    if(v) return Lib::Variable(printer(),v->point3d().x());
    return Lib::Variable(printer(),0.);
  } else if(aName=="by") {
    HepMC::GenVertex* v = obj->production_vertex();
    if(v) return Lib::Variable(printer(),v->point3d().y());
    return Lib::Variable(printer(),0.);
  } else if(aName=="bz") {
    HepMC::GenVertex* v = obj->production_vertex();
    if(v) return Lib::Variable(printer(),v->point3d().z());
    return Lib::Variable(printer(),0.);

  } else if(aName=="ex") {
    HepMC::GenVertex* v = obj->end_vertex();
    if(v) return Lib::Variable(printer(),v->point3d().x());
    return Lib::Variable(printer(),0.);
  } else if(aName=="ey") {
    HepMC::GenVertex* v = obj->end_vertex();
    if(v) return Lib::Variable(printer(),v->point3d().y());
    return Lib::Variable(printer(),0.);
  } else if(aName=="ez") {
    HepMC::GenVertex* v = obj->end_vertex();
    if(v) return Lib::Variable(printer(),v->point3d().z());
    return Lib::Variable(printer(),0.);
  } else if(aName=="ed") {
    HepMC::GenVertex* v = obj->end_vertex();
    if(v) return Lib::Variable(printer(),v->point3d().r());
    return Lib::Variable(printer(),0.);
    */

  } else {
    return Lib::Variable(printer());
  }
}
//////////////////////////////////////////////////////////////////////////////
void GenParticleType::visualize(
 Lib::Identifier aIdentifier
,void*
) 
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  if(!aIdentifier) return;
  if(!fUISvc) return;
  SoRegion* soRegion = fUISvc->currentSoRegion();
  if(!soRegion) return;

  ISession* session = fUISvc->session();
  std::string value;
  bool showDecay = false;
  if(session->parameterValue("modeling.showDecay",value))
    Lib::smanip::tobool(value,showDecay);

  if(showDecay) 
    representDecay((HepMC::GenParticle*)aIdentifier,
		   session,soRegion,fParticlePropertySvc);
  else 
    represent((HepMC::GenParticle*)aIdentifier,
	      session,soRegion,fParticlePropertySvc);

}
//////////////////////////////////////////////////////////////////////////////
void GenParticleType::clear(
) 
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  //fParticles.clear();
}
//////////////////////////////////////////////////////////////////////////////
void GenParticleType::represent(
 HepMC::GenParticle* aGenParticle
,ISession* aSession
,SoRegion* aRegion
,LHCb::IParticlePropertySvc* aParticlePropertySvc
) 
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  HepMC::GenVertex* pv = aGenParticle->production_vertex();
  HepMC::GenVertex* ev = aGenParticle->end_vertex();

  if(!pv && !ev) return;

  // Representation attributes :
  // Get color (default is grey (valid on black or white background) ):
  double r = 0.5, g = 0.5, b = 0.5;
  double hr = 1.0, hg = 1.0, hb = 0.0;
  std::string value;
  if(aSession->parameterValue("modeling.color",value))
    Lib::smanip::torgb(value,r,g,b);
  if(aSession->parameterValue("modeling.highlightColor",value))
    Lib::smanip::torgb(value,hr,hg,hb);
  double lineWidth = 0;
  if(aSession->parameterValue("modeling.lineWidth",value))
    if(!Lib::smanip::todouble(value,lineWidth)) lineWidth = 0;

  bool showText = false;
  aSession->parameterValue("modeling.showText",value);
  Lib::smanip::tobool(value,showText);
  //double sizeText = 0.05;
  //aSession->parameterValue("modeling.sizeText",value);
  //if(!Lib::smanip::todouble(value,sizeText)) sizeText = 0.05;
  aSession->parameterValue("modeling.posText",value);
  std::string posTextAtb = value=="" ? "medium" : value;

  double scale = 1;
  aSession->parameterValue("modeling.scale",value);
  if(!Lib::smanip::todouble(value,scale)) scale = 1;

  HepMC::ThreeVector* pb = 0 ;
  HepMC::ThreeVector* pe = 0 ;
  HepMC::ThreeVector* vect = 0 ;
  if(!pv && ev) {
    pe->set(scale * ev->point3d().x(),
	   scale * ev->point3d().y(),
	   scale * ev->point3d().z());
    vect->set(aGenParticle->momentum().px(),
       aGenParticle->momentum().py(),
       aGenParticle->momentum().pz());
    double factor = 10000/vect->r();
    pb->set( pe->x() - factor * vect->x(),
             pe->y() - factor * vect->y(),
             pe->z() - factor * vect->z()) ;
  } else if(pv && !ev) { 
    pb->set(scale * pv->point3d().x(),
	   scale * pv->point3d().y(),
	   scale * pv->point3d().z());
    vect->set(aGenParticle->momentum().px(),
       aGenParticle->momentum().py(),
       aGenParticle->momentum().pz());
    double factor = 10000/vect->r();
    pe->set( pb->x() + factor * vect->x(),
             pb->y() + factor * vect->y(),
             pb->z() + factor * vect->z()) ;
  } else {
    pb->set(scale * pv->point3d().x(),
	   scale * pv->point3d().y(),
	   scale * pv->point3d().z());
    pe->set(scale * ev->point3d().x(),
	   scale * ev->point3d().y(),
	   scale * ev->point3d().z());
  }

  // Build name :
  char sid[64];
  ::sprintf(sid,"GenParticle/0x%lx",(unsigned long)aGenParticle);
	
  SoSceneGraph* separator = new SoSceneGraph;
  separator->setString(sid);

  // Material :
  SoHighlightMaterial* highlightMaterial = new SoHighlightMaterial;
  highlightMaterial->diffuseColor.setValue
    (SbColor((float)r,(float)g,(float)b));
  highlightMaterial->highlightColor.setValue
    (SbColor((float)hr,(float)hg,(float)hb));
  //highlightMaterial->transparency.setValue((float)transparency);
  separator->addChild(highlightMaterial);
      
  SoDrawStyle* drawStyle = new SoDrawStyle;
  drawStyle->style.setValue(SoDrawStyle::LINES);
  drawStyle->linePattern.setValue(0xFFFF);
  drawStyle->lineWidth.setValue((float)lineWidth);
      
  SoLightModel* lightModel = new SoLightModel;
  lightModel->model = SoLightModel::BASE_COLOR;

  SbVec3f points[2];
  points[0].setValue((float)pb->x(),(float)pb->y(),(float)pb->z());
  points[1].setValue((float)pe->x(),(float)pe->y(),(float)pe->z());

  // Text : 
  if(showText) {
    SbVec3f textPoint;
    if(posTextAtb=="medium") {
      textPoint = (points[0] + points[1])/2;
    } else {
      double posText;
      if(Lib::smanip::todouble(posTextAtb,posText)) {
	SbVec3f direction = points[1]-points[0];
	direction.normalize();
	direction *= (float)posText; 
	textPoint = points[0] + direction; 
      } else {
	textPoint = (points[0] + points[1])/2;
      }
    }

    SoSeparator* sepText = new SoSeparator;
    SoTransform* tsf = new SoTransform;
    tsf->translation.setValue(textPoint);
	
    std::string partname = "nil";
    const LHCb::ParticleProperty* pp = 
      aParticlePropertySvc->find(LHCb::ParticleID(aGenParticle->pdg_id()));
    if(pp) partname = pp->particle();

    SoFont* font = new SoFont();
    font->name.setValue("times");
    font->size.setValue(20.0F);

    SoText2* text = new SoText2();
    text->string.set1Value(0,partname.c_str());
	
    sepText->addChild(tsf);
    sepText->addChild(font);
    sepText->addChild(text);
    
    separator->addChild(sepText);
  }
      
  separator->addChild(lightModel);
  separator->addChild(drawStyle);

  float d = (points[1]-points[0]).length();
  if(d<=0) {
    SoTranslation* translation = new SoTranslation;
    translation->translation = points[0];
    separator->addChild(translation);

    double radius = aGenParticle->momentum().e()/1000;
    //SbPolyhedron* sbPol = new SbPolyhedronSphere(0,radius,0,2* M_PI,0,M_PI);
    SbPolyhedron* sbPol = new SbPolyhedronBox(radius,radius,radius);
    SoPolyhedron* polyhedron = new SoPolyhedron(*sbPol);
    delete sbPol;
    polyhedron->solid.setValue(FALSE);
    separator->addChild(polyhedron);
  } else {
    SoCoordinate3* coordinate3 = new SoCoordinate3;
    coordinate3->point.setValues(0,2,points);
    separator->addChild(coordinate3);

    SoLineSet* lineSet = new SoLineSet;
    int32_t vertices = 2;
    lineSet->numVertices.setValues(0,1,&vertices);
    separator->addChild(lineSet);
  }

  region_addToDynamicScene(*aRegion,separator);
  
}
//////////////////////////////////////////////////////////////////////////////
void GenParticleType::representDecay(
 HepMC::GenParticle* aGenParticle
,ISession* aSession
,SoRegion* aRegion
,LHCb::IParticlePropertySvc* aParticlePropertySvc
) 
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{

  // Daughters :
  HepMC::GenVertex* ev = aGenParticle->end_vertex();
  if(!ev) return;
  HepMC::GenVertex::particle_iterator it;
  for(it=ev->particles_begin(HepMC::children);
      it!=ev->particles_end(HepMC::children);++it) {
    HepMC::GenParticle* particle = *it;
    if(particle) {
      represent(aGenParticle,aSession,aRegion,aParticlePropertySvc);

      representDecay(particle,aSession,aRegion,aParticlePropertySvc);
    }
  }
  
}
